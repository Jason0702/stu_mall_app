import '../enum/app_enum.dart';
import '../models/order_model.dart';
import '../models/ranking_models.dart';
import '../models/record_models.dart';

class LocalRecord {
  //取得排行榜
  static List<RankingModel> rankings = [];

  //消費紀錄(支出)=訂單紀錄
  static List<OrderModel> orders = [];

  //DM幣使用紀錄
  static List<RecordModel> dmRecords =  [];
  //dm幣收入
  static List<RecordModel> get dmRecordsIncome => dmRecords.where((element) => element.types == AppEnum.income).toList();
  //dm幣支出
  static List<RecordModel> get dmRecordsOutcome => dmRecords.where((element) => element.types == AppEnum.outcome).toList();

  //點數使用紀錄
  static List<RecordModel> pointUseRecords= [];
  //點數收入
  static List<RecordModel> get pointRecordsIncome => pointUseRecords.where((element) => element.types == AppEnum.income).toList();
  //點數支出
  static List<RecordModel> get pointRecordsOutcome => pointUseRecords.where((element) => element.types == AppEnum.outcome).toList();


  //還沒付款的
  static List<OrderModel> get noPaymentOrders => orders
      .where((element) =>
          element.isPayment == AppEnum.inactive &&
          element.isShipping == AppEnum.inactive &&
          element.isTaked == AppEnum.inactive)
      .toList();

  //發貨中的
  static List<OrderModel> get shippingOrders => orders
      .where((element) =>
          element.isPayment == AppEnum.active &&
          element.isShipping == AppEnum.inactive &&
          element.isTaked == AppEnum.inactive)
      .toList();

  //還沒取貨的
  static List<OrderModel> get noTakeOrders => orders
      .where((element) =>
          element.isPayment == AppEnum.active &&
          element.isShipping == AppEnum.active &&
          element.isTaked == AppEnum.inactive)
      .toList();

  //已經取貨的
  static List<OrderModel> get takeOrders => orders
      .where((element) =>
      element.isTaked == AppEnum.active)
      .toList();

  void saveRanking(List<RankingModel> data) {
    rankings = data;
  }

  void saveOrders(List<OrderModel> data) {
    orders = data;
  }

  void saveDmRecord(List<RecordModel> data) {
    dmRecords = data;
  }

  void savePointRecord(List<RecordModel> data) {
    pointUseRecords = data;
  }
}
