import 'dart:developer';

import '../models/member_model.dart';
import '../models/user_coin_models.dart';

class LocalMember{
  static MemberModel? memberModel;

  static int get memberId => memberModel?.id ?? 0;
  //暱稱
  static String get nickName => memberModel?.name ?? '';
  //使用者的錢包
  static CoinModels? coinModels;
  static int get points => coinModels?.point ?? 0;
  static int get dmCoin => coinModels?.dmCoin ?? 0;
  static int get sticker => coinModels?.sticker ?? 0;

  static String qrcode = '';

  //儲存會員資料
  void saveMember(MemberModel? data){
    memberModel = data;
  }

  //儲存金幣
  void saveCoin(CoinModels? data){
    coinModels = data;
  }

  //儲存qrcode
  void saveQrcode(String data){
    qrcode = data;
  }
}