import '../models/products_model.dart';
import '../models/products_type_model.dart';

class LocalProduct {
  //所有產品資料
  static List<ProductsModel> products = [];

  //所有產品型態
  static List<ProductsTypeModel> productsTypes = [];

  void saveProducts(List<ProductsModel> data) {
    products = List.from(data);
  }

  void saveProductTypes(List<ProductsTypeModel> data) {
    productsTypes = List.from(data);
  }

}
