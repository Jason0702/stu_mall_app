import '../models/game_question_model.dart';
import '../models/game_type_models.dart';

class LocalGame {
  //遊戲列表
  static List<GameTypeModel> games = [];

  static GameTypeModel get bingo => games.firstWhere((element) => element.enName == 'Lottery');
  static GameTypeModel get quiz => games.firstWhere((element) => element.enName == 'Quiz');
  static GameTypeModel get scratcher => games.firstWhere((element) => element.enName == 'Drawing');

  static GameModel? quizQuestion;

  void saveGames(List<GameTypeModel> data) {
    games = data;
  }

  void saveQuizQuestion(GameModel data) {
    quizQuestion = data;
  }

}
