import 'dart:developer';

import '../models/carousels_model.dart';
import '../models/news_model.dart';

class LocalNew {
  //新聞列表
  static List<NewsModel> newsModel = [];
  //輪播列表
  static List<CarouselsModel> carouselsModel=  [];

  static void saveNews(List<NewsModel> data) {
    newsModel = data;
  }
  static void saveCarouselsModel(List<CarouselsModel> data) {
    carouselsModel = data;
  }
}
