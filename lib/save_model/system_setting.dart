import '../enum/app_enum.dart';
import '../models/systen_setting_model.dart';

class LocalSetting {
  static SystemSettingModel? setting;

  static int get exchangeRatePointToDm => setting?.exchangeRatePointToDm ?? 5;

  //dm幣付款
  static bool get dmPaymentStatus =>
      setting?.paymentType?['6']['status'] == AppEnum.active;

  //linePay付款
  static bool get linePayPaymentStatus =>
      setting?.paymentType?['3']['status'] == AppEnum.active;

  //一卡通付款
  static bool get iPassPaymentStatus =>
      setting?.paymentType?['2']['status'] == AppEnum.active;

  static ShippmentTypeModel get toHome =>
      ShippmentTypeModel.fromJson(setting?.shippmentType?['0']);

  static ShippmentTypeModel get convenienceStore =>
      ShippmentTypeModel.fromJson(setting?.shippmentType?['1']);

  static ShippmentTypeModel get selfGet =>
      ShippmentTypeModel.fromJson(setting?.shippmentType?['2']);

  static ShippmentTypeModel get cabinetGet =>
      ShippmentTypeModel.fromJson(setting?.shippmentType?['3']);

  //儲存
  void save(SystemSettingModel data) {
    setting = data;
  }
}
