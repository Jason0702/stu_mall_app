import 'dart:convert';

import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

import '../api/api.dart';
import '../generated/myservice.pb.dart';
import '../protos/client_channel.dart';

class GrpcUtil {
  static GRPCClientSingleton? singleton;

  static void connectGrpc(String _account, String _password) {
    singleton = GRPCClientSingleton(Api.gRpcPosition, Api.po);
    singleton?.memberLogin(_account, _password).asStream().listen((event) {
      print('gRpc_Login: ${event.data}');
    });
  }

  static ResponseStream<EventValue>? memberLoginFromAnotherDevice() {
    return singleton?.memberLoginFromAnotherDevice();
  }

  static ResponseStream<EventValue>? eventMemberTransactionSuccessIn() {
    return singleton?.eventMemberTransactionSuccessIn();
  }




  static void transaction(int meId, int otherId, int money) {
    singleton?.transaction(meId, otherId, money).asStream().listen((event) {
      print('轉帳 ${event.data}');
      delegate.push(name: RouteName.paymentResult,arguments: event.data);
      EasyLoading.dismiss();
    }, onError: (error) {
      print('發送轉帳Error: ${error.toString()}');
      EasyLoading.showError('轉帳失敗');
    });
  }

  static void logOut() {
    singleton?.memberLogOut();
    singleton?.shutDown();
  }
}
