enum TransactionEnum{
  get,
  pay,
}

extension Extension on TransactionEnum{
  static const List<String> _labels = [
    "收款",
    "付款",
  ];
  String get label => _labels[index];
}