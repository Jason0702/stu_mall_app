import '../utils/app_image.dart';

enum SettingEnum{
  order,
  check,
  myDm,
  record,
  dmRanking,
  game,
}

extension Extension on SettingEnum{
  static const List<String> _labels = [
    "我的訂單",
    "簽到集點",
    "我的DM幣",
    "紀錄查詢",
    "DM排行榜",
    "好康樂翻天",
  ];
  static const List<String> _icons = [
    AppImage.iconPersonalInformation,
    AppImage.iconMemberEarnPoints,
    AppImage.iconMyDmDollars,
    AppImage.iconSearchRecord,
    AppImage.iconRanking,
    AppImage.iconGame,
  ];
  String get label => _labels[index];
  String get icon => _icons[index];
}