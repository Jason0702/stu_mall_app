import '../utils/app_image.dart';

enum ShoppingCartStepEnum{
  selectQty,
  orderInfo,
  finish,
}

extension Extension on ShoppingCartStepEnum{
  static const List<String> _labels = [
    "選定數量",
    "付款資訊",
    "完成結帳",
  ];
  static const List<String> _images = [
    AppImage.iconCartStep1,
    AppImage.iconCartStep2,
    AppImage.iconCartStep3,
  ];


  String get label => _labels[index];
  String get image => _images[index];
}