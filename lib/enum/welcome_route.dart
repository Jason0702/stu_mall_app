enum WelcomeEnum{
  login,
  register,
}

extension Extension on WelcomeEnum{
  static const List<String> _labels = [
    "登入",
    "註冊",
  ];
  String get label => _labels[index];
}