enum memberRoleEnum{
  normal,
  vip,
  teacher,
}

extension Extension on memberRoleEnum{
  static const List<String> _labels = [
    "一般會員",
    "VIP會員",
    "教師專用",
  ];
  String get label => _labels[index];
}