
class AppEnum {
  static const String hasMember = "1";
  static const String noMember = "0";
  static const int inactive = 0;
  static const int active = 1;
  static const int private = 0;
  static const int public = 1;
  static const int outcome = 0;
  static const int income = 1;

  static const String success = "success";
  static const String fail = "fail";

  static const int totalSticker = 10;
  static const int stickerExChangePoints = 100;
  //取得排行榜的數量
  static const int getRankMember = 10;
}


class busEventName{
  static const String transPages = "/transPages";
  static const String toNews = "/toNews";
  static const String toItems = "/toItems";

}
