import '../utils/app_image.dart';

enum OrderEnum {
  payment,
  shipping,
  take,
}

extension Extension on OrderEnum {
  static const List<String> _labels = [
    "未付款",
    "發貨中",
    "未取貨",
  ];

  String get label => _labels[index];
}


enum OrderStatusEnum {
  fail,
  finish,
  processing,
  cancel,
  delete,
}

extension Extension4 on OrderStatusEnum {
  static const List<String> _labels = [
    "失敗",
    "完成",
    "處理中",
    "取消",
    "刪除",
  ];

  String get label => _labels[index];
}


enum OrderPaymentEnum {
  none,
  cash,
  credit,
  linePay,
  atm,
  vAtm,
  dmCoin,
}

extension Extension2 on OrderPaymentEnum {
  static const List<String> _labels = [
    'NonType',
    '現金',
    '信用卡',
    'LinePay',
    'ATM',
    '虛擬帳戶',
    'DM幣'
  ];
  static const List<String> _images = [
    '',
    '',
    AppImage.imgIpassLogo,
    AppImage.imgLinePayLogo,
    '',
    '',
    AppImage.iconDmDollar
  ];
  static const List<String> _showLabels = [
    '',
    '',
    '',
    '',
    '',
    '',
    'DM幣'
  ];
  String get showLabel => _showLabels[index];
  String get label => _labels[index];
  String get image => _images[index];
}

enum DeliveryTypeEnum {
  normal,
  store,
  self,
  cabinet
}

extension Extension3 on DeliveryTypeEnum {
  static const List<String> _labels = ['宅配到府寄送', '便利商店運送', '無人商店自取', '智取櫃取貨'];

  String get label => _labels[index];
}
