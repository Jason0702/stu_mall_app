enum TitleBarEnum { consumer, exchange }
extension Extension3 on TitleBarEnum {
  static const List<String> _labels = [
    '消費紀錄',
    '兌換紀錄',
  ];

  String get label => _labels[index];
}
