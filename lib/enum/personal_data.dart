enum PersonalDataEnum {
  personalData,
  changePassword,
}

extension Extension3 on PersonalDataEnum {
  static const List<String> _labels = ['個人資料', '修改密碼'];

  String get label => _labels[index];
}
