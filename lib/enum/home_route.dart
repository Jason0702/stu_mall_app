enum HomeEnum{
  home,
  news,
  payCode,
  itemInfo,
  memberCenter
}

extension Extension on HomeEnum{
  static const List<String> _labels = [
    "首頁",
    "活動訊息",
    "付款碼",
    "商品資訊",
    "會員中心",
  ];
  String get label => _labels[index];
}