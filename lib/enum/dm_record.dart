enum DmOperationType{
  order,
  trans,
  draw,
  exchange,
  system,
  answer
}
extension Extension on DmOperationType{
  static const List<String> _labels = [
    "訂單使用", "轉帳", "抽獎", "點數兌換",'系統發放','問答問題'
  ];
  static const List<String> _pointLabels = [
    "訂單使用",
    "簽到",
    "DM 幣兌換",
    "系統發放",
    "抽獎",
    "貼紙兌換",
  ];

  String get label => _labels[index];
  String get pointLabels => _pointLabels[index];
}