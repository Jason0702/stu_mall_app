import 'dart:convert';

import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../app.dart';
import '../utils/app_color.dart';

class QrCodeRoute extends StatefulWidget {
  const QrCodeRoute({Key? key}) : super(key: key);

  @override
  State<QrCodeRoute> createState() => _QrCodeRouteState();
}

class _QrCodeRouteState extends State<QrCodeRoute> {
  String data = "";
  String money = "";
  String id = "";
  String name = "";

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> map = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    data = jsonEncode(map);
    money = map['money'];
    id = map['id'];
    name= map['name'];
    return Column(
      children: [
        titleBarIntegrate('條碼收款', [_backButton()]),
        _bodyWidget(),
      ],
    );
  }
  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }


  //Body
  Widget _bodyWidget() {
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Text('收款方:  ',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
              ),
              Container(
                child: Text('${name}',style: TextStyle(fontSize: 20)),
              ),
            ],
          ),
          Container(padding: EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Container(
                  child: Text('會員ID:  ',style: TextStyle(fontSize:20,fontWeight: FontWeight.bold),),
                ),
                Container(
                  child: Text('${id}',style: TextStyle(fontSize: 20)),
                ),
              ],
            ),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Text('${money}',style: TextStyle(color:Colors.red,fontSize:50,fontWeight: FontWeight.bold),),
              ),
              Container(padding: EdgeInsets.only(top: 10),
                child: Text('枚',style: TextStyle(color:Colors.red,fontSize: 20)),
              ),
            ],
          ),
          Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
              boxShadow: [
                AppColor.shadow
              ],
            ),
            child: QrImage(
              data: data,
              version: QrVersions.auto,
              size: 200.0,
            )
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            child: Text(
              '請出示此條碼給對方掃描',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            child: Text(
              '即可收款',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
