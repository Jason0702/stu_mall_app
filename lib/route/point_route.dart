import 'dart:developer';

import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../save_model/member.dart';
import '../save_model/system_setting.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../widgets/app_widget.dart';
import '../widgets/point_card.dart';

class PointsRoute extends StatefulWidget {
  const PointsRoute({Key? key}) : super(key: key);

  @override
  State<PointsRoute> createState() => _PointsRouteState();
}

class _PointsRouteState extends State<PointsRoute> {
  TextEditingController _point = TextEditingController();
  MemberProvider? _memberProvider;

  @override
  Widget build(BuildContext context) {
    _memberProvider ??= Provider.of(context);
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('簽到集點', [_backButton()]),
          Expanded(
              child: ScrollConfiguration(
                  behavior: NoShadowScrollBehavior(),
                  child: SingleChildScrollView(child: _body()))),
        ],
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _body() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: PointsCard(),
          ),
          GestureDetector(
              onTap: ()=> _memberProvider!.memberCheckIn(),
              child: Container(width: 120, child: buttonRedWidget('點選簽到'))),
          SizedBox(
            height: 20,
          ),
          Text(
            '*每日簽到獲得一張貼紙，集滿${AppEnum.totalSticker}張可兌換點數${AppEnum.stickerExChangePoints}點',
            style: TextStyle(color: Colors.blue, fontSize: 15),
          ),
          SizedBox(
            height: 30,
          ),
          // 貼紙列表
          _pointList(),
          SizedBox(
            height: 30,
          ),
          Text(
            '目前DM幣1枚:${LocalSetting.setting?.exchangeRatePointToDm ?? 5}點 請輸入要兌換多少點數',
            textScaleFactor: 1,
          ),
          SizedBox(
            height: 10,
          ),
          _textInput(),
          SizedBox(
            height: 40,
          ),
          _exchangePoint(),
        ],
      ),
    );
  }

  // 貼紙列表
  Widget _pointList() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(),
            _pointStick(1),
            _pointStick(2),
            _pointStick(3),
            SizedBox(),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _pointStick(4),
            _pointStick(5),
            _pointStick(6),
            _pointStick(7),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(),
            _pointStick(8),
            _pointStick(9),
            _pointStick(10),
            SizedBox(),
          ],
        ),
      ],
    );
  }

  Widget _pointStick(int set) {
    return Container(
      width: 60,
      height: 60,
      child: Image.asset(
        LocalMember.sticker >= set
            ? AppImage.imgTSticker
            : 'assets/images/img_earn_points_${set}.png',
      ),
    );
  }

  Widget _textInput() {
    return TextField(
      style: TextStyle(color: Colors.black),
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp('[0-9]')),
      ],
      decoration: InputDecoration(
        errorText: null,
        contentPadding: EdgeInsets.all(10.0),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.circular(10.0)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        filled: true,
        fillColor: Colors.white,
      ),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _point,
    );
  }

  Widget _exchangePoint() {
    return GestureDetector(
      onTap: ()=>_memberProvider!.exchangePointsToDmCoin(_point.text),
      child: Container(width: AppSize.appWidth, child: buttonWidget('點數兌換')),
    );
  }
}
