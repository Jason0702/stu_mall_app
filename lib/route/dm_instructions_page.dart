import 'package:flutter/material.dart';
import 'package:html/parser.dart';

import '../utils/no_shadow_scroll_behavior.dart';

/*class DmInstructions extends StatefulWidget {
  const DmInstructions({Key? key}) : super(key: key);

  @override
  _DmInstructionsState createState() => _DmInstructionsState();
}

class _DmInstructionsState extends State<DmInstructions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            //Title
            Expanded(flex: 1, child: _titleWidget()),
            //Body
            Expanded(flex: 9, child: _bodyWidget()),
          ],
        ),
      ),
    );
  }
  Widget _titleWidget() {
    return Container(
      color: HexColor.fromHex('#00BE9E'),
      child: Row(
        children: [
          //空
          Expanded(
              flex: 1,
              child: Container(
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    color: Colors.white,
                    size: 35,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              )),
          //標題
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  '使用說明',
                  textScaleFactor: 1,
                  style: TextStyle(fontSize: 40.sp, color: Colors.white),
                ),
              )),
          //QR
          Expanded(flex: 1, child: Container()),
        ],
      ),
    );
  }

  Widget _bodyWidget() {
    return Container(color: Colors.white,
      padding: EdgeInsets.only(left:50.w,right: 50.w,top: 10.h),
      height: ScreenUtil().screenHeight,
      width: ScreenUtil().screenWidth,
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Container(
            child: Text(_parseHtmlString(instructions),style: TextStyle(fontSize: 30.sp),textScaleFactor: 1,
          ),
        ),
      )),
    );
  }

  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body?.text).documentElement!.text;
    return parsedString;
  }
}*/
