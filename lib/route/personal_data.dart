import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:com.sj.stu_mall_app/utils/no_shadow_scroll_behavior.dart';
import 'package:com.sj.stu_mall_app/utils/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../api/api.dart';
import '../app.dart';
import '../enum/personal_data.dart';
import '../save_model/member.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../widgets/app_widget.dart';
import '../widgets/text_from_filed.dart';

class PersonalDataPage extends StatefulWidget {
  const PersonalDataPage({Key? key}) : super(key: key);

  @override
  State<PersonalDataPage> createState() => _PersonalDataPageState();
}

class _PersonalDataPageState extends State<PersonalDataPage> {
  SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();
  PersonalDataEnum select = PersonalDataEnum.personalData;
  TextEditingController _account = TextEditingController();
  TextEditingController _name = TextEditingController();
  TextEditingController _cellphone = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _againPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  MemberProvider? _memberProvider;

  @override
  void initState() {
    super.initState();
    _account.text = LocalMember.memberModel?.account ?? '';
    _name.text = LocalMember.memberModel?.name ?? '';
    _email.text = LocalMember.memberModel?.email ?? '';
    _cellphone.text = LocalMember.memberModel?.cellphone ?? '';
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) async {
    if(Api.forceUpdatePassword){
      select = PersonalDataEnum.changePassword;
      setState(() {});
      _password.text = await _sharedPreferenceUtil.getPassword();

    }
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('個人資料', [_backButton()]),
          Expanded(
              child: ScrollConfiguration(
                  behavior: NoShadowScrollBehavior(),
                  child: SingleChildScrollView(child: _body()))),
        ],
      ),
    );
  }

  Widget _body() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(
              horizontal: AppSize.defaultPadding, vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white,
              boxShadow: [AppColor.shadow]),
          child: Row(
            children: [
              _subTitleCard(PersonalDataEnum.personalData),
              _subTitleCard(PersonalDataEnum.changePassword),
            ],
          ),
        ),
        select == PersonalDataEnum.personalData
            ? _dataWidget()
            : _changePasswordWidget()
      ],
    );
  }

  Widget _dataWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding * 2),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            _cardTitle('帳號'),
            _accountTextField(_account),
            _cardTitle('姓名'),
            NickNameTextField(
              controller: _name,
            ),
            _cardTitle('手機'),
            _cellPhoneTextField(_cellphone),
            _cardTitle('電子信箱'),
            EmailTextField(controller: _email),
            SizedBox(
              height: 20,
            ),
            _sendChangeDataButton(),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  Widget _sendChangeDataButton() {
    return GestureDetector(
      onTap: () => _sendChangeDataButtonOnTap(select),
      child: buttonWidget('確認修改資料'),
    );
  }

  void _sendChangeDataButtonOnTap(PersonalDataEnum select) {
    switch (select) {
      case PersonalDataEnum.personalData:
        changeData(_name.text, _cellphone.text, _email.text);
        break;
      case PersonalDataEnum.changePassword:
        changePassword(_newPassword.text, _password.text);
        break;
    }
  }

  Future<void> changePassword(String newPassword, String password) async {
    if (EasyLoading.isShow) {
      return;
    }
    if (!_formKey2.currentState!.validate()) {
      return;
    }
    String _password = await SharedPreferenceUtil().getPassword();
    if (_password != password) {
      EasyLoading.showError(EasyLoadingMessage.oldPasswordError);
      return;
    }
    if (newPassword != _againPassword.text) {
      EasyLoading.showError(EasyLoadingMessage.newPasswordError);
      return;
    }
    EasyLoading.show();
    await _memberProvider!.changeMemberPassword(newPassword, password);
  }

  Future<void> changeData(String name, String cellphone, String email) async {
    if (EasyLoading.isShow) {
      return;
    }
    if (!_formKey.currentState!.validate()) {
      return;
    }
    EasyLoading.show();
    await _memberProvider!.changeMemberData(name, cellphone, email);
  }

  Widget _changePasswordWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding * 2),
      child: Form(
        key: _formKey2,
        child: Column(
          children: [
            _cardTitle('目前的密碼'),
            _passwordTextField(_password),
            _cardTitle('新密碼'),
            _passwordTextField(_newPassword),
            _cardTitle('新密碼確認'),
            _passwordTextField(_againPassword),
            SizedBox(
              height: 20,
            ),
            _sendChangeDataButton(),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  Widget _accountTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      readOnly: true,
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入學號",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入學號";
        }
        return null;
      },
    );
  }

  Widget _cellPhoneTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入手機號碼",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入手機號碼";
        } else if (!TextFieldSetting().phoneVerification(value)) {
          return "手機號碼格式錯誤";
        }
        return null;
      },
    );
  }

  Widget _passwordTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入密碼",
      ),
      obscureText: true,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入密碼";
        } else if (value.trim().length < 6 || value.trim().length > 12) {
          return "請輸入6~12位英數字";
        } else if (!TextFieldSetting().passwordVerification(value)) {
          return "密碼只能包含英數文字加底線";
        }
        return null;
      },
    );
  }

  Widget _cardTitle(String data) {
    return Container(
      alignment: Alignment.centerLeft,
      child: RichText(
          textScaleFactor: 1,
          text: TextSpan(
            text: data,
            style: TextStyle(
                fontSize: 19,
                color: AppColor.darkBlue,
                fontWeight: FontWeight.bold),
          )),
    );
  }

  //上方切換列卡片
  Widget _subTitleCard(PersonalDataEnum set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.all(5),
          padding: EdgeInsets.symmetric(
            vertical: 10,
          ),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            set.label,
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
        ),
      ),
    );
  }

  void _subTitleCardOnTap(PersonalDataEnum set) {
    select = set;
    setState(() {});
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }
}
