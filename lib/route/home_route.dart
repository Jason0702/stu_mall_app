import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/providers/auth_provider.dart';
import 'package:com.sj.stu_mall_app/providers/shopping_cart_provider.dart';
import 'package:com.sj.stu_mall_app/route/page/payment_code.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:com.sj.stu_mall_app/utils/event_bus.dart';
import 'package:com.sj.stu_mall_app/utils/shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app.dart';
import '../dialog/not_logging_dialog.dart';
import '../enum/home_route.dart';
import '../enum/order.dart';
import '../grpc/grpc.dart';
import '../providers/game_provider.dart';
import '../providers/member_provider.dart';
import '../providers/news_provider.dart';
import '../providers/product_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/token_refresh.dart';
import '../widgets/app_widget.dart';
import '../widgets/shopping_cart_button.dart';
import 'page/home.dart';
import 'page/items.dart';
import 'page/member_setting.dart';
import 'page/news.dart';
import '../enum/app_enum.dart';

class HomeRoute extends StatefulWidget {
  const HomeRoute({Key? key}) : super(key: key);

  @override
  State<HomeRoute> createState() => _HomeRouteState();
}

class _HomeRouteState extends State<HomeRoute> {
  SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();
  HomeEnum _selectPage = HomeEnum.home;
  ProductProvider? _productProvider;
  NewsProvider? _newsProvider;
  MemberProvider? _memberProvider;
  GameProvider? _gameProvider;
  ShoppingCartProvider? _shoppingCartProvider;
  AuthProvider? _authProvider;
  StreamSubscription? memberLoginFormOtherStream;

  StreamSubscription? memberTransactionSuccessInStream;

  List _page = [
    HomePage(),
    NewsPage(),
    PaymentCodePage(),
    ItemsPage(),
    MemberSettingPage(),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TokenRefresher.setTokenRefresher();
    Future.delayed(Duration(seconds: 5), listenMemberLoginFormOtherStream);
    listenMemberTransactionSuccessInStream();
    listenBusEvent();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  void _runWhileAppIsTerminated() async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var details = await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
    if(details != null){
      if (details.didNotificationLaunchApp) {
        if(details.notificationResponse != null){
          if (details.notificationResponse!.payload != null) {
            if (await _sharedPreferenceUtil.getHasMsgId()) {
              _sharedPreferenceUtil.delHasMsgId();
              if(LocalMember.memberId == 0){

              } else {
                delegate.push(name: RouteName.myOrderRoute, arguments: OrderEnum.take);
              }
            }
          }
        }
      }
    }

  }

  _onLayoutDone(_) async {
    await Future.wait([
      _productProvider!.getProductTypes(),
      _productProvider!.getProducts(),
      _newsProvider!.getNews(),
      _newsProvider!.getCarousels(),
    ]);
    if (LocalMember.memberId == 0) {
      return;
    }
    await Future.wait([
      _memberProvider!.getMemberPoints(),
      _memberProvider!.getMemberQrcode(),
      _gameProvider!.getGameTypes(),
      _shoppingCartProvider!.getShoppingCarts(),
    ]);
    //_runWhileAppIsTerminated();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    memberLoginFormOtherStream?.cancel();
    memberTransactionSuccessInStream?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    _productProvider = Provider.of<ProductProvider>(context);
    _newsProvider = Provider.of<NewsProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _gameProvider = Provider.of<GameProvider>(context, listen: false);
    _shoppingCartProvider =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    _authProvider = Provider.of<AuthProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            //Title
            _titleWidget(),
            //Body
            Expanded(child: _page[_selectPage.index]),
          ],
        ),
      ),
      floatingActionButton: _floatingActionButtonWidget(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavigationBar(
        unselectedFontSize: 14,
        selectedFontSize: 16,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, size: 25),
            label: HomeEnum.home.label,
          ),
          BottomNavigationBarItem(
              icon: _selectPage == HomeEnum.news
                  ? Image.asset(
                      AppImage.btnTabBarNewsSelected,
                      width: 25,
                    )
                  : Image.asset(
                      AppImage.btnTabBarNewsNormal,
                      width: 25,
                    ),
              label: HomeEnum.news.label),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 25,
              ),
              label: HomeEnum.payCode.label),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket, size: 25),
              label: HomeEnum.itemInfo.label),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 25),
              label: HomeEnum.memberCenter.label),
        ],
        currentIndex: _selectPage.index,
        // 選中的索引
        fixedColor: AppColor.appMainColor,
        // 選中的顏色
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
        onTap: bottomNavigatorOnTap,
      ),
    );
  }

  bottomNavigatorOnTap(index) {
    if (LocalMember.memberId == 0 &&
        (HomeEnum.values[index] == HomeEnum.payCode ||
            HomeEnum.values[index] == HomeEnum.memberCenter)) {
      _showNotLoggingDialog();
      return;
    }
    setState(() {
      _selectPage = HomeEnum.values[index];
    });
  }

  //浮動按鈕
  Widget _floatingActionButtonWidget() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: Colors.transparent,
        child: Image.asset(
          AppImage.btnPay,
          fit: BoxFit.fill,
        ),
        onPressed: _floatingActionButtonOnPressed,
      ),
      height: 60,
      width: 60,
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40), color: Colors.white),
    );
  }

  //浮動按鈕Pressed
  void _floatingActionButtonOnPressed() {
    if (LocalMember.memberId == 0) {
      _showNotLoggingDialog();
      return;
    }
    setState(() {
      this._selectPage = HomeEnum.payCode;
    });
  }

  //Title
  Widget _titleWidget() {
    return Container(
      height: AppWidgetSetting.appBarHeight,
      color: AppColor.appMainColor,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          //空
          GestureDetector(
            onTap: () => qrCodeIconOnTap(),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.qrcode,
                    color: Colors.white,
                    size: 35,
                  ),
                  Text(
                    '會員條碼',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  )
                ],
              ),
            ),
          ),
          //標題
          Expanded(
              child: Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  AppImage.logoStart,
                  width: 45,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  '智慧無人商店平台',
                  style: TextStyle(color: Colors.white, fontSize: 17),
                )
              ],
            ),
          )),
          ShoppingCartButton(),
        ],
      ),
    );
  }

  void listenMemberLoginFormOtherStream() {
    memberLoginFormOtherStream =
        GrpcUtil.memberLoginFromAnotherDevice()?.listen((value) {
      print(value);
      List getId = value.value.split(': ');
      print('MemberLoginFromAnotherDevice: ${getId[1]}');
      if (getId[1] == LocalMember.memberId.toString()) {
        EasyLoading.showError(EasyLoadingMessage.loginFormOtherDevice);
        _authProvider!.logout();
      }
    });
  }

  void listenMemberTransactionSuccessInStream() {
    memberTransactionSuccessInStream =
        GrpcUtil.eventMemberTransactionSuccessIn()?.listen((value) {
      log('$value');
      _memberProvider!.getMemberPoints();
      List getValue = value.value.split(': ');
      print(getValue[1]);
      delegate.push(name: RouteName.paymentResult, arguments: getValue[1]);
    });
  }

  void qrCodeIconOnTap() {
    if (LocalMember.memberId == 0) {
      _showNotLoggingDialog();
      return;
    }
    delegate.push(name: RouteName.memberQrcode);
  }


  //未登入視窗
  void _showNotLoggingDialog() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            alignment: Alignment.bottomRight,
            scale: anim1.value,
            child: NotLoggingDialog(
              key: UniqueKey(),
            ),
          );
        });
  }


  void newsLookOnTap(){
    setState(() {
      _selectPage = HomeEnum.news;
    });
  }

  void itemInfoLookOnTap(){
    setState(() {
      _selectPage = HomeEnum.itemInfo;
    });
  }

  void listenBusEvent(){
    bus.addListener(busEventName.transPages, (arg) {
      if(arg == busEventName.toNews){
        newsLookOnTap();
      }
      if(arg == busEventName.toItems){
        itemInfoLookOnTap();
      }

    });
  }
}
