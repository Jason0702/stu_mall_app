import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../models/ranking_models.dart';
import '../providers/record_provider.dart';
import '../save_model/record.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../widgets/app_widget.dart';

class DmRankingRoute extends StatefulWidget {
  const DmRankingRoute({Key? key}) : super(key: key);

  @override
  State<DmRankingRoute> createState() => _DmRankingRouteState();
}

class _DmRankingRouteState extends State<DmRankingRoute> {
  RecordProvider? _recordProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _recordProvider!.getPointRankings();
  }

  @override
  Widget build(BuildContext context) {
    _recordProvider = Provider.of<RecordProvider>(context);
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('點數排行榜', [_backButton()]),
          Expanded(
            child: Consumer<RecordProvider>(builder: (context, record, _) {
              return ListView.builder(
                  itemCount:  LocalRecord.rankings.length,
                  itemBuilder: (context, index) {
                    return _buildRankingList(
                        index, LocalRecord.rankings[index]);
                  });
            }),
          ),
        ],
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _buildRankingList(int index, RankingModel data) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
            width: 45,
            child: index < 3
                ? Container(
                    height: 60,
                    child:
                        Image.asset('assets/images/img_rank${index + 1}.png'))
                : Container(
                    alignment: Alignment.center,
                    height: 40,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColor.appMainColor,
                    ),
                    child: Text(
                      '${index + 1}',
                      textScaleFactor: 1,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  )),
        Expanded(
          flex: 3,
          child: FittedBox(
            alignment: Alignment.centerLeft,
            fit: BoxFit.scaleDown,
            child: Text(
              ' ${data.studentId} ',
              textScaleFactor: 1,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: FittedBox(
            alignment: Alignment.centerLeft,
            fit: BoxFit.scaleDown,
            child: Text(
              '${data.name}',
              textScaleFactor: 1,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.scaleDown,
              child: Text(
                '${data.point}',
                textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              )),
        ),
      ]),
    );
  }
}
