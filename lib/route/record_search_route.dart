import 'package:com.sj.stu_mall_app/enum/dm_record.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../enum/order.dart';
import '../enum/record_search_route.dart';
import '../models/order_model.dart';
import '../models/record_models.dart';
import '../providers/record_provider.dart';
import '../save_model/record.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/util.dart';
import '../widgets/app_widget.dart';
import '../widgets/dm_instruction_card.dart';
import '../widgets/point_card.dart';
import 'page/order_info.dart';

class RecordSearchRoute extends StatefulWidget {
  const RecordSearchRoute({Key? key}) : super(key: key);

  @override
  State<RecordSearchRoute> createState() => _RecordSearchRouteState();
}

class _RecordSearchRouteState extends State<RecordSearchRoute> {
  TitleBarEnum currentPage = TitleBarEnum.consumer;
  int select = AppEnum.income;

  RecordProvider? _recordProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _recordProvider!.getPointRecords();
    _recordProvider!.getOrderRecords();
  }

  @override
  Widget build(BuildContext context) {
    _recordProvider = Provider.of<RecordProvider>(context);
    return Column(
      children: [
        titleBarIntegrate('紀錄查詢', [_backButton()]),
        Expanded(child: _body())
      ],
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _body() {
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: [
          _titleSwitchBar(),
          if (currentPage == TitleBarEnum.exchange) _subTitle(),
          _pointWidget(),
          Expanded(child: _listView())
        ],
      ),
    );
  }

  //消費紀錄 兌換紀錄
  Widget _titleSwitchBar() {
    return Container(
      child: Row(children: [
        Expanded(child: _titleSwitchCard(TitleBarEnum.consumer)),
        Expanded(child: _titleSwitchCard(TitleBarEnum.exchange)),
      ]),
    );
  }

  Widget _titleSwitchCard(TitleBarEnum set) {
    return GestureDetector(
      onTap: () {
        currentPage = set;
        select = AppEnum.income;
        setState(() {});
      },
      child: Container(
        alignment: Alignment.center,
        height: 50,
        color: Colors.white,
        child: Text(
          set.label,
          textScaleFactor: 1,
          style: TextStyle(
              color: currentPage == set ? Colors.red : Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
      ),
    );
  }

  //會員點數
  Widget _pointWidget() {
    return Container(
      height: 110,
      margin: EdgeInsets.symmetric(
          vertical: 10, horizontal: AppSize.defaultPadding),
      width: AppSize.appWidth,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: AppColor.buttonOrange, width: 3)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //會員點數
          PointsCard(),
          //使用說明
          DmInstructionCard(),
        ],
      ),
    );
  }

  //已獲得 已使用
  Widget _subTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [AppColor.shadow],
        color: Colors.white,
      ),
      child: Row(children: [
        _subTitleCard(AppEnum.income),
        _subTitleCard(AppEnum.outcome),
      ]),
    );
  }

  Widget _subTitleCard(int set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            set == AppEnum.income ? "已獲得" : "已使用",
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _subTitleCardOnTap(int set) {
    select = set;
    setState(() {});
  }

  Widget _listView() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: currentPage == TitleBarEnum.consumer
            ? _orderListView()
            : _pointListView());
  }

  //訂單列表
  Widget _orderListView() {
    return ListView.builder(
        itemCount: LocalRecord.takeOrders.length,
        itemBuilder: (context, index) {
          return _buildPayRecord(LocalRecord.takeOrders[index]);
        });
  }

  Widget _buildPayRecord(OrderModel data) {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        padding: EdgeInsets.symmetric(
            horizontal: AppSize.defaultPadding, vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('交易編號 :${data.orderNumber!}', style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 5,
            ),
            Text(
                '交易日期 :${Util.dateFormatHHMM.format(Util().utcTimeToLocal(data.createdAt ?? ' '))}',
                style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 5,
            ),
            Text('付款方式 :${OrderPaymentEnum.values[data.paymentType!].label}',
                style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Text('交易金額 :', style: TextStyle(fontSize: 20)),
                Text('${data.payAmount}',
                    style: TextStyle(fontSize: 20, color: Colors.red)),
                const Spacer(),
                GestureDetector(
                    onTap: () => showOrderInfo(data),
                    child: Container(
                      child: Text(
                        '消費明細 >',
                        textScaleFactor: 1,
                        style: TextStyle(
                            color: AppColor.appMainColor, fontSize: 20),
                      ),
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }

  void showOrderInfo(OrderModel data) {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: OrderInfoPage(
              data: data,
              key: UniqueKey(),
            ),
          );
        });
  }

  //點數列表
  Widget _pointListView() {
    return ListView.builder(
        itemCount: _recordProvider!.getPointRecordList(select).length,
        itemBuilder: (context, index) {
          return _buildExchangeRecord(
              _recordProvider!.getPointRecordList(select)[index]);
        });
  }

  Widget _buildExchangeRecord(RecordModel set) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.white,
      ),
      margin:
          EdgeInsets.symmetric(vertical: 5, horizontal: AppSize.defaultPadding),
      padding: EdgeInsets.symmetric(
          vertical: 10, horizontal: AppSize.defaultPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(width: 60, child: Image.asset(AppImage.iconDmDollar)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${set.operation!.pointLabels}",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                  ),
                  Text(
                    '${Util.dateFormatHHMM.format(Util().utcTimeToLocal(set.createdAt ?? ' '))}',
                    style: TextStyle(fontSize: 17),
                  )
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            child: Row(children: [
              Container(
                child: Text(
                  set.types == AppEnum.income ? '+' : '-',
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: set.types == AppEnum.income
                          ? Colors.red
                          : Colors.black,
                      fontSize: 17),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "${set.point}",
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: set.types == AppEnum.income
                          ? Colors.red
                          : Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 23),
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
