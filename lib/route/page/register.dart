import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../../dialog/privacy_policies_dialog.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/text_filed_title.dart';
import '../../widgets/text_from_filed.dart';
class RegisterPage extends StatefulWidget {
  @override
  State createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _account = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _checkPassword = TextEditingController();
  TextEditingController _phone = TextEditingController();
  bool _checkboxSelected = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
              child: buildSingInTextForm(),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildSingInTextForm() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.transparent,
      ),
      child: Column(
        children: [
          TitleHasSign('帳號'),
          StudentIdTextField(
            controller: _account,
          ),
          TitleHasSign('密碼'),
          PasswordTextField(
            controller: _password,
          ),
          TitleHasSign('確認密碼'),
          PasswordTextField(
            controller: _checkPassword,
          ),
          TitleHasSign('行動電話'),
          AccountTextField(
            controller: _phone,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Checkbox(
                      value: _checkboxSelected,
                      activeColor: AppColor.darkBlue,
                      checkColor: Colors.white,
                      onChanged: (value) {
                        setState(() {
                          _checkboxSelected = value!;
                        });
                      }),
                  Container(
                    child: Text(
                      '我同意無人商店',
                      textScaleFactor: 1,
                      style: TextStyle(color: AppColor.darkBlue, fontSize: 17),
                    ),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () => _privacyPoliciesOnTap(),
                child: Text(
                  '隱私權政策',
                  textScaleFactor: 1,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 17),
                ),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: _registerOnTap,
            child:
                Container(width: AppSize.appWidth, child: buttonWidget('確認註冊')),
          )
        ],
      ),
    );
  }

  Future<void> _privacyPoliciesOnTap() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: PrivacyPoliciesDialog(
              key: UniqueKey(),
            ),
          );
        });
    _checkboxSelected = result ?? false;
    setState(() {});
  }

  void _registerOnTap() {
    EasyLoading.showError("請至樹科大註冊");
    //Navigator.pushNamed(context, '/Home');
  }
}
