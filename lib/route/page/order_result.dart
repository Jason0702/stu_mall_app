import 'dart:convert';

import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:com.sj.stu_mall_app/widgets/shopping_cart_step.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../enum/order.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';

class OrderResultPage extends StatefulWidget {
  @override
  State createState() => _OrderResultPageState();
}

class _OrderResultPageState extends State<OrderResultPage>
    with WidgetsBindingObserver {
  bool orderStatus = false;
  String orderNumber = '';
  DeliveryTypeEnum deliverFee = DeliveryTypeEnum.self;

  _onLayoutDone(_) {}

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _map =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    orderStatus = _map['orderStatus'];
    orderNumber = _map['orderNumber'];
    deliverFee = _map['delivery_option'];
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            titleBarIntegrate('完成結帳', [], [_backButton()]),
            //Body
            Expanded(flex: 9, child: _bodyWidget()),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popUntilPage(name: RouteName.home);
      },
      child: backButtonClearWidget(),
    );
  }

  //body
  Widget _bodyWidget() {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          ShoppingCartStep(),
          Container(
            padding: EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            child: Text(
              orderStatus == true ? '完成結帳' : '購買失敗',
              textScaleFactor: 1,
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            alignment: Alignment.center,
            child: Text(
              orderStatus == true
              ? '${deliverFee.label}'
              : '',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 22, color: Colors.black),
            ),
          ),
          Container(
            width: 130,
            margin: EdgeInsets.only(top: 20, bottom: 20),
            child: Image.asset(orderStatus == true
                ? AppImage.iconSuccess
                : AppImage.iconFailed),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              orderStatus == true
                  ? '消費編號${jsonDecode(orderNumber)}'
                  : '請重新選購商品',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 22, color: Colors.black),
            ),
          ),
          Container(
              padding: EdgeInsets.only(top: 20),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  //繼續購物
                  InkWell(
                    onTap: _continueShopping,
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 20, right: 20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: AppColor.shoppingCartActivity),
                      child: Text(
                        '< 繼續購物',
                        textScaleFactor: 1,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                  //查看紀錄
                  if (orderStatus)
                    InkWell(
                      onTap: _viewRecordsOnTap,
                      child: Container(
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, left: 20, right: 20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColor.textRedColor),
                        child: Text(
                          '查看紀錄 >',
                          textScaleFactor: 1,
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                ],
              ))
        ],
      ),
    );
  }

  //繼續購物
  void _continueShopping() {
    delegate.popUntilPage(name: RouteName.home);
  }

  //查看紀錄
  void _viewRecordsOnTap() {
    delegate.popUntilPage(name: RouteName.home);
    delegate.push(name: RouteName.myOrderRoute);
  }
}
