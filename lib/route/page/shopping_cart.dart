import 'package:com.sj.stu_mall_app/providers/product_provider.dart';
import 'package:com.sj.stu_mall_app/providers/shopping_cart_provider.dart';
import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../dialog/suggest_product_list_dialog.dart';
import '../../enum/shopping_cart.dart';
import '../../models/products_model.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/item_card_horizontal.dart';
import '../../widgets/shopping_cart_step.dart';

class ShoppingCartPage extends StatefulWidget {
  const ShoppingCartPage({Key? key}) : super(key: key);

  @override
  State<ShoppingCartPage> createState() => _ShoppingCartPageState();
}

class _ShoppingCartPageState extends State<ShoppingCartPage> {
  ShoppingCartProvider? _shoppingCartProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _shoppingCartProvider =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _shoppingCartProvider!.changeShoppingCartStepEnum(ShoppingCartStepEnum.selectQty);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('選定數量', [_backButton()]),
          ShoppingCartStep(),
          Expanded(child: _shoppingCartList()),
          _bottomTotalBar(),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _shoppingCartList() {
    return Container(
      color: Colors.grey[200],
      child: Consumer<ShoppingCartProvider>(builder: (context, s, _) {
        return ListView.builder(
            shrinkWrap: true,
            itemCount: s.memberCart.length,
            itemBuilder: (context, index) {
              return ItemCardHorizontal(
                data: s.memberCart[index],
              );
            });
      }),
    );
  }

  Widget _bottomTotalBar() {
    return Consumer<ShoppingCartProvider>(builder: (context, s, _) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Column(
              children: [
                Text(
                  '共${s.memberCart.length}件商品',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  '小計',
                  textScaleFactor: 1,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '\$${s.totalMoney}',
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const Spacer(),
            Column(
              children: [
                Row(
                  children: [
                    Text(
                      '小計',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Text(
                      '\$${s.totalMoney}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '總計',
                      style: TextStyle(
                          fontSize: 18,
                          color: AppColor.appMainColor,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Text(
                      '\$${s.totalMoney}',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                nextStep(),
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget nextStep() {
    return Container(
      child: InkWell(
        onTap: () => nextStepOnTap(),
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 30, left: 30),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: AppColor.textRedColor,
          ),
          child: Text(
            '下一步',
            textScaleFactor: 1,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }

  void nextStepOnTap() {
    if(_shoppingCartProvider!.memberCart.isEmpty){
      EasyLoading.showToast(EasyLoadingMessage.selectItems);
      return;
    }
    List<ProductsModel> data = _shoppingCartProvider!.checkProductIsEnough();
    if (data.isNotEmpty) {
      showSuggestList(data);
      return;
    }
    delegate.push(name: RouteName.selectPayment);
    _shoppingCartProvider!
        .changeShoppingCartStepEnum(ShoppingCartStepEnum.orderInfo);
  }

  void showSuggestList(List<ProductsModel> data) {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: SuggestDialog(
              key: UniqueKey(),
              data: data,
            ),
          );
        });
  }
}
