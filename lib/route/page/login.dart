import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../dialog/privacy_policies_dialog.dart';
import '../../providers/auth_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/error_message.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/text_from_filed.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _account = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool _checkboxSelected = false;
  bool _checkPrivacyPolicies = false;
  final _formKey = GlobalKey<FormState>();

  _onLayoutDone(_) {}
  AuthProvider? _authProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _authProvider ??= Provider.of<AuthProvider>(context);
    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: ScrollConfiguration(
            behavior: NoShadowScrollBehavior(),
            child: SingleChildScrollView(
              child: Container(
                padding:
                    EdgeInsets.only(top: 23, left: 23, right: 23, bottom: 0),
                child: buildSingInTextForm(),
              ),
            ),
          ),
        ));
  }

  Widget buildSingInTextForm() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.transparent,
      ),
      child: Form(
        key: _formKey,
        child: Column(children: [
          SizedBox(
            height: 5,
          ),
          _cardTitle('帳號'),
          StudentIdTextField(controller: _account),
          SizedBox(
            height: 5,
          ),
          _cardTitle('密碼'),
          PasswordTextField(controller: _password),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Checkbox(
                      value: _checkboxSelected,
                      activeColor: AppColor.darkBlue,
                      checkColor: Colors.white,
                      onChanged: (value) {
                        setState(() {
                          _checkboxSelected = value!;
                        });
                      }),
                  Container(
                    child: Text(
                      '記住帳號',
                      textScaleFactor: 1,
                      style: TextStyle(color: AppColor.darkBlue, fontSize: 20),
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: _forgotPasswordOnTap,
                child: Row(
                  children: [
                    Icon(
                      Icons.help,
                      size: 25,
                      color: AppColor.textRedColor,
                    ),
                    Text(
                      '忘記密碼?',
                      textScaleFactor: 1,
                      style: TextStyle(
                        color: AppColor.textRedColor,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Checkbox(
                  value: _checkPrivacyPolicies,
                  activeColor: AppColor.darkBlue,
                  checkColor: Colors.white,
                  onChanged: (value) {
                    setState(() {
                      _checkPrivacyPolicies = value!;
                      setState(() {});
                    });
                  }),
              Text('我同意', textScaleFactor: 1, style: TextStyle(fontSize: 20)),
              GestureDetector(
                  onTap: () => _privacyPoliciesOnTap(),
                  child: Text(
                    '隱私權政策',
                    textScaleFactor: 1,
                    style: TextStyle(color: Colors.blueAccent, fontSize: 20),
                  ))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () => _loginOnTap(),
            child:
                Container(width: AppSize.appWidth, child: buttonWidget('登入')),
          )
        ]),
      ),
    );
  }

  //登入觸發事件
  void _loginOnTap() {
    if (!_formKey.currentState!.validate()) {
      EasyLoading.showToast(EasyLoadingMessage.formError);
      return;
    }
    if (EasyLoading.isShow) {
      return;
    }
    if (!_checkPrivacyPolicies) {
      EasyLoading.showError('請同意隱私權政策');
      return;
    }
    _authProvider!.memberLogin(_account.text, _password.text);
  }

  void _forgotPasswordOnTap() {
    delegate.push(name: RouteName.forgetPassword);
  }

  Widget _cardTitle(String data) {
    return Container(
      alignment: Alignment.centerLeft,
      child: RichText(
          text: TextSpan(
            text: data,
            style: TextStyle(
                fontSize: 19,
                color: AppColor.darkBlue,
                fontWeight: FontWeight.bold),
          )),
    );
  }

  Future<void> _privacyPoliciesOnTap() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: PrivacyPoliciesDialog(
              key: UniqueKey(),
            ),
          );
        });
    _checkPrivacyPolicies = result ?? false;
    setState(() {});
  }
}
