import 'package:com.sj.stu_mall_app/models/hero_view_params.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../utils/app_size.dart';
import '../../widgets/app_widget.dart';

class OrderQrcode extends StatelessWidget{
  final HeroViewParamsModel paramsModel;
  const OrderQrcode({Key? key, required this.paramsModel}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: Container(
            color: Colors.white,
            child: Column(
              children: [
                titleBarIntegrate('訂單QRCode',[_backButton(context)]),
                //Body
                Expanded(flex: 11, child: Hero(
                  tag: paramsModel.tag,
                  child: _bodyWidget(),
                ))
              ],
            )
        ),
      ),
    );
  }

  Widget _backButton(BuildContext context){
    return GestureDetector(
      onTap: (){
        Navigator.pop(context);
      },
      child: backButtonWidget(),
    );
  }

  Widget _bodyWidget(){
    return Container(
      color: Colors.white,
      width: AppSize.appWidth,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 300,
            height: 300,
            child: QrImage(
              data: paramsModel.data,
              version: QrVersions.auto,
              size: 200.0,
            ),
          )
        ],
      ),
    );
  }

}
