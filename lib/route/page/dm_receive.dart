import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:flutter/material.dart';

import '../../save_model/member.dart';
import '../../utils/app_color.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/text_from_filed.dart';

class DmReceivePage extends StatefulWidget {
  const DmReceivePage({Key? key}) : super(key: key);

  @override
  State<DmReceivePage> createState() => _DmReceivePageState();
}

class _DmReceivePageState extends State<DmReceivePage> {
  TextEditingController _name = TextEditingController();
  TextEditingController _studentId = TextEditingController();
  TextEditingController _money = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _studentId.text = LocalMember.memberModel?.studentId ?? '';
    _name.text = LocalMember.memberModel?.name ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding * 2),
          child: Column(
            children: [
              _cardTitle('收款方'),
              NickNameTextField(
                controller: _name,
              ),
              _cardTitle('會員ID'),
              _accountTextField(_studentId),
              _cardTitle('收款金額'),
              MoneyTextField(
                controller: _money,
              ),
              const Spacer(),
              _buttonWidget(),
              const Spacer(flex: 4,),
            ],
          ),
        ),
      ),
    );
  }

  Widget _accountTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      readOnly: true,
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入學號",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入學號";
        }
        return null;
      },
    );
  }

  Widget _cardTitle(String data) {
    return Container(
      alignment: Alignment.centerLeft,
      child: RichText(
          textScaleFactor: 1,
          text: TextSpan(
            text: data,
            style: TextStyle(
                fontSize: 19,
                color: AppColor.darkBlue,
                fontWeight: FontWeight.bold),
          )),
    );
  }

  Widget _buttonWidget() {
    return GestureDetector(
      onTap: () => _buttonWidgetOnTap(),
      child: buttonWidget('確定'),
    );
  }

  void _buttonWidgetOnTap() {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    Map<String, dynamic> map = {
      'id': _studentId.text.toString(),
      'name': _name.text.toString(),
      'money':_money.text.toString(),
    };
    delegate.push(name: RouteName.qrCodeRoute,arguments: map);
  }
}
