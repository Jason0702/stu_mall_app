import 'dart:async';
import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:com.sj.stu_mall_app/enum/app_enum.dart';
import 'package:com.sj.stu_mall_app/utils/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../../api/api.dart';
import '../../app.dart';
import '../../models/products_type_model.dart';
import '../../providers/game_provider.dart';
import '../../providers/member_provider.dart';
import '../../providers/news_provider.dart';
import '../../providers/position_provider.dart';
import '../../providers/product_provider.dart';
import '../../providers/util_provider.dart';
import '../../routers/route_name.dart';
import '../../save_model/news.dart';
import '../../save_model/product.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../utils/util.dart';
import '../../widgets/dm_instruction_card.dart';
import '../../widgets/item_card.dart';
import '../../widgets/news_card.dart';
import '../../widgets/point_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController? _tabController;
  UtilProvider? _utilProvider;
  PositionProvider? _positionProvider;
  String timestamp = "";
  String temp = "";
  Timer? _timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    timestamp = Util.dateFormatHHMM.format(DateTime.now());
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) async {
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) => getTime());
    _positionProvider!.checkPosition();
    _utilProvider!.getWeather();
    _positionProvider!.addListener(getTemp);
    if(Api.forceUpdatePassword){
      EasyLoading.showSuccess('將跳至變更密碼畫面，請更新您的密碼');
      delegate.push(name: RouteName.personalDataPage);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _positionProvider!.removeListener(getTemp);
    _timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    _utilProvider = Provider.of<UtilProvider>(context);
    _positionProvider = Provider.of<PositionProvider>(context);
    if(_tabController == null || _tabController?.length != LocalProduct.productsTypes.length){
      _tabController = TabController(length: LocalProduct.productsTypes.length, vsync: this);
    }
    return Scaffold(
        backgroundColor: Colors.grey[200],
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(), child: _buildBodyWidget()),
        ));
  }

  //全部
  Widget _buildBodyWidget() {
    return Container(
      child: DefaultTabController(
        length: LocalProduct.productsTypes.length,
        child: NestedScrollView(
            physics: NeverScrollableScrollPhysics(),
            headerSliverBuilder: (context, _) {
              return <Widget>[
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      //會員點數與天氣
                      _titleWidget(),
                      //Banner
                      _bannerWidget(),
                      //活動訊息
                      _eventMessageWidget(),
                      //商品資訊
                      _commodityInformation(),
                    ],
                  ),
                ),
              ];
            },
            body: TabBarView(
              children:
                  LocalProduct.productsTypes.map((e) => mTableView(e)).toList(),
              controller: _tabController,
              physics: ScrollPhysics(),
            )),
      ),
    );
  }

  //商品資訊
  Widget _commodityInformation() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: AppSize.defaultPadding, vertical: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(25),
                        bottomRight: Radius.circular(25)),
                    color: AppColor.buttonOrange),
                child: Text(
                  '商品資訊',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 19,
                      fontWeight: FontWeight.bold),
                ),
              ),
              InkWell(
                onTap: ()=>sendToItemsBus(),
                child: Container(
                  margin: EdgeInsets.only(right: AppSize.defaultPadding),
                  child: Text(
                    '看更多 >>',
                    textScaleFactor: 1,
                    style: TextStyle(fontSize: 19),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 45,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, boxShadow: [AppColor.shadow]),
              child: TabBar(
                isScrollable: true,
                tabs: LocalProduct.productsTypes
                    .map((e) => Tab(
                          child: Text(
                            '${e.name}',
                            style: TextStyle(fontSize: 19),
                          ),
                        ))
                    .toList(),
                controller: _tabController,
                indicatorColor: AppColor.textRedColor,
                labelColor: AppColor.textRedColor,
                unselectedLabelColor: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  //活動訊息
  Widget _eventMessageWidget() {
    return Consumer<NewsProvider>(builder: (context, news, _) {
      return Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppSize.defaultPadding, vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(25),
                          bottomRight: Radius.circular(25)),
                      color: AppColor.buttonOrange),
                  child: Text(
                    '活動訊息',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 19,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                InkWell(
                  onTap: ()=>sendToNewsBus(),
                  child: Container(
                    margin: EdgeInsets.only(right: AppSize.defaultPadding),
                    child: Text(
                      '看更多 >>',
                      textScaleFactor: 1,
                      style: TextStyle(fontSize: 19),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              //確定是否有新聞資料
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                LocalNew.newsModel.length > 0
                    ? NewsCard(LocalNew.newsModel[0])
                    : Container(
                        width: AppSize.appWidth / 2,
                      ),
                LocalNew.newsModel.length > 1
                    ? NewsCard(LocalNew.newsModel[1])
                    : Container(
                        width: AppSize.appWidth / 2,
                      ),
              ],
            )
          ],
        ),
      );
    });
  }

  //Banner
  Widget _bannerWidget() {
    return Consumer<NewsProvider>(builder: (context, news, _) {
      return Container(
        height: 220,
        child: Swiper(
          itemBuilder: (BuildContext context, int index) {
            return Image(
              image: CachedNetworkImageProvider(
                  Util().getPhotoUrl(LocalNew.carouselsModel[index].body!)),
              fit: BoxFit.fill,
            );
          },
          itemCount: LocalNew.carouselsModel.length,
          autoplay: true,
        ),
      );
    });
  }

  Widget mTableView(ProductsTypeModel type) {
    return LocalProduct.products
            .any((element) => element.productsTypeId == type.id)
        ? GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 0.87),
            itemCount: LocalProduct.products
                .where((element) => element.productsTypeId == type.id)
                .length,
            itemBuilder: (context, index) {
              return ItemCard(LocalProduct.products
                  .where((element) => element.productsTypeId == type.id)
                  .toList()[index]);
            })
        : Container(
            alignment: Alignment.center,
            width: AppSize.appWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.remove_shopping_cart,
                  color: Colors.grey,
                  size: 60,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "此分頁沒有商品",
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                ),
              ],
            ),
          );
  }

  //會員點數與天氣
  Widget _titleWidget() {
    return Container(
      height: 130,
      margin: EdgeInsets.symmetric(
          vertical: 10, horizontal: AppSize.defaultPadding),
      width: AppSize.appWidth,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: AppColor.buttonOrange, width: 3)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //會員點數
          PointsCard(),
          //使用說明
          DmInstructionCard(),
          Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.grey[200],
          ),
          //天氣
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //日期 時間
                Container(
                  child: Text(
                    timestamp,
                    style: TextStyle(fontSize: 19),
                  ),
                ),
                //區域 天氣狀況
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        '${_positionProvider!.locality}',
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 17),
                      ),
                    ),
                  ],
                ),
                //溫度
                temp.isNotEmpty
                    ? Container(
                        child: Column(
                          children: [
                            Text(
                              '$temp °C',
                              textScaleFactor: 1,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )
        ],
      ),
    );
  }

  //取得現在時間
  void getTime() {
    if (mounted) {
      setState(() {
        timestamp = Util.dateFormatHHMM.format(DateTime.now());
      });
    }
  }

  void getTemp() {
    temp = _utilProvider!
        .getWeatherArea(_positionProvider!.city, _positionProvider!.locality);
  }


  void sendToNewsBus(){
    bus.sendBroadcast(busEventName.transPages,busEventName.toNews);
  }

  void sendToItemsBus(){
    bus.sendBroadcast(busEventName.transPages,busEventName.toItems);
  }
}
