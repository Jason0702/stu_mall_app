import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:com.sj.stu_mall_app/providers/util_provider.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:com.sj.stu_mall_app/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../enum/order.dart';
import '../../enum/shopping_cart.dart';
import '../../models/turple_model.dart';
import '../../providers/shopping_cart_provider.dart';
import '../../routers/route_name.dart';
import '../../save_model/system_setting.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/shopping_cart_step.dart';

class SelectPaymentPage extends StatefulWidget {
  const SelectPaymentPage({Key? key}) : super(key: key);

  @override
  State<SelectPaymentPage> createState() => _SelectPaymentPageState();
}

class _SelectPaymentPageState extends State<SelectPaymentPage> {
  ShoppingCartProvider? _shoppingCartProvider;
  OrderPaymentEnum payMethod = OrderPaymentEnum.dmCoin;
  DeliveryTypeEnum deliverFee = DeliveryTypeEnum.self;
  late UtilProvider _utilProvider;
  MemberProvider? _memberProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _utilProvider.getSystemSetting();
  }

  @override
  Widget build(BuildContext context) {
    _shoppingCartProvider =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    _utilProvider = Provider.of<UtilProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    return WillPopScope(
      onWillPop: () => pop(),
      child: Scaffold(
        body: Column(
          children: [
            titleBarIntegrate('付款資訊', [_backButton()]),
            ShoppingCartStep(),
            Expanded(child: _body()),
            _bottomTotalBar(),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  Widget _body() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _topTotalBar(),
              titleCard('付款方式'),
              _paymentCard(OrderPaymentEnum.dmCoin),
              _paymentCard(OrderPaymentEnum.linePay),
              _paymentCard(OrderPaymentEnum.credit),
              titleCard('運送方式'),
              _deliverCard(DeliveryTypeEnum.self),
              _deliverCard(DeliveryTypeEnum.cabinet),
              _deliverCard(DeliveryTypeEnum.store),
              _deliverCard(DeliveryTypeEnum.normal),
            ],
          ),
        ));
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        _shoppingCartProvider!
            .changeShoppingCartStepEnum(ShoppingCartStepEnum.selectQty);
        _shoppingCartProvider!.changeIsImmediately(false);
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _topTotalBar() {
    return Consumer<ShoppingCartProvider>(builder: (context, s, _) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '共${s.memberCart.length}件商品',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                //小計
                Row(
                  children: [
                    Text(
                      '小計',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                    Container(
                      width: 100,
                      alignment: Alignment.centerRight,
                      child: Text(
                        '\$${s.totalMoney}',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                //運費
                Row(
                  children: [
                    Text(
                      '運費',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                    Container(
                      width: 100,
                      alignment: Alignment.centerRight,
                      child: Text(
                        '\$${_shoppingCartProvider!.getDeliverFee(deliverFee)}',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                //總計
                Row(
                  children: [
                    Text(
                      '總計',
                      style: TextStyle(
                          fontSize: 18,
                          color: AppColor.appMainColor,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: 100,
                      alignment: Alignment.centerRight,
                      child: Text(
                        '\$${s.totalMoney + _shoppingCartProvider!.getDeliverFee(deliverFee)}',
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget _bottomTotalBar() {
    return Consumer<ShoppingCartProvider>(builder: (context, s, _) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Column(
              children: [
                Text(
                  '總付款金額',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '\$${s.totalMoney + _shoppingCartProvider!.getDeliverFee(deliverFee)}',
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const Spacer(),
            nextStep(s.totalMoney +
                _shoppingCartProvider!.getDeliverFee(deliverFee)),
          ],
        ),
      );
    });
  }

  Widget _paymentCard(OrderPaymentEnum set) {
    return GestureDetector(
      onTap: () => _paymentCardOnTap(set),
      child: Container(
        height: 60,
        margin: EdgeInsets.symmetric(
            horizontal: AppSize.defaultPadding, vertical: 5),
        decoration: BoxDecoration(
            // 裝飾內裝元件
            color: _shoppingCartProvider!.getOpenStatus(set)
                ? Colors.white
                : Colors.grey.shade200,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            border: Border.all(
              width: 2,
              color: payMethod == set
                  ? AppColor.shoppingCartActivity
                  : AppColor.shoppingCartNone,
            )),
        padding: EdgeInsets.only(left: 5, top: 5, right: 100, bottom: 5),
        child: Row(children: [
          Image.asset(set.image),
          Text(
            " ${set.showLabel}",
            textScaleFactor: 1,
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          )
        ]),
      ),
    );
  }

  void _paymentCardOnTap(OrderPaymentEnum set) {
    if (!_shoppingCartProvider!.getOpenStatus(set)) {
      EasyLoading.showToast(EasyLoadingMessage.noOpen);
      return;
    }
    payMethod = set;
    setState(() {});
  }

  Widget _deliverCard(DeliveryTypeEnum set) {
    return GestureDetector(
      onTap: () => _deliverCardOnTap(set),
      child: Container(
        height: 60,
        margin: EdgeInsets.symmetric(
            horizontal: AppSize.defaultPadding, vertical: 5),
        decoration: BoxDecoration(
            // 裝飾內裝元件
            color: _shoppingCartProvider!.getDeliveryOpenStatus(set)
                ? Colors.white
                : Colors.grey.shade200,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            border: Border.all(
              width: 2,
              color: deliverFee == set
                  ? AppColor.shoppingCartActivity
                  : AppColor.shoppingCartNone,
            )),
        padding: EdgeInsets.only(left: 5, top: 5, right: 100, bottom: 5),
        child: Row(children: [
          Text(
            " ${set.label}",
            textScaleFactor: 1,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 30,
          ),
          Text(
            "\$${_shoppingCartProvider!.getDeliverFee(set)}",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          )
        ]),
      ),
    );
  }

  void _deliverCardOnTap(DeliveryTypeEnum set) {
    if (!_shoppingCartProvider!.getDeliveryOpenStatus(set)) {
      EasyLoading.showToast(EasyLoadingMessage.noOpen);
      return;
    }
    deliverFee = set;
    setState(() {});
  }

  Widget titleCard(String title) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 5, left: AppSize.defaultPadding),
          height: 20,
          width: 5,
          color: AppColor.textRedColor,
        ),
        Container(
          child: Text(
            '$title',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
        ),
      ],
    );
  }

  Future<bool> pop() async {
    return true;
  }

  Widget nextStep(int money) {
    return Container(
      child: InkWell(
        onTap: () => nextStepOnTap(money),
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 30, left: 30),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: AppColor.textRedColor,
          ),
          child: Text(
            '下一步',
            textScaleFactor: 1,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }

  Future<void> nextStepOnTap(int totalMoney) async {
    if (LocalMember.dmCoin < totalMoney) {
      EasyLoading.showToast(EasyLoadingMessage.coinNotEnough);
      return;
    }
    if ((LocalMember.memberModel?.cellphone ?? '').isEmpty) {
      EasyLoading.showToast(EasyLoadingMessage.setCellphone);
      return;
    }
    if ((LocalMember.memberModel?.name ?? '').isEmpty) {
      EasyLoading.showToast(EasyLoadingMessage.setName);
      return;
    }
    if(EasyLoading.isShow){
      return;
    }
    EasyLoading.show();
    Tuple<bool, String> result = await _shoppingCartProvider!.postOrder(totalMoney, payMethod, deliverFee);
    if (result.i1 ?? true) {
      _memberProvider!.getMemberPoints();
    }
    Map<String,dynamic> _map = {
      'orderStatus' : result.i1 ?? false,
      'orderNumber' : result.i2 ?? '',
      'delivery_option': deliverFee
    };
    EasyLoading.dismiss();
    _shoppingCartProvider!.changeShoppingCartStepEnum(ShoppingCartStepEnum.finish);
    delegate.push(name: RouteName.orderResultPage,arguments: _map);
  }
}
