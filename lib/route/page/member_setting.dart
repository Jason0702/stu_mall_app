import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/enum/role.dart';
import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:com.sj.stu_mall_app/utils/app_color.dart';
import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../dialog/confirm_dialog.dart';
import '../../enum/setting_page.dart';
import '../../providers/auth_provider.dart';
import '../../routers/route_name.dart';
import '../../save_model/product.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../widgets/item_card.dart';
import '../../widgets/point_card.dart';

class MemberSettingPage extends StatefulWidget {
  const MemberSettingPage({Key? key}) : super(key: key);

  @override
  State<MemberSettingPage> createState() => _MemberSettingPageState();
}

class _MemberSettingPageState extends State<MemberSettingPage> {
  AuthProvider? _authProvider;

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
              child: ScrollConfiguration(
            behavior: NoShadowScrollBehavior(),
            child: CustomScrollView(slivers: <Widget>[
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    memberCard(),
                    //會員幣
                    Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        child: PointsCard(mainAxis: MainAxisAlignment.end)),
                    //功能列表
                    _functionTable(),
                    SizedBox(
                      height: 10,
                    ),
                    //猜您也會喜歡
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        '猜您也會喜歡',
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 19),
                      ),
                    ),
                  ],
                ),
              ),
              SliverGrid(
                  delegate: SliverChildBuilderDelegate((context, index) {
                    return ItemCard(
                      LocalProduct.products[index],
                      showMoney: false,
                    );
                  }, childCount: LocalProduct.products.length),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 0.87))
            ]),
          )),
        ],
      ),
    );
  }

  //功能表
  Widget _functionTable() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          Row(
            children: [
              _functionButton(SettingEnum.order),
              _functionButton(SettingEnum.check),
              _functionButton(SettingEnum.myDm),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              _functionButton(SettingEnum.record),
              _functionButton(SettingEnum.dmRanking),
              _functionButton(SettingEnum.game),
            ],
          ),
        ],
      ),
    );
  }

  //功能按鈕
  Widget _functionButton(SettingEnum set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _functionButtonOnTap(set),
        child: Column(
          children: [
            Container(
              child: Image.asset(
                set.icon,
                width: 50,
              ),
            ),
            Container(
              child: Text(
                set.label,
                style: TextStyle(fontSize: 17, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //功能按鈕事件
  void _functionButtonOnTap(SettingEnum set) {
    switch (set) {
      case SettingEnum.order:
        delegate.push(name: RouteName.myOrderRoute);
        break;
      case SettingEnum.check:
        delegate.push(name: RouteName.pointsRoute);
        break;
      case SettingEnum.myDm:
        delegate.push(name: RouteName.myDMRoute);
        break;
      case SettingEnum.record:
        delegate.push(name: RouteName.recordSearchRoute);
        break;
      case SettingEnum.dmRanking:
        delegate.push(name: RouteName.dmRankingRoute);
        break;
      case SettingEnum.game:
        delegate.push(name: RouteName.gameRouter);
        break;
    }
  }

  //會員資料
  Widget memberCard() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      color: AppColor.appMainColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(
            flex: 2,
          ),
          Icon(
            Icons.account_circle,
            color: Colors.white,
            size: 60,
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(onTap: () {
                    delegate.push(name: RouteName.personalDataPage);
                  }, child: Consumer<MemberProvider>(
                    builder: (context, s, _) {
                      return Text(
                        '${LocalMember.nickName}',
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 19,
                            decoration: TextDecoration.underline),
                      );
                    },
                  )),
                  Text(
                    ",  您好",
                    style: TextStyle(color: Colors.white, fontSize: 19),
                  ),
                ],
              ),
              Text(
                '(${LocalMember.memberModel?.role?.label})',
                textScaleFactor: 1,
                style: TextStyle(fontSize: 19, color: Colors.white),
              ),
            ],
          ),
          const Spacer(),
          GestureDetector(
            onTap: () => _logOut('確認登出?'),
            child: smallButtonWidget('登出'),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
              onTap: () => _logOut('確認刪除帳號?'), child: delButtonWidget('刪除')),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }

  ///小按鈕
  Widget delButtonWidget(String value) {
    return Container(
        width: 70,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 7),
        decoration: BoxDecoration(
            color: AppColor.textRedColor,
            border: Border.all(width: 1, color: AppColor.textRedColor),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [AppColor.shadow]),
        child: FittedBox(
          child: Text(
            value,
            style: const TextStyle(
                color: AppWidgetSetting.buttonTextColor,
                fontSize: 18,
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  Future<void> _logOut(String hint) async {
    bool result = await _confirmDialog(hint);
    if (result) {
      _authProvider!.logout();
    }
  }

  ///確認視窗
  Future<bool> _confirmDialog(String data) async {
    dynamic _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: ConfirmDialog(
              key: UniqueKey(),
              text: data,
            ),
          );
        });
    if (_result != null && (_result as bool)) {
      return true;
    } else {
      return false;
    }
  }
}
