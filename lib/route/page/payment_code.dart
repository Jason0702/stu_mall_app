
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../../app.dart';
import '../../enum/order.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';

class PaymentCodePage extends StatefulWidget {
  const PaymentCodePage({Key? key}) : super(key: key);

  @override
  State<PaymentCodePage> createState() => _PaymentCodePageState();
}

class _PaymentCodePageState extends State<PaymentCodePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding,vertical: 10),
            child: Text(
              '請選擇付款方式',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 20),
            ),
          ),
          _paymentCard(OrderPaymentEnum.dmCoin),
          _paymentCard(OrderPaymentEnum.linePay),
          _paymentCard(OrderPaymentEnum.credit),
        ],
      ),
    );
  }

  Widget _paymentCard(OrderPaymentEnum set) {
    return GestureDetector(
      onTap: () => _paymentCardOnTap(set),
      child: Container(
        height: 100,
        margin: EdgeInsets.symmetric(
            horizontal: AppSize.defaultPadding, vertical: 10),
        decoration: BoxDecoration(
          // 裝飾內裝元件
            color: Colors.grey.shade200,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            border: Border.all(
              width: 2,
              color: AppColor.shoppingCartNone,
            )),
        padding: EdgeInsets.only(left: 20,),
        child: Row(children: [
          Container(
            height: 60,
              child: Image.asset(set.image)),
          Text(
            " ${set.showLabel}",
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          )
        ]),
      ),
    );
  }
  void _paymentCardOnTap(OrderPaymentEnum set){
    switch(set){
      case OrderPaymentEnum.dmCoin:
        delegate.push(name: RouteName.memberQrcode);
        return;
      default: 
        EasyLoading.showToast(EasyLoadingMessage.noOpen);
    }
  }
}
