import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/news_provider.dart';
import '../../save_model/news.dart';
import '../../utils/app_size.dart';
import '../../widgets/news_card.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  NewsProvider? _newsProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _newsProvider!.getNews();
  }

  @override
  Widget build(BuildContext context) {
    _newsProvider = Provider.of<NewsProvider>(context);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SafeArea(
        child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.8,
            ),
            itemCount: LocalNew.newsModel.length,
            itemBuilder: (context, index) {
              return NewsCard(LocalNew.newsModel[index]);
            }),
      ),
    );
  }
}
