import 'dart:developer';

import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';


class MemberQrcode extends StatefulWidget{
  @override
  State createState() => _MemberQrcodeState();
}
class _MemberQrcodeState extends State<MemberQrcode>{
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
            titleBarIntegrate('會員條碼',[_backButton()]),
              //Body
              Expanded(flex: 11, child: _bodyWidget())
            ],
          ),
        ),
      ),
    );
  }

  Widget _backButton(){
    return GestureDetector(
      onTap: (){
        Navigator.pop(context);
      },
      child: backButtonWidget(),
    );
  }



  //Body
  Widget _bodyWidget() {
    return Container(
      color: Colors.white,
      width: AppSize.appWidth,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 300,
            height: 300,
            child: QrImage(
              data: '${LocalMember.memberModel?.id}'+'_'+'${LocalMember.qrcode}',
              version: QrVersions.auto,
              size: 200.0,
            )
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            child: Text(
              '請掃描QR碼，進行消費及集點',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 19),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            child: Text(
              '${LocalMember.memberModel?.account}/${LocalMember.memberModel?.name}',
              textScaleFactor: 1,
              style: TextStyle(color: Colors.blue,fontSize: 19),
            ),
          )
        ],
      ),
    );
  }
}