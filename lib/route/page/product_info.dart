import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../enum/shopping_cart.dart';
import '../../models/products_model.dart';
import '../../models/shop_carts_models.dart';
import '../../providers/shopping_cart_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../utils/util.dart';
import '../../widgets/app_widget.dart';

class ProductInfo extends StatefulWidget {
  const ProductInfo({Key? key}) : super(key: key);

  @override
  State<ProductInfo> createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  late ProductsModel item;
  ShoppingCartProvider? _shoppingCartProvider;

  @override
  Widget build(BuildContext context) {
    item = ModalRoute.of(context)!.settings.arguments as ProductsModel;
    _shoppingCartProvider ??= Provider.of<ShoppingCartProvider>(context);
    return Container(
      width: AppSize.appWidth,
      height: AppSize.appHeight,
      color: Colors.white,
      child: Column(
        children: [
          titleBarIntegrate('商品資訊', [_backButton()]),
          //圖片
          Container(
            margin: EdgeInsets.only(top: 5),
            height: 300,
            child: Image(
              image: CachedNetworkImageProvider(
                Util().getPhotoUrl(item.photo ?? ''),
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
          _cardTitle(),
          SizedBox(
            height: 20,
          ),
          Expanded(child: _body()),
        ],
      ),
    );
  }

  Widget _cardTitle() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //標題
          Text(item.name ?? '',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 27)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Text('${item.price}枚',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.red,
                    fontSize: 23)),
          ),
          Row(
            children: [
              Expanded(child: _addShoppingCart()),
              SizedBox(
                width: 20,
              ),
              Expanded(child: _immediatePayment()),
            ],
          )
        ],
      ),
    );
  }

  Widget _body() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //規格
              titleCard('商品規格'),
              Text(
                item.eslItemStatus!,
                textScaleFactor: 1,
                style: TextStyle(fontSize: 20),
              ),
              //內容
              titleCard('商品描述'),
              Html(
                data: item.description,
                style: {
                  "p": Style(color: Colors.black, fontSize: FontSize.larger)
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget titleCard(String title) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 5),
          height: 20,
          width: 5,
          color: AppColor.textRedColor,
        ),
        Container(
          child: Text(
            '$title',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
        ),
      ],
    );
  }

  Widget _addShoppingCart() {
    return GestureDetector(
      onTap: () => _addShoppingCartOnTap(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: AppColor.buttonOrange,
            borderRadius: BorderRadius.circular(10)),
        child: Text(
          '加入購物車',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
    );
  }

  Future<void> _addShoppingCartOnTap() async {
    _shoppingCartProvider!.changeIsImmediately(false);
    ShoppingCartsModel data = ShoppingCartsModel(
      productsId: item.id,
      qty: 1,
    );
    await _shoppingCartProvider!.addItem(data);
    delegate.push(name: RouteName.shoppingCart);
  }

  Widget _immediatePayment() {
    return GestureDetector(
      onTap: () => _immediatePaymentOnTap(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: AppColor.textRedColor,
            borderRadius: BorderRadius.circular(10)),
        child: Text(
          '立即購買',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
    );
  }

  void _immediatePaymentOnTap() {
    _shoppingCartProvider!.changeIsImmediately(true);
    _shoppingCartProvider!.addImmediatelyCart(item);
    _shoppingCartProvider!
        .changeShoppingCartStepEnum(ShoppingCartStepEnum.orderInfo);
    delegate.push(name: RouteName.selectPayment);
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, false);
      },
      child: backButtonWidget(),
    );
  }
}
