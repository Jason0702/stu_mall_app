import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../save_model/member.dart';
import '../../utils/app_image.dart';
import '../../utils/app_size.dart';

class PaymentResult extends StatefulWidget {
  const PaymentResult({Key? key}) : super(key: key);

  @override
  _PaymentResultState createState() => _PaymentResultState();
}

class _PaymentResultState extends State<PaymentResult> {
  MemberProvider? _memberProvider;
  bool successed = true;
  String toMemberName = "";
  String toMemberStudentId = "";
  String fromMemberName = "";
  String fromMemberStudentId = "";
  int coin = 0;
  String createdAt = "";
  String number = "";

  //0是收款方1是付款方
  int recvOrPay = -1;

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    String data = ModalRoute.of(context)!.settings.arguments as String;
    Map<String, dynamic> tmp = jsonDecode(data);
    fromMemberName = tmp['from_members_name'];
    fromMemberStudentId = tmp['from_members_student_id'];
    toMemberName = tmp['to_members_name'];
    toMemberStudentId = tmp['to_members_student_id'];
    createdAt = tmp['action_time'].substring(0, 16);
    number = tmp['transaction_number'];
    coin = tmp['amount'];

    if (toMemberStudentId == LocalMember.memberModel?.studentId) {
      recvOrPay = 0;
    }
    if (fromMemberStudentId == LocalMember.memberModel?.studentId) {
      recvOrPay = 1;
    }

    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            titleBarIntegrate(
                recvOrPay == 0 ? '收款成功' : (successed ? '付款成功' : '付款失敗'),
                [],
                [_backButton()]),
            //Body
            Expanded(child: _bodyWidget()),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        _memberProvider!.getMemberPoints();
        delegate.popRoute();
      },
      child: backButtonClearWidget(),
    );
  }

  //body
  Widget _bodyWidget() {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      width: AppSize.appWidth,
      color: Colors.white,
      child: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: 120,
                  padding: EdgeInsets.all(20),
                  child: Image.asset(recvOrPay == 0
                      ? AppImage.iconSuccess
                      : (successed
                          ? AppImage.iconSuccess
                          : AppImage.iconFailed)),
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: Text(
                      recvOrPay == 0 ? '收款成功' : (successed ? '付款成功' : '付款失敗'),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )),
                Container(
                  width: AppSize.appWidth,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '$coin',
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 40,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '枚',
                        style: TextStyle(color: Colors.red, fontSize: 20,height: 2.2),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]),
        Container(
          width: AppSize.appWidth,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 20),
                  child: Text(
                    '交易資訊',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 10),
                  child: Text(
                    '交易序號 : $number',
                    style: TextStyle(fontSize: 20),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 10),
                  child: Text(
                    recvOrPay == 0 ? '收款日期 : $createdAt' : '付款日期 : $createdAt',
                    style: TextStyle(fontSize: 20),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 10),
                  child: Text(
                    recvOrPay == 0
                        ? '付款方名稱 : $fromMemberName'
                        : '收款方名稱 : $toMemberName',
                    style: TextStyle(fontSize: 20),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 10),
                  child: Text(
                    recvOrPay == 0
                        ? '收款方ID : $fromMemberStudentId'
                        : '收款方ID : $toMemberStudentId',
                    style: TextStyle(fontSize: 20),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 0, bottom: 10),
                  child: Text(
                    recvOrPay == 0 ? '收款點數 : $coin' : '付款點數 : $coin',
                    style: TextStyle(fontSize: 20),
                  )),
            ],
          ),
        )
      ]),
    );
  }
}
