import 'package:flutter/material.dart';

import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/app_size.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        alignment: Alignment.center,
        children: [
           Container(
              width: AppSize.appWidth,
              height:AppSize.appHeight,
              color:AppColor.appMainColor,),
          const Image(
            width: 150,
              height: 150,
              image: AssetImage(AppImage.logoWhite))
        ],
      ),
    );
  }
}
