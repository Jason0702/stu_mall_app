
import 'package:com.sj.stu_mall_app/api/member.dart';
import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/models/forget_password_model.dart';
import 'package:com.sj.stu_mall_app/providers/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import '../../models/turple_model.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/app_size.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/text_filed_title.dart';
import '../../widgets/text_from_filed.dart';


class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  AuthProvider? _authProvider;
  TextEditingController _account = TextEditingController();
  TextEditingController _cellphoneNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            //Title
            titleBarIntegrate("忘記密碼",[_backButton()]),
            Expanded(
              child: _bodyWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return  GestureDetector(
      onTap: (){
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _bodyWidget() {
    return Container(
      width:AppSize.appWidth,
      height:AppSize.appHeight,
      alignment: Alignment.center,
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            child: Image(
              image: AssetImage(AppImage.logoBlack),
              height: 130,
            ),
          ),
          TitleHasSign('帳號'),
          StudentIdTextField(controller: _account,),
          SizedBox(height: 10,),
          TitleHasSign('手機'),
          AccountTextField(controller: _cellphoneNumber,),
          SizedBox(height: 20,),
          Container(
            child:Text(
              '請輸入您的帳號與手機號碼，將會驗證並登入，\n登入後請修改為常用密碼！',
              textScaleFactor: 1,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 16,
                  color:AppColor.hintColor,
                  fontWeight: FontWeight.normal),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            alignment: Alignment.center,
            child: ButtonTheme(
              height: 25,
              child: GestureDetector(
                onTap: _verificationTap,
                child: buttonWidget('驗證並登入'),
              ),
            ),
          )
        ],
      ),
    );
  }

  //"確認送出"的按鈕按下去執行
  void _verificationTap(){
    EasyLoading.show(status: '驗證中');
    _forgotPassword();
    //EasyLoading.showError("請聯絡系統管理員");
  }



  //post忘記密碼
  void _forgotPassword()async{
    if(_account.text.isNotEmpty && _cellphoneNumber.text.isNotEmpty){
      ForgetPasswordModel dto = ForgetPasswordModel(
          account: _account.text,
          cellphone: _cellphoneNumber.text);
      Tuple<bool, String> r = await MemberApi().forgetPassword(dto);
      if(r.i1!){
        EasyLoading.dismiss();
        _authProvider!.memberLogin(_account.text, r.i2!);
      }else{
        EasyLoading.showError(r.i2!);
      }
    } else if(_account.text.isNotEmpty){
      EasyLoading.showError('請輸入帳號');
    } else if(_cellphoneNumber.text.isNotEmpty){
      EasyLoading.showError('請輸入手機號碼');
    }

  }
}
