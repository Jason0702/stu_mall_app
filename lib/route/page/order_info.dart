import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/models/hero_view_params.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../enum/app_enum.dart';
import '../../enum/order.dart';
import '../../models/order_model.dart';
import '../../models/shop_carts_models.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../utils/util.dart';
import '../../widgets/app_widget.dart';
import 'order_qrcode.dart';

class OrderInfoPage extends StatefulWidget {
  const OrderInfoPage({Key? key, required this.data}) : super(key: key);
  final OrderModel data;

  @override
  State<OrderInfoPage> createState() => _OrderInfoPageState();
}

class _OrderInfoPageState extends State<OrderInfoPage> {
  OrderModel get data => widget.data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            titleBarIntegrate('交易明細', [_backButton()]),
            Expanded(child: _body()),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: backButtonWidget(),
    );
  }

  Widget _body() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Container(
            width: AppSize.appWidth,
            padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '交易資訊',
                  textScaleFactor: 1,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
                ),
                _textCard('交易編號 :${data.orderNumber!}'),
                _textCard(
                    '交易日期 :${Util.dateFormatHHMM.format(Util().utcTimeToLocal(data.createdAt ?? ' '))}'),
                _textCard(
                    '付款方式 :${OrderPaymentEnum.values[data.paymentType!].label}'),
                _textCard(
                    '運送方式 :${DeliveryTypeEnum.values[data.deliveryOption!].label}'),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '收件資訊 :',
                      textScaleFactor: 1,
                      style: TextStyle(fontSize: 18),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${data.recvName}',
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            '${data.recvCellphone}',
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            '${data.recvAddress}',
                            softWrap: true,
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                _textCard('交易金額 : ${data.payAmount}'),
                _textCard('物品狀態 : ${Util().getStatus(data)}'),
                if(data.smartDeviceNo!.isNotEmpty)
                _textCard('取貨櫃號 : ${data.smartDeviceNo}'),
                _productListTitle(),
                ...data.productInfo.map((e) => _productList(e)),
                _totalMoneyWidget(),
                if (data.isShipping != AppEnum.inactive)
                  _qrCodeWidget(),
              ],
            ),
          ),
        ));
  }

  Widget _qrCodeWidget(){
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation secondaryAnimation){
              return FadeTransition(
                  opacity: animation,
                  child: OrderQrcode(paramsModel: HeroViewParamsModel(
                      tag: 'qrcode',
                      data: data.orderNumber!),),
              );
            },
        )
      ),
      child: Hero(
        tag: 'qrcode',
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              child: QrImage(
                data: data.orderNumber!,
                version: QrVersions.auto,
                size: 200.0,
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: _textCard(
                  '請點選放大',
                  Colors.blueAccent,
                  TextDecoration.underline
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: _textCard(
                  '${DeliveryTypeEnum.values[data.deliveryOption!].label}使用',
                  Colors.blueAccent,
                  TextDecoration.underline
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _textCard(String data, [Color? color, TextDecoration? decoration]) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Text(data, style: TextStyle(fontSize: 18, color: color == null ? Colors.black : color, decoration: decoration == null ? TextDecoration.none : decoration)));
  }

  //產品list
  Widget _productListTitle() {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 6,
              child: Text('商品名稱',
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold))),
          Expanded(
              child: Container(
                  alignment: Alignment.center,
                  child: Text('數量',
                      style: TextStyle(
                          fontSize: 17, fontWeight: FontWeight.bold)))),
          Expanded(
              flex: 2,
              child: Container(
                  alignment: Alignment.centerRight,
                  child: Text('價錢',
                      style: TextStyle(
                          fontSize: 17, fontWeight: FontWeight.bold)))),
        ],
      ),
    );
  }

  //產品list
  Widget _productList(ShoppingCartsModel data) {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 6,
              child: FittedBox(
                  alignment: Alignment.centerLeft,
                  fit: BoxFit.scaleDown,
                  child: Text('${data.name}', style: TextStyle(fontSize: 17)))),
          Expanded(
              child: FittedBox(
                  alignment: Alignment.center,
                  fit: BoxFit.scaleDown,
                  child: Text('${data.qty}', style: TextStyle(fontSize: 17)))),
          Expanded(
              flex: 2,
              child: FittedBox(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.scaleDown,
                  child: Text('${data.price! * data.qty!}',
                      style: TextStyle(fontSize: 17)))),
        ],
      ),
    );
  }

  //總金額
  Widget _totalMoneyWidget() {
    return Container(
      height: 90,
      margin: EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: [
          Expanded(
              child: Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '共 ${data.productInfo.length} 件商品',
                    textScaleFactor: 1,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ))),
          Expanded(
            child: Column(
              children: [
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text('小計',
                            textScaleFactor: 1,
                            style: TextStyle(fontSize: 18)))),
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text('運費',
                            textScaleFactor: 1,
                            style: TextStyle(fontSize: 18)))),
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text(
                          '總付款金額',
                          textScaleFactor: 1,
                          style: TextStyle(
                              fontSize: 18,
                              color: AppColor.appMainColor,
                              fontWeight: FontWeight.bold),
                        )))
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text(
                            '\$${data.payAmount! - Util.deliveryValue[data.deliveryOption!]}',
                            textScaleFactor: 1,
                            style: TextStyle(fontSize: 18)))),
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text(
                            '\$${Util.deliveryValue[data.deliveryOption!]}',
                            textScaleFactor: 1,
                            style: TextStyle(fontSize: 18)))),
                Expanded(
                    child: Container(
                        alignment: Alignment.topRight,
                        child: Text('\$${data.payAmount}',
                            textScaleFactor: 1,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.red,
                                fontWeight: FontWeight.bold))))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
