import 'dart:convert';

import 'package:com.sj.stu_mall_app/utils/no_shadow_scroll_behavior.dart';
import 'package:com.sj.stu_mall_app/utils/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../../api/member.dart';
import '../../dialog/qrcode_scanner.dart';
import '../../models/member_model.dart';
import '../../models/turple_model.dart';
import '../../providers/member_provider.dart';
import '../../save_model/member.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../widgets/app_widget.dart';
import '../../widgets/dm_instruction_card.dart';
import '../../widgets/point_card.dart';
import '../../widgets/text_from_filed.dart';

class DmPaymentPage extends StatefulWidget {
  const DmPaymentPage({Key? key}) : super(key: key);

  @override
  State<DmPaymentPage> createState() => _DmPaymentPageState();
}

class _DmPaymentPageState extends State<DmPaymentPage> {
  TextEditingController _studentId = TextEditingController();
  TextEditingController _money = TextEditingController();
  TextEditingController _password = TextEditingController();
  MemberProvider? _memberProvider;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      body: Column(
        children: [
          _pointWidget(),
          Expanded(child: _body()),
        ],
      ),
    );
  }

  Widget _body() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
            child: Column(
              children: [
                _cardTitle('會員ID'),
                Row(
                  children: [
                    Expanded(child: _accountTextField(_studentId)),
                    SizedBox(
                      width: 20,
                    ),
                    scanButton(),
                  ],
                ),
                _cardTitle('轉出金額'),
                MoneyTextField(
                  controller: _money,
                ),
                _cardTitle('密碼'),
                _passwordTextField(_password),
                const SizedBox(
                  height: 50,
                ),
                _buttonWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget scanButton() {
    return GestureDetector(
      onTap: () => showDialog(),
      child: Container(
        width: 100,
        child: buttonRedWidget('QR掃碼'),
      ),
    );
  }

  //會員點數
  Widget _pointWidget() {
    return Container(
      height: 110,
      margin: EdgeInsets.symmetric(
          vertical: 10, horizontal: AppSize.defaultPadding),
      width: AppSize.appWidth,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: AppColor.buttonOrange, width: 3)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //會員點數
          PointsCard(),
          //使用說明
          DmInstructionCard(),
        ],
      ),
    );
  }

  Widget _passwordTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入密碼",
      ),
      obscureText: true,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入密碼";
        } else if (value.trim().length < 6 || value.trim().length > 12) {
          return "請輸入6~12位英數字";
        } else if (!TextFieldSetting().passwordVerification(value)) {
          return "密碼只能包含英數文字加底線";
        }
        return null;
      },
    );
  }

  Widget _accountTextField(TextEditingController _controller) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        //文字方向
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入學號",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請輸入學號";
        }
        return null;
      },
    );
  }

  Future<void> showDialog() async {
    var result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            alignment: Alignment.topRight,
            scale: anim1.value,
            child: QrcodeScanner(
              key: UniqueKey(),
            ),
          );
        });
    if (result != "" && result != null) {
      String input = result as String;
      List<String> _inputSplit = input.split('_');
      EasyLoading.show();
      Tuple<bool, String> r = await MemberApi().getMember(int.parse(_inputSplit[0]));
      if(r.i1!){
        EasyLoading.dismiss();
        MemberModel _member = MemberModel.fromJson(jsonDecode(r.i2!));
         _studentId.text = _member.studentId!;
        setState(() {});
      } else {
        EasyLoading.showError('${r.i2}');
      }
      /*Map<String, dynamic> data = jsonDecode(input);
      _studentId.text = data['id'].toString();
      _money.text = data['money'].toString();*/
    }
  }

  Widget _cardTitle(String data) {
    return Container(
      alignment: Alignment.centerLeft,
      child: RichText(
          textScaleFactor: 1,
          text: TextSpan(
            text: data,
            style: TextStyle(
                fontSize: 19,
                color: AppColor.darkBlue,
                fontWeight: FontWeight.bold),
          )),
    );
  }

  Widget _buttonWidget() {
    return GestureDetector(
      onTap: () => _buttonWidgetOnTap(_studentId.text, _money.text),
      child: buttonWidget('確定'),
    );
  }

  Future<void> _buttonWidgetOnTap(String account, String moneyT) async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    if (_studentId.text.toString() == LocalMember.memberModel?.studentId) {
      EasyLoading.showError('不能轉帳給自己');
      return;
    }
    if (_password.text.toString() !=
        await SharedPreferenceUtil().getPassword()) {
      EasyLoading.showError('密碼輸入錯誤');
      return;
    }
    int? _money = int.tryParse(moneyT);
    if (_money == null) {
      return;
    }
    _memberProvider!.postTransferData(account, _money);
  }
}
