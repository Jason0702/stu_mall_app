import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.sj.stu_mall_app/utils/app_color.dart';
import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../models/news_model.dart';
import '../../utils/util.dart';
import '../../widgets/app_widget.dart';

class NewsInfo extends StatefulWidget {
  const NewsInfo({Key? key, required this.data}) : super(key: key);
  final NewsModel data;

  @override
  State<NewsInfo> createState() => _NewsInfoState();
}

class _NewsInfoState extends State<NewsInfo> {
  NewsModel get info => widget.data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleBarIntegrate('活動訊息', [_backButton()]),
            //圖片
            Image(
              image: CachedNetworkImageProvider(
                Util().getPhotoUrl(info.pic ?? ''),
              ),
              fit: BoxFit.fill,
            ),
            //標題
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppSize.defaultPadding, vertical: 10),
              child: Text(info.title ?? '',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 27)),
            ),
            //時間
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppSize.defaultPadding, vertical: 10),
              child: Text(
                  '${Util.dateFormatISO.format(Util().utcTimeToLocal(info.createdAt ?? ' '))}',
                  style: TextStyle(fontSize: 19, color: AppColor.appMainColor)),
            ),
            //內容
            Html(
              data: info.body,
              style: {
                "p": Style(
                    //margin: const EdgeInsets.symmetric(vertical: 20),
                    color: Colors.black,
                    fontSize: FontSize.larger)
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: backButtonWidget(),
    );
  }
}
