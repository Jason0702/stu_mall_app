import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/products_type_model.dart';
import '../../providers/product_provider.dart';
import '../../save_model/product.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../widgets/item_card.dart';

class ItemsPage extends StatefulWidget {
  const ItemsPage({Key? key}) : super(key: key);

  @override
  State<ItemsPage> createState() => _ItemsPageState();
}

class _ItemsPageState extends State<ItemsPage> with TickerProviderStateMixin {
  late TabController _tabController;
  ProductProvider? _productProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) async {
    await Future.wait([
      _productProvider!.getProductTypes(),
      _productProvider!.getProducts(),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    _productProvider = Provider.of<ProductProvider>(context);
    _tabController =
        TabController(length: LocalProduct.productsTypes.length, vsync: this);
    return Scaffold(
        backgroundColor: Colors.grey[200],
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(), child: _buildBodyWidget()),
        ));
  }

  //全部
  Widget _buildBodyWidget() {
    return Container(
      child: DefaultTabController(
        length: LocalProduct.productsTypes.length,
        child: NestedScrollView(
            physics: NeverScrollableScrollPhysics(),
            headerSliverBuilder: (context, _) {
              return <Widget>[
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      //商品資訊
                      _commodityInformation(),
                    ],
                  ),
                ),
              ];
            },
            body: TabBarView(
              children:
                  LocalProduct.productsTypes.map((e) => mTableView(e)).toList(),
              controller: _tabController,
              physics: ScrollPhysics(),
            )),
      ),
    );
  }

  //商品資訊
  Widget _commodityInformation() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 45,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, boxShadow: [AppColor.shadow]),
              child: new TabBar(
                isScrollable: true,
                tabs: LocalProduct.productsTypes
                    .map((e) => Tab(
                          child: Text(
                            '${e.name}',
                            style: TextStyle(fontSize: 19),
                          ),
                        ))
                    .toList(),
                controller: _tabController,
                indicatorColor: AppColor.textRedColor,
                labelColor: AppColor.textRedColor,
                unselectedLabelColor: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget mTableView(ProductsTypeModel type) {
    return LocalProduct.products
            .any((element) => element.productsTypeId == type.id)
        ? GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 0.87),
            itemCount: LocalProduct.products
                .where((element) => element.productsTypeId == type.id)
                .length,
            itemBuilder: (context, index) {
              return ItemCard(LocalProduct.products
                  .where((element) => element.productsTypeId == type.id)
                  .toList()[index]);
            })
        : Container(
            alignment: Alignment.center,
            width: AppSize.appWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.remove_shopping_cart,
                  color: Colors.grey,
                  size: 60,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "此分頁沒有商品",
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                ),
              ],
            ),
          );
  }
}
