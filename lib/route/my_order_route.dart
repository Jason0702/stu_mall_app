import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../app.dart';
import '../enum/order.dart';
import '../models/order_model.dart';
import '../providers/record_provider.dart';
import '../save_model/record.dart';
import '../utils/app_color.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/util.dart';
import '../widgets/app_widget.dart';
import 'page/order_info.dart';

class OrderRoute extends StatefulWidget {
  const OrderRoute({Key? key}) : super(key: key);

  @override
  State<OrderRoute> createState() => _MyOrderPageState();
}

class _MyOrderPageState extends State<OrderRoute> {
  RecordProvider? _recordProvider;
  OrderEnum select = OrderEnum.payment;
  bool _once = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _recordProvider!.getOrderRecords();
  }

  @override
  Widget build(BuildContext context) {
    _recordProvider = Provider.of<RecordProvider>(context);
    final arguments = ModalRoute.of(context)?.settings.arguments as OrderEnum?;
    if(arguments != null && !_once){
      select = arguments;
      _once = true;
    }
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('我的訂單', [_backButton()]),
          _subTitle(),
          Expanded(child: _buildList()),
        ],
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _subTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [AppColor.shadow],
        color: Colors.white,
      ),
      child: Row(children: [
        _subTitleCard(OrderEnum.payment),
        _subTitleCard(OrderEnum.shipping),
        _subTitleCard(OrderEnum.take),
      ]),
    );
  }

  Widget _subTitleCard(OrderEnum set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            set.label,
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _subTitleCardOnTap(OrderEnum set) {
    select = set;
    setState(() {});
  }

  Widget _buildList() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: ListView.builder(
          itemCount: _recordProvider!.getOrderList(select).length,
          itemBuilder: (context, index) {
            return _buildPayRecord(
                _recordProvider!.getOrderList(select)[index]);
          }),
    );
  }

  Widget _buildPayRecord(OrderModel data) {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: AppSize.defaultPadding, vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('交易編號 :${data.orderNumber!}', style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 5,
            ),
            Text(
                '交易日期 :${Util.dateFormatHHMM.format(Util().utcTimeToLocal(data.createdAt ?? ' '))}',
                style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('付款方式 :${OrderPaymentEnum.values[data.paymentType!].label}  ',
                    style: TextStyle(fontSize: 20)),
                GestureDetector(
                  onTap: () => showOrderInfo(data),
                  child: Text.rich(
                      TextSpan(
                          text: '運送方式 :',
                          style: TextStyle(fontSize: 20),
                          children: [
                            TextSpan(
                              text: '${DeliveryTypeEnum.values[data.deliveryOption!].label}',
                              style: TextStyle(fontSize: 20, color: Colors.blueAccent, decoration: TextDecoration.underline),
                            )
                          ]
                      )
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Text('貨品狀態 :', style: TextStyle(fontSize: 20)),
                Text(Util().getStatus(data),
                    textScaleFactor: 1,
                    style: TextStyle(fontSize: 20, color: Colors.red))
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('交易金額 :${data.payAmount}', style: TextStyle(fontSize: 20)),
                GestureDetector(
                    onTap: () => showOrderInfo(data),
                    child: Container(
                      child: Text(
                        '消費明細 >',
                        textScaleFactor: 1,
                        style: TextStyle(
                            color: AppColor.appMainColor, fontSize: 20),
                      ),
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }

  void showOrderInfo(OrderModel data) {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: OrderInfoPage(
              data: data,
              key: UniqueKey(),
            ),
          );
        });
  }
}
