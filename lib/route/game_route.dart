import 'dart:math';

import 'package:com.sj.stu_mall_app/providers/game_provider.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:scratcher/widgets.dart';

import '../app.dart';
import '../enum/game.dart';
import '../models/game_type_models.dart';
import '../save_model/game.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widgets/app_widget.dart';

class GameRouter extends StatefulWidget {
  const GameRouter({Key? key}) : super(key: key);

  @override
  State<GameRouter> createState() => _GameRouterState();
}

class _GameRouterState extends State<GameRouter> with TickerProviderStateMixin {
  GameProvider? _gameProvider;
  GameEnum select = GameEnum.bingo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_onLayOutDone);
  }

  _onLayOutDone(_) {
    precacheImage(imgPrize2Selected.image, context);
    _gameProvider!.getGameTypes();
    _controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    _controller.addStatusListener(_bingoListener);
  }

  @override
  Widget build(BuildContext context) {
    _gameProvider ??= Provider.of<GameProvider>(context);
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            titleBarIntegrate('好康樂翻天', [_backButton()]),
            Expanded(child: _body())
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _body() {
    return Stack(
      children: [
        gameBody(select),
        Positioned(top: 0, right: 0, left: 0, child: _subTitle()),
      ],
    );
  }

  Widget gameBody(GameEnum select) {
    switch (select) {
      case GameEnum.bingo:
        return _game1BingoGame();
      case GameEnum.quiz:
        return _gameQuizWidget();
      case GameEnum.scratcher:
        return _gameOneWidget();
    }
  }

  //上方切換列
  Widget _subTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [AppColor.shadow],
        color: Colors.white,
      ),
      child: Row(children: [
        _subTitleCard(GameEnum.bingo),
        _subTitleCard(GameEnum.quiz),
        _subTitleCard(GameEnum.scratcher),
      ]),
    );
  }

//上方切換列卡片
  Widget _subTitleCard(GameEnum set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.all(5),
          padding: EdgeInsets.symmetric(
            vertical: 10,
          ),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            _gameProvider!.getName(set),
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
        ),
      ),
    );
  }

  //上方切換列事件
  void _subTitleCardOnTap(GameEnum set) {
    select = set;
    setState(() {});
    _gameProvider!.getTodayIsPlay();
  }

  //region 九宮格
  late AnimationController _controller;
  late Animation<int> _selectedIndexTween;
  final Map<int, int> _selectMap = {
    0: 0,
    1: 1,
    2: 2,
    3: 5,
    4: 8,
    5: 7,
    6: 6,
    7: 3
  };
  int _currentSelect = -1;
  final int repeatRound = 3;
  int randomNumber = 0;
  var rng = Random();
  bool _isStart = false;
  Image imgPrize2Selected = Image.asset(AppImage.imgPrize1Selected);
  int point = 0;

  //UI
  Widget _game1BingoGame() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Image.asset(
          AppImage.imgGame2,
          height: AppSize.appHeight,
          width: AppSize.appWidth,
          fit: BoxFit.cover,
        ),
        _bingo(),
        Positioned(
            bottom: 10, left: 0, right: 0, child: _gameRule(LocalGame.bingo)),
      ],
    );
  }

  //九宮格
  Widget _bingo() {
    return Container(
      margin: const EdgeInsets.all(60),
      padding: const EdgeInsets.all(25),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(AppImage.imgGame2Frmae),
      )),
      child: GridView.builder(
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 9,
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
              crossAxisCount: 3,
              childAspectRatio: 1),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () => _startOnTap(index),
              child: Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(15)),
                child: _returnImage(index),
              ),
            );
          }),
    );
  }

  //九宮格圖案
  Image _returnImage(int index) {
    ///被選中
    if (index == 4) {
      return Image.asset(AppImage.btnLuckydraw);
    }
    if (_currentSelect == index) {
      return imgPrize2Selected;
    }
    if (_isStart) {
      return Image.asset(AppImage.imgPrize1);
    }
    return index % 2 == 0
        ? Image.asset(AppImage.imgPrize2)
        : Image.asset(AppImage.imgPrize1);
  }

  //開始遊戲
  Future<void> _startOnTap(int index) async {
    //中間財觸發
    if (index != 4) {
      return;
    }
    if (EasyLoading.isShow) {
      return;
    }
    if (_isStart) {
      return;
    }
    if (LocalMember.dmCoin < 1) {
      EasyLoading.showToast(EasyLoadingMessage.coinNotEnough);
      return;
    }
    if (_gameProvider!.jiugonggeTodayIsPlay) {
      todayCantPlayDialog();
      return;
    }
    if (!_gameProvider!.todayCanPlay(LocalGame.bingo)) {
      EasyLoading.showToast(EasyLoadingMessage.gameIsClose);
      return;
    }
    _isStart = true;
    EasyLoading.show();
    int? result = await _gameProvider!.playGame(LocalGame.bingo.id);
    if (result == null) {
      _isStart = false;
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    point = result;
    EasyLoading.dismiss();
    _controller.reset();
    _controller.forward();
    randomNumber = rng.nextInt(8);

    _selectedIndexTween =
        IntTween(begin: 0, end: repeatRound * 8 + randomNumber).animate(
            CurvedAnimation(parent: _controller, curve: Curves.easeOutQuart))
          ..addListener(_listen);
  }

  //動畫狀態監聽
  void _listen() {
    if (_currentSelect != _selectMap[_selectedIndexTween.value % 8]) {
      setState(() {
        _currentSelect = _selectMap[_selectedIndexTween.value % 8]!;
      });
    }
  }

  //動畫結束監聽
  void _bingoListener(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      _isStart = false;
      _currentSelect = -1;
      showGetPointDialog(point);
      _selectedIndexTween.removeListener(_listen);
      setState(() {});
    }
  }

  Future showGetPointDialog(int point) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: Container(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  GestureDetector(
                    child: Icon(
                      Icons.clear,
                      size: 30,
                    ),
                    onTap: () => Navigator.of(context).pop(), //关闭对话框
                  ),
                ]),
                const Spacer(),
                Text(
                  "恭喜獲得",
                  textScaleFactor: 1,
                  style: TextStyle(fontSize: 24, color: Colors.red),
                ),
                Text(
                  "點數$point點",
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 24,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "使用期限:無使用期限",
                  textScaleFactor: 1,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                Text(
                  "已扣除DM幣一枚",
                  textScaleFactor: 1,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                const Spacer(
                  flex: 2,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future todayCantPlayDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: Container(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  GestureDetector(
                    child: Icon(
                      Icons.clear,
                      size: 30,
                    ),
                    onTap: () => Navigator.of(context).pop(), //关闭对话框
                  ),
                ]),
                const Spacer(),
                Text(
                  "今日已遊玩",
                  textScaleFactor: 1,
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
                Text(
                  "請明日再來",
                  textScaleFactor: 1,
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
                const Spacer(
                  flex: 2,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

//endregion

  //region 刮刮樂
  double _opacity = 0.0;

  Widget _gameOneWidget() {
    return Stack(
      children: [
        //底圖
        Image.asset(
          AppImage.imgGame1,
          height: AppSize.appHeight,
          width: AppSize.appWidth,
          fit: BoxFit.cover,
        ),
        LocalMember.dmCoin != 0 &&
                !_gameProvider!.scratcherTodayIsPlay &&
                _gameProvider!.todayCanPlay(LocalGame.scratcher)
            ? Positioned(
                bottom: AppSize.appHeight / AppSize.appWidth > 1.7 ? 200 : 140,
                right: 50,
                left: 50,
                child: _buildScratcher())
            : Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 50),
                child: Text(
                  _statusTextReturn(),
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ),

        Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: _gameRule(LocalGame.scratcher)),
      ],
    );
  }

  String _statusTextReturn() {
    if (LocalMember.dmCoin == 0) {
      return EasyLoadingMessage.coinNotEnough;
    }
    if (_gameProvider!.scratcherTodayIsPlay) {
      return EasyLoadingMessage.todayIsPlay;
    }
    return EasyLoadingMessage.gameIsClose;
  }

  Widget _buildScratcher() {
    return Scratcher(
      color: AppColor.greyCo,
      accuracy: ScratchAccuracy.high,
      threshold: 30,
      brushSize: 25,
      onThreshold: () => playScratcher(),
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 10),
        opacity: _opacity,
        child: Container(
          height: 230,
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                "恭喜獲得",
                textScaleFactor: 1,
                style: TextStyle(fontSize: 23, color: Colors.red),
              ),
              Text(
                '點數$point點',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 35,
                    color: Colors.red,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "使用期限:無使用期限",
                textScaleFactor: 1,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              Text(
                "已扣除DM幣一枚",
                textScaleFactor: 1,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void playScratcher() async {
    int? result = await _gameProvider!.playGame(LocalGame.scratcher.id);
    if (result == null) {
      _isStart = false;
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      setState(() {});
      return;
    }
    point = result;
    _opacity = 1;
    setState(() {});
  }

  //endregion

  //region 有獎問答
  //是否開啟遊戲
  bool questionIsStart = false;

  //現在進行的題目
  int questionIndex = 0;

  //選擇的答案
  int answerIndex = -1;

  Widget _gameQuizWidget() {
    return !questionIsStart
        ? Stack(
            children: [
              //底圖
              Image.asset(
                AppImage.imgGame3,
                height: AppSize.appHeight,
                width: AppSize.appWidth,
                fit: BoxFit.cover,
              ),
              //按鈕
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  //禮物
                  Image.asset(
                    AppImage.titleGame,
                    width: AppSize.appWidth,
                    fit: BoxFit.cover,
                  ),
                  GestureDetector(
                    onTap: () => quizOnTap(),
                    child: Container(
                      alignment: Alignment.center,
                      width: 220,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.all(Radius.circular(40)),
                      ),
                      child: Text(
                        _gameProvider!.quizIsPlay ? '已答題' : '開始答題',
                        style: TextStyle(
                            color: AppColor.quizGameBlue,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  )
                ],
              ),
              //規則
              Positioned(
                  bottom: 10,
                  left: 0,
                  right: 0,
                  child: _gameRule(LocalGame.quiz)),
            ],
          )
        : Stack(
            children: [
              Image.asset(
                AppImage.imgGame3,
                height: AppSize.appHeight,
                width: AppSize.appWidth,
                fit: BoxFit.cover,
              ),
              Container(
                width: AppSize.appWidth,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _questionTitle(questionIndex),
                    Expanded(child: _questionList(questionIndex)),
                  ],
                ),
              ),
            ],
          );
  }

  void quizOnTap() {
    if (!_gameProvider!.quizIsPlay) {
      questionIsStart = true;
      setState(() {});
    }
  }

  //題目抬頭
  Widget _questionTitle(int questionIndex) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        //題目
        Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(10, 86, 10, 0),
            padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
            height: 160,
            decoration: BoxDecoration(
                border: Border.all(color: AppColor.quizGameBlack, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white),
            child: Text(
              '${LocalGame.quizQuestion?.questions![questionIndex].title}',
              maxLines: 4,
              style: TextStyle(
                  color: AppColor.quizGameBlack,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            )),
        //黃色圖
        Container(
            margin: EdgeInsets.only(top: 70),
            width: 270,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(AppImage.titleQuestion, fit: BoxFit.cover),
                Container(
                    alignment: Alignment.center,
                    child: Text(
                      'Q${questionIndex + 1}',
                      style: TextStyle(
                          color: AppColor.quizGamePurple,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    )),
              ],
            )),
      ],
    );
  }

  Widget _questionList(int questionIndex) {
    return Column(
      children: [
        const Spacer(),
        _questionCard(0),
        _questionCard(1),
        _questionCard(2),
        _questionCard(3),
        const Spacer(),
        GestureDetector(
          onTap: () => sendAnswerOnTap(),
          child: Container(
            alignment: Alignment.center,
            height: 60,
            width: 200,
            decoration: BoxDecoration(
                border: Border.all(color: AppColor.quizGameBlack, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: AppColor.quizSendColor),
            child: Text(
              // questionIndex+1 == gameModel.questions.length ? '確認送出' : "下一題"
              '確認送出',
              textScaleFactor: 1,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: AppColor.quizGameBlack),
            ),
          ),
        ),
        const Spacer(),
      ],
    );
  }

  void sendAnswerOnTap() {
    if (questionIndex + 1 != LocalGame.quizQuestion?.questions?.length) {
      return;
    }
    /*      if (answerIndex != -1) {
        _gameProvider! = true;
        showQuestionConfirmDialog().then((value) {
          setState(() {});
        });
      }
    } else {
      if (answerIndex != -1) {
        this.questionIndex += 1;
      }
    }*/
    if (LocalMember.dmCoin == 0) {
      EasyLoading.showError(EasyLoadingMessage.coinNotEnough);
      return;
    }
    if (answerIndex == -1) {
      EasyLoading.showToast(EasyLoadingMessage.selectAnswer);
      return;
    }
    showQuestionConfirmDialog();
    setState(() {});
  }

  //列表卡片
  Widget _questionCard(int set) {
    return GestureDetector(
      onTap: () {
        answerIndex = set;
        setState(() {});
      },
      child: Container(
        alignment: Alignment.center,
        height: 60,
        margin: EdgeInsets.symmetric(horizontal: 60, vertical: 7),
        decoration: BoxDecoration(
            border: Border.all(color: AppColor.quizGameBlack, width: 2),
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color:
                answerIndex == set ? AppColor.selectQuizColor : Colors.white),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            '${LocalGame.quizQuestion?.questions![questionIndex].options![set]}',
            style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            textScaleFactor: 1,
          ),
        ),
      ),
    );
  }

  Future showQuestionConfirmDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: Container(
            height: 270,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  "您選擇的答案是",
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "${LocalGame.quizQuestion?.questions?[0].options?[answerIndex]}",
                  style: TextStyle(
                      fontSize: 30,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "將扣除一枚DM幣，是否送出",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Expanded(
                      child: _cancelButton(),
                    ),
                    Expanded(
                      child: _confirmButton(),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _cancelButton() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 20, right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.blue,
        ),
        height: 50,
        child: Text(
          "取消",
          textScaleFactor: 1,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 19, color: Colors.white),
        ),
      ),
    );
  }

  Widget _confirmButton() {
    return GestureDetector(
      onTap: () => _confirmButtonOnTap(),
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: 10, right: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.red,
        ),
        height: 50,
        child: Text(
          "確認送出",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 19, color: Colors.white),
        ),
      ),
    );
  }

  void _confirmButtonOnTap() {
    questionIsStart = false;
    _gameProvider!.answerQuiz(
        LocalGame.quizQuestion?.questions?[questionIndex].id ?? 0,
        "${LocalGame.quizQuestion?.questions?[questionIndex].answer}",
        answerIndex);
    Navigator.pop(context);
  }

  //endregion

  //遊戲規則
  Widget _gameRule(GameTypeModel set) {
    return Container(
      padding:
          EdgeInsets.symmetric(horizontal: AppSize.defaultPadding, vertical: 5),
      color: Colors.white.withOpacity(0.7),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //遊戲規則
          Row(
            children: [
              Icon(
                Icons.insert_drive_file,
                color: AppColor.appMainColor,
              ),
              const Text(
                "遊戲規則",
                style: TextStyle(
                    color: AppColor.appMainColor,
                    fontSize: AppSize.dialogMainTextSize),
              )
            ],
          ),
          RichText(
              text: TextSpan(
                  text: '1.使用DM幣',
                  style: TextStyle(color: Colors.black, fontSize: 18),
                  children: [
                TextSpan(
                    text: ' 1 ',
                    style: TextStyle(
                      color: AppColor.textRedColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    )),
                TextSpan(
                    text: '枚,可獲得',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    )),
                TextSpan(
                    text: ' 1 ',
                    style: TextStyle(
                      color: AppColor.textRedColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    )),
                TextSpan(
                    text: '次${set.name}機會',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    )),
              ])),
          RichText(
              text: TextSpan(
                  text:
                      '2.活動日期:${set.timeToOpen?.replaceAll('T', ' ')}~${set.timeToClose?.replaceAll('T', ' ')}',
                  style: const TextStyle(color: Colors.black, fontSize: 18),
                  children: [])),
          if (set.id == LocalGame.bingo.id)
            RichText(
                text: const TextSpan(
              text: '3.Apple 並非贊助商，也不以任何方式參與競賽或抽獎活動',
              style: TextStyle(color: Colors.black, fontSize: 18),
            )),
        ],
      ),
    );
  }
}
