import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/enum/app_enum.dart';
import 'package:com.sj.stu_mall_app/enum/dm_record.dart';
import 'package:com.sj.stu_mall_app/providers/record_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/record_models.dart';
import '../routers/route_name.dart';
import '../save_model/record.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';
import '../widgets/app_widget.dart';
import '../widgets/dm_instruction_card.dart';
import '../widgets/point_card.dart';

class MyDmCoinRoute extends StatefulWidget {
  const MyDmCoinRoute({Key? key}) : super(key: key);

  @override
  State<MyDmCoinRoute> createState() => _MyDmCoinRouteState();
}

class _MyDmCoinRouteState extends State<MyDmCoinRoute> {
  RecordProvider? _recordProvider;
  int select = AppEnum.income;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) {
    _recordProvider!.getDmRecords();
  }

  @override
  Widget build(BuildContext context) {
    _recordProvider = Provider.of<RecordProvider>(context);
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleBarIntegrate('我的DM幣', [_backButton()]),
          _tradeWidget(),
          _subTitle(),
          _pointWidget(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
            child: Text(
              '使用紀錄',
              textScaleFactor: 1,
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            ),
          ),
          _theData(),
        ],
      ),
    );
  }

  //會員點數
  Widget _pointWidget() {
    return Container(
      height: 110,
      margin: EdgeInsets.symmetric(
          vertical: 10, horizontal: AppSize.defaultPadding),
      width: AppSize.appWidth,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: AppColor.buttonOrange, width: 3)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //會員點數
          PointsCard(),
          //使用說明
          DmInstructionCard(),
        ],
      ),
    );
  }

  Widget _subTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [AppColor.shadow],
        color: Colors.white,
      ),
      child: Row(children: [
        _subTitleCard(AppEnum.income),
        _subTitleCard(AppEnum.outcome),
      ]),
    );
  }

  Widget _subTitleCard(int set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            set == AppEnum.income ? "收入" : "支出",
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _subTitleCardOnTap(int set) {
    select = set;
    setState(() {});
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }

  Widget _theData() {
    return Expanded(
      child: ListView.builder(
          itemCount: _recordProvider!.getDmRecordList(select).length,
          itemBuilder: (context, index) {
            return _buildExchangeRecord(
                _recordProvider!.getDmRecordList(select)[index]);
          }),
    );
  }

  Widget _buildExchangeRecord(RecordModel set) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: AppSize.defaultPadding, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                          width: 35,
                          child: Image.asset(AppImage.iconIcon)),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          "${set.operation?.label}",
                          textScaleFactor: 1,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(
                        '${Util.dateFormatHHMM.format(Util().utcTimeToLocal(set.createdAt ?? ' '))}',
                        style: TextStyle(fontSize: 20),
                      ))
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            child: Row(children: [
              Container(
                child: Text(
                  set.types == AppEnum.income ? '+' : '-',
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: set.types == AppEnum.income
                          ? Colors.red
                          : Colors.black,
                      fontSize: 20),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 20),
                alignment: Alignment.centerRight,
                child: Text(
                  "${set.coin}",
                  textScaleFactor: 1,
                  style: TextStyle(
                      color: set.types == AppEnum.income
                          ? Colors.red
                          : Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }

  Widget _tradeWidget() {
    return GestureDetector(
      onTap: (){
        delegate.push(name: RouteName.pointTransaction);
      },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding,vertical: 5),
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                AppImage.iconTransaction,width: 50,
              ),
              Text('DM幣轉帳',textScaleFactor: 1,
                  style:
                  TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
              Text('收/付款',textScaleFactor: 1,
                  style: TextStyle(
                    fontSize: 22,
                  )),
              const Spacer(),
              Container(child: Icon(Icons.navigate_next)),
            ],
          ),
        ));
  }
}
