import 'package:flutter/material.dart';

import '../app.dart';
import '../enum/dm_transaction.dart';
import '../utils/app_color.dart';
import '../widgets/app_widget.dart';
import 'page/dm_payment.dart';
import 'page/dm_receive.dart';

class PointTransactionRoute extends StatefulWidget {
  const PointTransactionRoute({Key? key}) : super(key: key);

  @override
  State<PointTransactionRoute> createState() => _PointTransactionRouteState();
}

class _PointTransactionRouteState extends State<PointTransactionRoute> {
  PageController _pageController = PageController();
  TransactionEnum select = TransactionEnum.get;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          titleBarIntegrate('DM幣轉帳', [_backButton()]),
          _subTitle(),
          Expanded(
            child: PageView(
              controller: _pageController,
              children: [DmReceivePage(),DmPaymentPage()],
              onPageChanged: (index) {
                setState(() {
                  select = TransactionEnum.values[index];
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _subTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [AppColor.shadow],
        color: Colors.white,
      ),
      child: Row(children: [
        _subTitleCard(TransactionEnum.get),
        _subTitleCard(TransactionEnum.pay),
      ]),
    );
  }

  Widget _subTitleCard(TransactionEnum set) {
    return Expanded(
      child: GestureDetector(
        onTap: () => _subTitleCardOnTap(set),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: select == set
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: AppColor.appMainColor)
              : null,
          child: Text(
            set.label,
            style: TextStyle(
                color: select == set ? Colors.white : Colors.black,
                fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _subTitleCardOnTap(TransactionEnum set) {
    _pageController.animateToPage(set.index,
        duration: Duration(milliseconds: 300), curve: Curves.decelerate);
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popRoute();
      },
      child: backButtonWidget(),
    );
  }
}
