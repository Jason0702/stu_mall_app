
import 'package:flutter/material.dart';
import '../app.dart';
import '../enum/welcome_route.dart';

import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../widgets/app_widget.dart';
import 'page/login.dart';
import 'page/register.dart';

class WelcomePage extends StatefulWidget {
  @override
  State createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> with WidgetsBindingObserver {
  late PageController _pageController;
  late PageView _pageView;
  WelcomeEnum _currentPage = WelcomeEnum.login;

  _onLayoutDone(_) {}

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_onLayoutDone);
    _pageController = PageController();
    _pageView = PageView(
      controller: _pageController,
      children: [LoginPage(), RegisterPage()],
      onPageChanged: (index) {
        setState(() {
          _currentPage = WelcomeEnum.values[index];
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            //Title
            titleBarIntegrate('${_currentPage.label}',[_backButton()]),
            //Body
            Expanded(
              child: _bodyWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        delegate.popUntilPage(name: RouteName.home);
      },
      child: backButtonWidget(),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Container(
      width: AppSize.appWidth,
      height: AppSize.appHeight,
      alignment: Alignment.center,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            child: Image(
              image: AssetImage(AppImage.logoBlack),
              height: 140,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 50,
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.symmetric(horizontal: AppSize.defaultPadding),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: AppColor.backgroundColor,
                boxShadow: [AppColor.shadow]),
            child: Row(
              children: [
                Expanded(child: switchButton(WelcomeEnum.login)),
                Expanded(child: switchButton(WelcomeEnum.register)),
              ],
            ),
          ),
          Expanded(flex: 5, child: _pageView)
        ],
      ),
    );
  }

  Widget switchButton(WelcomeEnum set) {
    return Container(
      decoration: _currentPage == set
          ? BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: AppColor.appMainColor)
          : null,
      child: Center(
        child: GestureDetector(
          onTap: () => switchButtonOnTap(set),
          child: Text(
            set.label,
            textScaleFactor: 1,
            style: TextStyle(
                color: _currentPage == set ? Colors.white : Colors.black,
                fontSize: 19),
          ),
        ),
      ),
    );
  }

  void switchButtonOnTap(WelcomeEnum set) {
    _pageController.animateToPage(set.index,
        duration: Duration(milliseconds: 300), curve: Curves.decelerate);
  }
}
