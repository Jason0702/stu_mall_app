class ForgetPasswordModel {
  final String account;
  final String cellphone;

  const ForgetPasswordModel({required this.account, required this.cellphone});

  Map<String, dynamic> toMap() => {
    'account': account,
    'cellphone': cellphone
  };
}
