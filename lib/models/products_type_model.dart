class ProductsTypeModel{
  int? id;
  String? name;

  ProductsTypeModel(this.id, this.name);

  ProductsTypeModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
  };



}