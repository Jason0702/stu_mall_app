import 'dart:convert';

class GameModel{
  int? id;
  int? status;
  String? title;
  String? startTime;
  String? endTime;
  List<QuestionsModel>? questions;

  GameModel(this.title,this.startTime,this.id,this.status,this.endTime);

  GameModel.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        startTime = json['start_time'],
        id = json['id'],
        status = json['status'],
        questions =  []..addAll(
            (json['questions'] as List).map((o) => QuestionsModel.fromJson(o))
        ),
        endTime = json['end_time'];

  Map<String, dynamic> toJson() => {
    'title': title,
    'start_time': startTime,
    'id': id,
    'status': status,
    'end_time': endTime,
  };

}

class QuestionsModel{

  int? id;
  int? status;
  String? title;
  List? answer;
  List? options;

  QuestionsModel({this.title,this.id,this.status,this.options,this.answer});


  factory  QuestionsModel.fromJson(Map<String, dynamic> json){
    return QuestionsModel(
      title : json['title'],
      id : json['id'],
      status : json['status'],
      options : jsonDecode(json['options']),
      answer : jsonDecode(json['answer']),
    );
  }


  Map<String, dynamic> toJson() => {
    'title': title,
    'id': id,
    'status': status,
    'answer': answer,
    'options': options,
  };
}