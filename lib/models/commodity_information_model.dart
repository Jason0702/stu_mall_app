class CommodityInformationModel{
  String? imageUrl;
  String? title;
  String? price;
  String? indexText;

  CommodityInformationModel({this.imageUrl, this.title, this.price, this.indexText});
}