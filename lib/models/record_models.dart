import '../enum/dm_record.dart';

class RecordModel {
  DmOperationType? operation;
  int? types; //   0:支出 1 :收入
  int? coin;
  int? ordersId;
  String? createdAt;
  int? point;
  String? transactionNumber;

  RecordModel(this.operation, this.types, this.coin, this.ordersId,
      this.createdAt, this.point, this.transactionNumber);

  RecordModel.fromJson(Map<String, dynamic> json)
      : operation = DmOperationType.values[json['operation'] ?? 0],
        types = json['types'],
        ordersId = json['orders_id'],
        point = json['point'],
        transactionNumber = json['transaction_number'],
        createdAt = json['created_at'],
        coin = json['coin'];

  Map<String, dynamic> toJson() => {
        'operation': operation,
        'types': types,
        'coin': coin,
        'transaction_number': transactionNumber,
        'point': point,
        'ordersId': ordersId,
        'createdAt': createdAt,
      };
}
