class ReceiverModels {
  String? name;
  String? cellphone;
  int? gender;
  String? address;
  String? city;
  String? area;

  ReceiverModels({
    required this.name,
    required this.cellphone,
    required this.gender,
    required this.address,
    required this.city,
    required this.area,
  });

  ReceiverModels.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        cellphone = json['cellphone'],
        gender = json['gender'],
        city = json['city'],
        area = json['area'],
        address = json['address'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'cellphone': cellphone,
        'gender': gender,
        'city': city,
        'area': area,
        'address': address,
      };
}
