class EventMessageModel{
  String? imageUrl;
  String? title;
  String? time;
  String? indexText;

  EventMessageModel({this.imageUrl, this.title, this.time, this.indexText});
}