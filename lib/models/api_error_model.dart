class ApiErrorModel {
  int? code;
  String? data;
  String? message;

  ApiErrorModel({this.code, this.data, this.message});

  ApiErrorModel.from(Map<String, dynamic> json) :
        code = json['code'],
        data = json['data'],
        message = json['message'];

  Map<String, dynamic> toJson() => {
    'code': code,
    'data':data,
    'message':message
  };
}