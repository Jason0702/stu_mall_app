class HeroViewParamsModel{
  final String tag;
  final dynamic data;

  const HeroViewParamsModel({
    required this.tag,
    required this.data
  });
}