import 'dart:convert';

import 'shop_carts_models.dart';

class OrderModel {
  String? orderNumber;
  String? createdAt;
  int? paymentType;
  int? deliveryOption;
  String? recvName;
  String? recvCellphone;
  String? recvAddress;
  int? payAmount;
  List<ShoppingCartsModel> productInfo = [];

  int? isShipping;
  int? isTaked;
  int? isPayment;
  int? status;

  String? smartDeviceNo;

  OrderModel(
      this.orderNumber,
      this.createdAt,
      this.paymentType,
      this.deliveryOption,
      this.recvName,
      this.recvCellphone,
      this.recvAddress,
      this.payAmount,
      this.productInfo,
      this.isPayment,
      this.isTaked,
      this.isShipping);

  OrderModel.fromJson(Map<String, dynamic> json) {
    orderNumber = json['order_number'];
    createdAt = json['created_at'];
    paymentType = json['payment_type'];
    deliveryOption = json['delivery_option'];
    recvName = json['recv_name'];
    recvCellphone = json['recv_cellphone'];
    recvAddress = json['recv_address'];
    isShipping = json['is_shipping'];
    isTaked = json['is_taked'];
    isPayment = json['is_payment'];
    payAmount = json['pay_amount'];
    status = json['status'];
    List list = jsonDecode(json['product_info']);
    productInfo = list.map((e) => ShoppingCartsModel.fromOrderJson(e)).toList();
    smartDeviceNo = json['smart_device_no'] ?? '';
  }

  Map<String, dynamic> toJson() => {
        'order_number': orderNumber,
        'status': status,
        'created_at': createdAt,
        'is_payment': isPayment,
        'is_taked': isTaked,
        'is_shipping': isPayment,
        'payment_type': paymentType,
        'delivery_option': deliveryOption,
        'recvName': recvName,
        'recvCellphone': recvCellphone,
        'recvAddress': recvAddress,
        'payAmount': payAmount,
        'productInfo': productInfo,
        'smartDeviceNo': smartDeviceNo
      };
}
