class RankingModel{
  String? studentId;
  int? dmCoin;
  String? name;
  int? point;

  RankingModel(this.studentId,this.dmCoin,this.point,this.name);

  RankingModel.fromJson(Map<String, dynamic> json)
      : studentId = json['member']['student_id'],
        dmCoin = json['dm_coin'],
        name = json['member']['name'],
        point = json['point'];

  Map<String, dynamic> toJson() => {
    'id': studentId,
    'dmCoin': dmCoin,
    'name': name,
    'point': point,
  };



}