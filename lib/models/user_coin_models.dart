class CoinModels{
  int? point;
  int? dmCoin;
  int? sticker;
  String? checkInAt;

  CoinModels(this.point,this.dmCoin,this.checkInAt);


  CoinModels.fromJson(Map<String, dynamic> json):
        point = json['point'],
        checkInAt = json['check_in_at'],
        sticker = json['sticker'],
        dmCoin = json['dm_coin'];

  Map<String, dynamic> toJson() => {
    'point':point,
    'check_in_at':checkInAt,
    'sticker':sticker,
    'dm_coin':dmCoin,
  };
}