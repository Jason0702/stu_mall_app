class ShoppingCartsModel {
  int? id;
  String? name;
  int? productsId;
  int? qty;
  int? price;
  String? photo;

  ShoppingCartsModel({this.id,required this.productsId,required this.qty,this.name,this.price});

  ShoppingCartsModel.fromJson(Map<String, dynamic> json)
      : productsId = json['products_id'],
        qty = json['qty'],
        name = json['Product']['name'],
        price = json['Product']['price'],
        photo = json['Product']['photo'],
        id = json['id'];

  ShoppingCartsModel.fromOrderJson(Map<String, dynamic> json)
      : productsId = json['products_id'],
        qty = json['qty'],
        name = json['name'],
        price = json['price'],
        id = json['id'];

 /* Map<String, dynamic> toJson() => {
    'id': id,
    'products_id': productsId,
    'name': name,
    'qty': qty,
  };*/

  Map<String, dynamic> toProductInfo() => {
    'id': productsId,
    'price': price,
    'name': name,
    'qty': qty,
  };

  Map<String, dynamic> forLine() => {
    'price': price,
    'name': name,
    'quantity': qty,
  };
}
