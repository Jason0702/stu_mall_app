class ProductsModel{
  int? id;
  int? productsTypeId;
  String? name;
  int? price;
  String? description;
  String? photo;
  String? eslItemStatus;
  int? status;
  int? qty;
  ProductsModel(this.id,this.productsTypeId, this.name, this.price, this.description, this.photo,this.eslItemStatus,this.qty);

  ProductsModel.fromJson(Map<String, dynamic> json)
      : productsTypeId = json['product_types_id'],
        id = json['id'],
        name = json['name'],
        price = json['price'],
        description = json['description'],
        photo = json['photo'],
        eslItemStatus = json['esl_item_status'],
        qty = json['inventory'],
        status = json['status'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'product_types_id': productsTypeId,
    'inventory': qty,
    'name': name,
    'price': price,
    'description': description,
    'photo': photo,
    'esl_item_status': eslItemStatus,
    'status': status,
  };



}