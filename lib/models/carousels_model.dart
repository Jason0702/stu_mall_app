class CarouselsModel {
  String? createdAt;
  String? title;
  int? id;
  String? body;
  int? audience;
  int? status;

  CarouselsModel(this.createdAt, this.title, this.id, this.body, this.audience,this.status);

  CarouselsModel.fromJson(Map<String, dynamic> json)
      : createdAt = json['created_at'],
        title = json['title'],
        id = json['id'],
        body = json['body'],
        status = json['status'],
        audience = json['audience'];

  Map<String, dynamic> toJson() => {
    'createdAt': createdAt,
    'title': title,
    'id': id,
    'status': status,
    'body': body,
    'audience': audience,
  };
}
