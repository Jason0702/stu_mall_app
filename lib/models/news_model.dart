class NewsModel {
  String? createdAt;
  String? title;
  String? pic;
  String? body;
  int? status;
  int? types;

  NewsModel(this.createdAt, this.title, this.pic, this.body, this.status,this.types);

  NewsModel.fromJson(Map<String, dynamic> json)
      : createdAt = json['created_at'],
        title = json['title'],
        pic = json['pic'],
        body = json['body'],
        types = json['types'],
        status = json['status'];

  Map<String, dynamic> toJson() => {
        'createdAt': createdAt,
        'title': title,
        'pic': pic,
        'body': body,
        'types': types,
        'status': status,
      };
}
