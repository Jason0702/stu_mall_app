class TokenModel{
  int? id;
  String token = "";
  String? refreshToken;
  bool? forceUpdatePassword;

  TokenModel({required this.id, required this.token, required this.refreshToken, required this.forceUpdatePassword});

  TokenModel.fromJson(Map<String, dynamic> responseData) {
    id = responseData['id'] ?? 0;
    token = responseData['token']['access_token'] ?? '';
    refreshToken = responseData['token']['refresh_token']?? 0;
    forceUpdatePassword = responseData['force_update_password'] ?? false;
  }
}