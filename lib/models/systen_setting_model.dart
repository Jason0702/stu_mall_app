import 'dart:convert';

class SystemSettingModel{

  Map<String, dynamic>? paymentType;
  Map<String, dynamic>? shippmentType;
  int? exchangeRatePointToDm;
  String? instructions;


  SystemSettingModel(this.paymentType, this.shippmentType,);

  SystemSettingModel.fromJson(Map<String, dynamic> json){
    instructions = json['instructions'] ?? '';
    paymentType = jsonDecode(json['payment_type'] ?? '{}');
    exchangeRatePointToDm = json['exchange_rate_point_to_dm'];
    shippmentType = jsonDecode(json['shippment_type']?? '{}') ;
  }

  Map<String, dynamic> toJson() => {
    "payment_type": paymentType,
    "instructions":instructions,
    "exchange_rate_point_to_dm" :exchangeRatePointToDm,
    'shippment_type': shippmentType,
  };
}

class PaymentTypeModel{
  int? status;

  PaymentTypeModel({ this.status});

  factory PaymentTypeModel.fromJson(Map<String, dynamic> json){
    return PaymentTypeModel(status: json['status']);
  }

  Map<String, dynamic> toJson() => {
    'status':status,
  };
}

class ShippmentTypeModel{
  int? status;
  int? fee;
  int? freeThreshold;
  int? freeStatus;

  ShippmentTypeModel({ this.status,this.fee,this.freeStatus,this.freeThreshold});

  factory ShippmentTypeModel.fromJson(Map<String, dynamic> json){
    return ShippmentTypeModel(
      status: json['status'],
      fee: json['fee'],
      freeThreshold: json['free_threshold'],
      freeStatus: json['free_status'],

    );
  }

  Map<String, dynamic> toJson() => {
    'status':status,
  };
}