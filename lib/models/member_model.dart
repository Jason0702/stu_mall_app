import '../enum/role.dart';

class MemberModel {
  String? studentId;
  String? account;
  String? cellphone;
  String? name;
  String? mugshot;
  String? email;
  memberRoleEnum? role;
  int? id;

  MemberModel(this.studentId, this.account, this.cellphone, this.name,
      this.mugshot, this.email);

  MemberModel.fromJson(Map<String, dynamic> json)
      : studentId = json['student_id'],
        account = json['account'],
        cellphone = json['cellphone'],
        name = json['name'],
        role = memberRoleEnum.values[json['role']],
        id = json['id'],
        mugshot = json['mugshot'],
        email = json['email'];

  Map<String, dynamic> toJson() => {
        'studentId': studentId,
        'account': account,
        'role': role?.index,
        'cellphone': cellphone,
        'name': name,
        'id': id,
        'mugshot': mugshot,
        'email': email,
      };
}
