class GameTypeModel{
  String? name;
  String? timeToOpen;
  String? timeToClose;
  String? enName;
  int? status;
  int? id;

  GameTypeModel(this.id,this.name,this.timeToOpen,this.timeToClose,this.status);

  GameTypeModel.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        id = json['id'],
        enName = json['en_name'],
        timeToOpen = json['time_to_open'],
        timeToClose = json['time_to_close'],
        status = json['status'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'en_name':enName,
    'time_to_open': timeToOpen,
    'time_to_close': timeToClose,
    'status': status,
  };

}