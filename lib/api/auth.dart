import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import '../models/turple_model.dart';
import 'api.dart';

class AuthApi {
  ///memberLogin
  ///String account [name]
  ///String password [password]
  /// string : id
  Future<Tuple<bool, String>> memberLogin(String name, String password) async {
    log('${name} ${password}');
    try {
      dynamic response = await Api.httpUtils.post(
        Api.memberLogin,
        data: {'account': name, 'password': password, 'id': "1"},
        options: Options(
            contentType: 'application/json',),
      );
      log('會員登入 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('會員登入 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
