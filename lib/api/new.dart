import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import '../models/turple_model.dart';
import 'api.dart';

class NewApi {
  ///取得活動消息
  Future<Tuple<bool, String>> getNews() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getNews,
        options: Options(contentType: 'application/json', headers: {
          "authorization": 'Basic ${base64.encode(utf8.encode('guest:guest'))}'
        }),
      );
      //log('取得活動消息 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得活動消息 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  ///取得輪播
  Future<Tuple<bool, String>> getCarousels() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getCarousels,
        options: Options(contentType: 'application/json', headers: {
          "authorization": 'Basic ${base64.encode(utf8.encode('guest:guest'))}'
        }),
      );
      //log('取得輪播 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得輪播 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
