import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import '../models/shop_carts_models.dart';
import '../models/turple_model.dart';
import 'api.dart';

class ShoppingCartApi {
  //取得購物車
  Future<Tuple<bool, String>> getShoppingCart(int memberId) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.shoppingCartsByMember}/$memberId',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('取得購物車 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得購物車 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //刪除所有購物車
  Future<Tuple<bool, String>> deleteAllShoppingCart(int memberId) async {
    try {
      dynamic response = await Api.httpUtils.delete(
        '${Api.shoppingCartsByMember}/$memberId',
        data: {
          'id': memberId,
        },
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('刪除所有購物車 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('刪除所有購物車 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //新增購物車
  Future<Tuple<bool, String>> addShoppingCart(
      int memberId, ShoppingCartsModel shoppingCartsModel) async {
    Map<String, dynamic> map = {
      'products_id': shoppingCartsModel.productsId,
      'members_id': memberId,
      'qty': shoppingCartsModel.qty,
    };
    try {
      dynamic response = await Api.httpUtils.post(
        Api.shoppingCarts,
        data: map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('新增購物車 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('新增購物車 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //更新購物車
  Future<Tuple<bool, String>> updatedShoppingCart(
      ShoppingCartsModel shoppingCartsModel) async {
    Map<String, dynamic> map = {
      'id': shoppingCartsModel.id,
      'qty': shoppingCartsModel.qty,
    };
    try {
      dynamic response = await Api.httpUtils.put(
        '${Api.shoppingCarts}/${shoppingCartsModel.id}',
        data: map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('更新購物車 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('更新購物車 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //刪除購物車
  Future<Tuple<bool, String>> deleteShoppingCart(int shoppingCartId) async {
    Map<String, dynamic> map = {
      'id': shoppingCartId,
    };
    try {
      dynamic response = await Api.httpUtils.delete(
        '${Api.shoppingCarts}/$shoppingCartId',
        data: map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('刪除購物車 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('刪除購物車 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
