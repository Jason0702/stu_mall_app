import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import '../models/turple_model.dart';
import 'api.dart';

class ProductApi {

  Future<Tuple<bool, String>> getProductTypes() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getProductsType,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": 'Basic ${base64.encode(utf8.encode('guest:guest'))}'}),
      );
      //log('取得產品類別 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得產品類別 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }


  Future<Tuple<bool, String>> getProducts() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getProducts,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": 'Basic ${base64.encode(utf8.encode('guest:guest'))}'}),
      );
      //log('取得產品 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得產品 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
