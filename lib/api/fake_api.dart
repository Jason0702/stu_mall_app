import 'dart:convert';

class FakeApi {
  static Map<String, dynamic> fakeSystemSetting = {
    "id": 1,
    "created_at": "2020-08-21T17:32:47+08:00",
    "updated_at": "2022-02-10T13:28:39+08:00",
    "registration_declare_checkout": "<p>結帳機註冊畫面宣告</p>",
    "registration_declare_app": "<p>APP註冊畫面宣告</p>",
    "payment_type": json.encode({
      "2": {"status": 0},
      "3": {"status": 0},
      "6": {"status": 1}
    }),
    "shippment_type": json.encode({
      "0": {"fee": 120, "free_threshold": 0, "free_status": 0, "status": 0},
      "1": {"fee": 70, "free_threshold": 0, "free_status": 0, "status": 0},
      "2": {"fee": 0, "free_threshold": 0, "free_status": 1, "status": 1},
      "3": {"fee": 0, "free_threshold": 0, "free_status": 1, "status": 1}
    }),
    "layout_type": 1,
    "privacy_policy": "",
    "exchange_rate_point_to_dm": 5,
    "instructions": """<p>1.DM幣使用說明</p>

<p>2.DM幣使用說明</p>

<p>3.DM幣使用說明</p>

<p>4.DM幣使用說明</p>

<p>5.DM幣使用說明</p>

<p>&nbsp;</p>"""
  };
}
