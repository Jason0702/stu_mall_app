import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';

import '../models/turple_model.dart';
import 'api.dart';

class GameApi {
  //取得遊戲資料
  Future<Tuple<bool, String>> getGameTypeData() async {
    try {
      dynamic response = await Api.httpUtils.get(
          Api.gameType,
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('取得遊戲資料 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得遊戲資料 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  Future<Tuple<bool, String>> playGame(int gameId, int memberId) async {
    try {
      dynamic response = await Api.httpUtils.put(
          '${Api.gameType}/$gameId/play',
          data: {
            'members_id': memberId,
          },
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('玩遊戲 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('玩遊戲 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //取得遊戲是否有遊玩
  Future<Tuple<bool, String>> getGameTodayIsPlay(
      int gameId, int memberId, String time) async {
    try {
      dynamic response = await Api.httpUtils.get(
          '${Api.getGameTodayIsPlay}/$gameId/member/$memberId/data/$time',
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('取得遊戲是否有遊玩 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得遊戲是否有遊玩 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //取得抽獎活動
  Future<Tuple<bool, String>> getActivity() async {
    try {
      dynamic response = await Api.httpUtils.get(
          Api.activitys,
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('取得抽獎活動 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得抽獎活動 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //取得抽獎活動紀錄
  Future<Tuple<bool, String>> getActivityLog(int quizId, int memberId) async {
    try {
      dynamic response = await Api.httpUtils.get(
          '${Api.answerLogByActivity}/$quizId/member/$memberId',
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('取得抽獎活動紀錄 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得抽獎活動紀錄 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

  //新增問答紀錄
  Future<Tuple<bool, String>> postAnswerLog(Map<String, dynamic> set) async {
    try {
      dynamic response = await Api.httpUtils.post(
          Api.answerLog,
          data: set,
          options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"},
          ));
      log('新增問答紀錄 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('新增問答紀錄 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
