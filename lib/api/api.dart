import 'dart:convert';
import 'dart:developer';
import 'package:com.sj.stu_mall_app/enum/app_enum.dart';
import '../../protos/client_channel.dart';
import '../dio_http/http_request.dart';

late GRPCClientSingleton singleton;

class Api {
  static late Http httpUtils;
  //樹科大120.119.99.12
  static const String baseUrl = 'http://120.119.99.12:5030';
  //static const String baseUrl = 'http://103.136.211.215:5040';
  static const String gRpcPosition = '120.119.99.12';
  //static const String gRpcPosition = '103.136.211.215';
  static const int po = 50081;
  static String accessToken = "";
  static String refreshToken = "";
  static bool forceUpdatePassword = false;
  ///取得檔案
  static const String getFile = '/upload/';//後面帶檔名

//本地
/*String api = 'http://192.168.1.117:5030/api/v1';
String gRpcPosition = '192.168.1.117';
int po = 50081;
//本地圖片
String public = 'http://192.168.1.101:5030/public/';*/

//用戶
  static const String password = '/api/v1/password';
  static const String getMembersData = "/api/v1/members";

//會員
  static const String memberLogin = '/api/v1/members/auth';
  static const String membersForgotPassword = '/api/v1/members-forgot-password';
  static const String getMemberPoint = '/api/v1/member-points-by-members-id';
  static const String getMembersCheckIn = '/api/v1/members-check-in';
  static const String getMembersPointConvertToDmCoin =
      '/api/v1/members-point-convert-to-dm-coin';
  //+id + sticker/10
  static const String membersStickerConvertToPoint =
      '/api/v1/members-sticker-convert-to-point';

//產品
  static const String getProducts = '/api/v1/guest/products';
//今日是否遊玩 + $gameId/member/$id/data/$timestamp
  static const String getGameTodayIsPlay = '/api/v1/member-game-logs-by-game-member-date-like/game';



//產品型號
  static const String getProductsType = '/api/v1/guest/product-types';

//建立定單
  static const String postOrders = '/api/v1/orders';

//新聞
  static const String getNews = '/api/v1/guest/news';

//輪播
  static const String getCarousels = '/api/v1/guest/carousels';

//抽抽樂
  static const String getMembersDrawingGame = '/api/v1/members-drawing-game/';

//購物車
  static const String shoppingCartsByMember =
      '/api/v1/shopping-carts-by-members-id';
  static const String shoppingCarts = '/api/v1/shopping-carts';


//購物紀錄
  static const String getOrderLists = '/api/v1/orders-by-members-id/';

//點數紀錄
  static const String getPointLists =
      '/api/v1/point-use-records-by-members-id/';

//DM幣排行榜
  static const String getDMRanking =
      '/api/v1/member-points-last-n-order-by-dm-coin/${AppEnum.getRankMember}';

//點數排行榜
  static const String getPointRanking =
      '/api/v1/member-points-last-n-order-by-point/${AppEnum.getRankMember}';

//轉帳
  static const String postTransfer = '/api/v1/dm-coin-transfer';

//取得DM幣使用紀錄
  static const String getRecord = '/api/v1/dm-coin-use-records-by-members-id/';
//系統設定
  static const String systemSettingGuest = '/api/v1/guest/system-settings';

//抽獎活動
  static const String activitys = '/api/v1/activitys';

//回答題目
  static const String answerLog = '/api/v1/member-answer-logs';

//取得答題記錄
  static const String answerLogByActivity =
      '/api/v1/member-answer-logs-by-activity-and-members-id/activity';

//QRcode
  static const String postQrcode = '/api/v1/members/generate-qrcode';

//確認帳號是否存在
  static const String accountExits = '/api/v1/members-exist-by-account';

//
  static const String gameType = '/api/v1/game-types';

  //忘記密碼
  static const String forgetPassword = '/api/v2/members-forgot-password';

//天氣
  static const String getWeatherApi =
      'https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/O-A0001-001?Authorization=CWB-331DC27D-8E34-481B-9D2B-AE80016CA6FB&format=JSON';

  static void initHttp(){
    httpUtils = Http();
    httpUtils.init(baseUrl: baseUrl);
  }
}

