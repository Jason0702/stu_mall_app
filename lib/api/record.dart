import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import '../models/turple_model.dart';
import 'api.dart';

class RecordApi {
  //取得點數排行榜
  Future<Tuple<bool, String>> getPointRanking() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getPointRanking,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('取得點數排行榜 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得點數排行榜 Error: ${e.response?.statusCode}');
         return Tuple<bool, String>(false, e.message);
    }
  }

  //取得訂單記錄
  Future<Tuple<bool, String>> getOrderRecords(int memberId) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getOrderLists}/$memberId',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('取得訂單記錄 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得訂單記錄 Error: ${e.response?.statusCode}');
         return Tuple<bool, String>(false, e.message);
    }
  }

  //取得金幣排行榜
  Future<Tuple<bool, String>> getDMRanking() async {
    try {
      dynamic response = await Api.httpUtils.get(
        Api.getDMRanking,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('取得金幣排行榜 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得金幣排行榜 Error: ${e.response?.statusCode}');
         return Tuple<bool, String>(false, e.message);
    }
  }

  //取得dm幣使用記錄
  Future<Tuple<bool, String>> getDmRecord(int memberId) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getRecord}/$memberId',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('取得dm幣使用記錄 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得dm幣使用記錄 Error: ${e.response?.statusCode}');
         return Tuple<bool, String>(false, e.message);
    }
  }

  //取得點數使用記錄
  Future<Tuple<bool, String>> getPointsRecord(int memberId) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getPointLists}/$memberId',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('取得點數使用記錄 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得點數使用記錄 Error: ${e.response?.statusCode}');
         return Tuple<bool, String>(false, e.message);
    }
  }
}
