import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';

import '../models/turple_model.dart';
import 'api.dart';

class OrderApi {
  ///發送訂單
  Future<Tuple<bool, String>> postOrder(Map<String, dynamic> map) async {
    try {
      dynamic response = await Api.httpUtils.post(
        Api.postOrders,
        data: map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('發送訂單 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('發送訂單 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}
