import 'dart:convert';
import 'dart:developer';
import 'package:com.sj.stu_mall_app/enum/app_enum.dart';
import 'package:dio/dio.dart';
import '../models/forget_password_model.dart';
import '../models/turple_model.dart';
import '../utils/firebase_message.dart';
import 'api.dart';

class MemberApi {
  ///取得會員
  Future<Tuple<bool, String>> getMember(int id) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getMembersData}/$id',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('取得會員 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得會員 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///取得會員點數
  Future<Tuple<bool, String>> getMemberPoint(int id) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getMemberPoint}/$id',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('取得會員點數 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('取得會員點數 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///會員簽到
  Future<Tuple<bool, String>> getMembersCheckIn(int id) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getMembersCheckIn}/$id',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('會員簽到 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('會員簽到 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///會員貼紙轉點數
  Future<Tuple<bool, String>> membersStickerConvertToPoint(int id) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.membersStickerConvertToPoint}/$id/sticker/${AppEnum.totalSticker}',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('會員貼紙轉點數 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('會員貼紙轉點數 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///會員點數轉DM幣
  Future<Tuple<bool, String>> getMembersPointConvertToDmCoin(int id,int qty) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.getMembersPointConvertToDmCoin}/$id/point/$qty',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('會員點數轉DM幣 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('會員點數轉DM幣 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///發送取得QRcode
  Future<Tuple<bool, String>> getMemberQrcode(int id) async {
    try {
      dynamic response = await Api.httpUtils.post(
        '${Api.postQrcode}/$id',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('發送取得QRcode response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('發送取得QRcode Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }


  ///更改會員密碼
  Future<Tuple<bool, String>> changeMemberPassword(int id,String newPassword,String password) async {
    Map<String, dynamic> map = {
      // 'id': id,
      'password':password,
      'new_password':newPassword,
    };
    try {
      dynamic response = await Api.httpUtils.put(
        '${Api.getMembersData}/$id/password',
        data:map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('更改會員密碼 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('更改會員密碼 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///更改會員資料
  Future<Tuple<bool, String>> changeMemberData(int id, Map<String, dynamic> map) async {
    try {
      dynamic response = await Api.httpUtils.put(
        '${Api.getMembersData}/$id',
        data:map,
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('更改會員密碼 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('更改會員密碼 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///檢查帳號是否存在
  Future<Tuple<bool, String>> checkAccountExits(String account) async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.accountExits}/$account',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('檢查帳號是否存在 response: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      log('檢查帳號是否存在 Error: ${e.response?.statusCode}');
       return Tuple<bool, String>(false, e.message);
    }
  }

  ///更新FirebaseToken
  Future<Tuple<bool, String>> updateToken(int id)async {
    String? token = await FirebaseMessage.getToken();
    if(token != null){
      Map<String, dynamic> dto = {
        'id': id,
        'fcm_token': token
      };
      try{
        dynamic response = await Api.httpUtils.put(
          '${Api.getMembersData}/$id/fcm-token',
          data: dto,
          options: Options(
              contentType: 'application/json',
              headers: {"authorization": "Bearer ${Api.accessToken}"}),
        );
        return Tuple<bool, String>(true, json.encode(response));
      } on DioError catch (e) {
        log('更新FirebaseToken Error: ${e.message}');
        return Tuple<bool, String>(false, e.message);
      }
    } else {
      return Tuple<bool, String>(false, '未取得Token');
    }
  }

  //忘記密碼
  Future<Tuple<bool, String>> forgetPassword(ForgetPasswordModel data) async {
    try{
      dynamic value = await Api.httpUtils.post(
        '${Api.forgetPassword}',
        data: data.toMap(),
      );
      log('忘記密碼: $value');
      return Tuple<bool, String>(true, value);
    } on DioError catch(e) {
      log('忘記密碼 Error: ${e.response?.statusCode}');
      return Tuple<bool, String>(false, e.message);
    }
  }

}
