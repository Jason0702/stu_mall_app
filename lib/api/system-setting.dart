import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/api/fake_api.dart';
import 'package:dio/dio.dart';

import '../models/turple_model.dart';
import 'api.dart';

class SystemSettingApi {
  ///取得系統設定 未驗證
  Future<Tuple<bool, String>> systemSettingByGuest() async {
    try {
      dynamic response = await Api.httpUtils.get(
        '${Api.systemSettingGuest}/1',
        options: Options(
            contentType: 'application/json',
            headers: {"authorization": 'Basic ${base64.encode(utf8.encode('guest:guest'))}'}),
      );
      response = FakeApi.fakeSystemSetting;
      log('取得系統設定完成: $response');
      return Tuple<bool, String>(true, json.encode(response));
    } on DioError catch (e) {
      //錯誤
      log('取得系統設定Error: ${e.message}');
      return Tuple<bool, String>(false, e.message);
    }
  }
}