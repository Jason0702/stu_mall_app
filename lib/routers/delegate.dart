//路由2.0
import 'package:com.sj.stu_mall_app/route/home_route.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../route/dm_ranking_route.dart';
import '../route/game_route.dart';
import '../route/my_dm_route.dart';
import '../route/my_order_route.dart';
import '../route/dm_transaction_route.dart';
import '../route/page/forget_password.dart';
import '../route/page/member_qrcode.dart';
import '../route/page/order_result.dart';
import '../route/page/payment_result.dart';
import '../route/page/product_info.dart';
import '../route/page/select_pay_ship_page.dart';
import '../route/page/shopping_cart.dart';
import '../route/page/splash_page.dart';
import '../route/personal_data.dart';
import '../route/point_route.dart';
import '../route/qr_coder_router.dart';
import '../route/record_search_route.dart';
import '../route/welcome_route.dart';
import '../providers/current_locale_provider.dart';
import '../utils/app_size.dart';

class CustomizeRouterDelegate extends RouterDelegate<List<RouteSettings>>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<List<RouteSettings>> {
  final List<Page> _pages = [];

  List<Page> get page => _pages;
  @override
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: List.of(_pages),
      onPopPage: _onPopPage,
    );
  }

  @override
  Future<void> setNewRoutePath(List<RouteSettings> configuration) async {
    debugPrint('setNewRoutePath ${configuration.last.name}');

    _setPath(configuration
        .map((routeSettings) => _createPage(routeSettings))
        .toList());
    return Future.value(null);
  }

  void _setPath(List<Page> pages) {
    _pages.clear();
    _pages.addAll(pages);
    if (_pages.first.name != '/') {}
    notifyListeners();
  }

  ///回上一頁
  @override
  Future<bool> popRoute() {
    if (canPop()) {
      _pages.removeLast();
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }

  ///彈出直到
  void popUntilPage({required String name, dynamic arguments}) {
    if (_pages.any((element) => element.name == name)) {
      int lastId = _pages.indexWhere((element) => element.name == name);
      _pages.removeRange(lastId, _pages.length);
      push(name: name, arguments: arguments);
    } else {
      push(name: name, arguments: arguments);
    }
  }

  ///回到第一頁
  Future<bool> popUtil() {
    if (canPop()) {
      Page _pageCopy = _pages.first;
      _pages.clear();
      _pages.add(_pageCopy);
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }

  ///關閉當前畫面並建立下一頁
  void popAndPushRouter({required String name, dynamic arguments}) {
    if (_pages.isNotEmpty) {
      _pages.removeAt(_pages.length - 1);
    }
    push(name: name, arguments: arguments);
  }

  bool canPop() {
    return _pages.length > 1;
  }

  bool _onPopPage(Route route, dynamic result) {
    if (!route.didPop(result)) return false;
    if (canPop()) {
      _pages.removeLast();
      return true;
    } else {
      return false;
    }
  }

  void push({required String name, dynamic arguments}) {
    _pages.add(_createPage(RouteSettings(name: name, arguments: arguments)));
    notifyListeners();
  }

  void replace({required String name, dynamic arguments}) {
    if (_pages.isNotEmpty) {
      _pages.clear();
    }
    push(name: name, arguments: arguments);
  }

  MaterialPage _createPage(RouteSettings routeSettings) {
    Widget child;
    switch (routeSettings.name) {
      case RouteName.splashPage:
        child = const SplashPage();
        break;
      case RouteName.welcome:
        child = WelcomePage();
        break;
      case RouteName.forgetPassword:
        child = ForgotPassword();
        break;
      case RouteName.home:
        child = HomeRoute();
        break;
      case RouteName.memberQrcode:
        child = MemberQrcode();
        break;
      case RouteName.pointsRoute:
        child = PointsRoute();
        break;
      case RouteName.dmRankingRoute:
        child = DmRankingRoute();
        break;
      case RouteName.myOrderRoute:
        child = OrderRoute();
        break;
      case RouteName.myDMRoute:
        child = MyDmCoinRoute();
        break;
      case RouteName.recordSearchRoute:
        child = RecordSearchRoute();
        break;
      case RouteName.gameRouter:
        child = GameRouter();
        break;
      case RouteName.productInfo:
        child = ProductInfo();
        break;
      case RouteName.shoppingCart:
        child = ShoppingCartPage();
        break;
      case RouteName.selectPayment:
        child = SelectPaymentPage();
        break;
      case RouteName.orderResultPage:
        child = OrderResultPage();
        break;
      case RouteName.personalDataPage:
        child = PersonalDataPage();
        break;
      case RouteName.pointTransaction:
        child = PointTransactionRoute();
        break;
      case RouteName.qrCodeRoute:
        child = QrCodeRoute();
        break;
      case RouteName.paymentResult:
        child = PaymentResult();
        break;



      default:
        child = const Scaffold(
          backgroundColor: Colors.white,
          body: Text(
            'error',
            style: TextStyle(color: Colors.black),
          ),
        );
    }
    return MaterialPage(
        child: Consumer<CurrentLocaleProvider>(
          builder: (context, currentLocale, _) {
            AppSize.appWidth = MediaQuery.of(context).size.width;
            AppSize.appHeight = MediaQuery.of(context).size.height;
            return SafeArea(
                top: routeSettings.name == RouteName.splashPage ? false : true,
                bottom: false,
                child: child);
          },
        ),
        key: Key(routeSettings.name!) as LocalKey,
        name: routeSettings.name,
        arguments: routeSettings.arguments);
  }

  Future<bool> _confirmExit() async {
    final result = await showDialog<bool>(
        context: navigatorKey.currentContext!,
        builder: (context) {
          return AlertDialog(
            content: const Text(
              '確定要退出APP嗎?',
              textScaleFactor: 1,
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: const Text(
                  '取消',
                  textScaleFactor: 1,
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: const Text(
                  '確認',
                  textScaleFactor: 1,
                ),
              )
            ],
          );
        });
    return result ?? true;
  }
}
