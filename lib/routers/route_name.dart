class RouteName {
  static const String splashPage = "/splashPage";
  static const String welcome = "/Welcome";
  static const String home = "/Home";
  static const String forgetPassword = "/ForgetPassword";
  static const String memberQrcode = "/MemberQrcode";
  static const String pointsRoute = "/PointsRoute";
  static const String dmRankingRoute = "/DmRankingRoute";
  static const String myOrderRoute = "/OrderRoute";
  static const String myDMRoute = "/MyDmRoute";
  static const String recordSearchRoute = "/RecordSearchRoute";
  static const String gameRouter = "/GameRouter";
  static const String productInfo = "/ProductInfo";
  static const String shoppingCart = "/ShoppingCart";
  static const String selectPayment = "/SelectPayment";
  static const String orderResultPage = "/OrderResultPage";
  static const String personalDataPage = "/PersonalDataPage";
  static const String pointTransaction = "/PointTransactionRoute";
  static const String qrCodeRoute = "/QrCodeRoute";
  static const String paymentResult = "/PaymentResult";


}
