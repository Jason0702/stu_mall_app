import 'dart:io';
import 'package:com.sj.stu_mall_app/utils/firebase_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'app.dart';
import 'providers/auth_provider.dart';
import 'providers/current_locale_provider.dart';
import 'providers/game_provider.dart';
import 'providers/member_provider.dart';
import 'providers/news_provider.dart';
import 'providers/packages_info_provider.dart';
import 'providers/position_provider.dart';
import 'providers/product_provider.dart';
import 'providers/record_provider.dart';
import 'providers/shopping_cart_provider.dart';
import 'providers/util_provider.dart';
import 'routers/parser.dart';
import 'routers/route_name.dart';
import 'api/api.dart';

//入口
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.notification.isDenied.then((value) {
    if (value) {
      Permission.notification.request();
    }
  });
  await FirebaseMessage.firebaseInit();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key) {
    //跳轉
    delegate.replace(name: RouteName.splashPage);
  }

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<SingleChildWidget> _providers = [];
  CurrentLocaleProvider? currentLocaleProvider;
  PackagesInfoProvider? _packagesInfoProvider;
  UtilProvider? _utilProvider;
  AuthProvider? _authProvider;
  @override
  void initState() {
    super.initState();
    initProviders();
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    //HttpUtils.init(baseUrl: Api.baseUrl);
    Api.initHttp();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    if (Platform.isIOS) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack, overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
          statusBarColor: Colors.black,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarIconBrightness: Brightness.dark));
    }
  }

  _onLayoutDone(_) async {
    //取得APP資訊
    _packagesInfoProvider!.getPackageInfo();
    if (Platform.isIOS) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
          overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ));
    }
    _utilProvider!.decodeFile();
    await _utilProvider!.getSystemSetting();
    _authProvider!.autoLogin();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: _providers,
        child: Consumer<CurrentLocaleProvider>(
          builder: (context, currentLocale, _) {
            _packagesInfoProvider = Provider.of<PackagesInfoProvider>(context);
            _utilProvider = Provider.of<UtilProvider>(context, listen: false);
            _authProvider = Provider.of<AuthProvider>(context,listen: false);
            currentLocaleProvider = currentLocale;
            return MaterialApp.router(
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('en'),
                Locale('zh', 'Hant'),
              ],
              debugShowCheckedModeBanner: false,
              routeInformationParser: const CustomizeRouteInformationParser(),
              routerDelegate: delegate,
              builder: (context, child) {
                final MediaQueryData data = MediaQuery.of(context);
                child = EasyLoading.init()(context, child);
                child = Scaffold(
                  backgroundColor: Colors.white,
                  resizeToAvoidBottomInset: false,
                  body: MediaQuery(
                    data: data.copyWith(textScaleFactor: 1, boldText: false),
                    child: GestureDetector(
                        onTap: () {
                          SystemChannels.textInput
                              .invokeMethod('TextInput.hide');
                        },
                        child: child),
                  ),
                );
                return ResponsiveWrapper.builder(child,
                    maxWidth: 1200,
                    minWidth: 480,
                    defaultScale: true,
                    breakpoints: [
                      const ResponsiveBreakpoint.resize(100, name: MOBILE),
                      const ResponsiveBreakpoint.autoScale(900, name: TABLET),
                      const ResponsiveBreakpoint.autoScale(1200, name: DESKTOP),
                      const ResponsiveBreakpoint.resize(2460, name: "4K"),
                    ],
                    background: Container(color: Colors.white));
              },
            );
          },
        ));
  }

  //狀態管理初始化
  void initProviders() {
    _providers
        .add(ChangeNotifierProvider(create: (_) => CurrentLocaleProvider()));
    _providers
        .add(ChangeNotifierProvider(create: (_) => PackagesInfoProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => UtilProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => AuthProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => ProductProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => NewsProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => PositionProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => MemberProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => RecordProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => GameProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => ShoppingCartProvider()));

  }


}
