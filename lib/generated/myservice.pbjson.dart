///
//  Generated code. Do not modify.
//  source: myservice.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use eventValueDescriptor instead')
const EventValue$json = const {
  '1': 'EventValue',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `EventValue`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventValueDescriptor = $convert.base64Decode('CgpFdmVudFZhbHVlEhQKBXZhbHVlGAEgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use getEventRequestDescriptor instead')
const GetEventRequest$json = const {
  '1': 'GetEventRequest',
  '2': const [
    const {'1': 'membersID', '3': 1, '4': 1, '5': 13, '10': 'membersID'},
    const {'1': 'lastEventTime', '3': 2, '4': 1, '5': 9, '10': 'lastEventTime'},
  ],
};

/// Descriptor for `GetEventRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getEventRequestDescriptor = $convert.base64Decode('Cg9HZXRFdmVudFJlcXVlc3QSHAoJbWVtYmVyc0lEGAEgASgNUgltZW1iZXJzSUQSJAoNbGFzdEV2ZW50VGltZRgCIAEoCVINbGFzdEV2ZW50VGltZQ==');
@$core.Deprecated('Use sendEventRequestDescriptor instead')
const SendEventRequest$json = const {
  '1': 'SendEventRequest',
  '2': const [
    const {'1': 'eventID', '3': 2, '4': 1, '5': 13, '10': 'eventID'},
    const {'1': 'value', '3': 3, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `SendEventRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendEventRequestDescriptor = $convert.base64Decode('ChBTZW5kRXZlbnRSZXF1ZXN0EhgKB2V2ZW50SUQYAiABKA1SB2V2ZW50SUQSFAoFdmFsdWUYAyABKAlSBXZhbHVl');
@$core.Deprecated('Use eventReplyDescriptor instead')
const EventReply$json = const {
  '1': 'EventReply',
  '2': const [
    const {'1': 'typeID', '3': 1, '4': 1, '5': 13, '10': 'typeID'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `EventReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventReplyDescriptor = $convert.base64Decode('CgpFdmVudFJlcGx5EhYKBnR5cGVJRBgBIAEoDVIGdHlwZUlEEhQKBXZhbHVlGAIgASgJUgV2YWx1ZQ==');
@$core.Deprecated('Use loginRequestDescriptor instead')
const LoginRequest$json = const {
  '1': 'LoginRequest',
  '2': const [
    const {'1': 'account', '3': 1, '4': 1, '5': 9, '10': 'account'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `LoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginRequestDescriptor = $convert.base64Decode('CgxMb2dpblJlcXVlc3QSGAoHYWNjb3VudBgBIAEoCVIHYWNjb3VudBIaCghwYXNzd29yZBgCIAEoCVIIcGFzc3dvcmQ=');
@$core.Deprecated('Use generalReplyDescriptor instead')
const GeneralReply$json = const {
  '1': 'GeneralReply',
  '2': const [
    const {'1': 'code', '3': 1, '4': 1, '5': 13, '10': 'code'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'data', '3': 3, '4': 1, '5': 9, '10': 'data'},
  ],
};

/// Descriptor for `GeneralReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List generalReplyDescriptor = $convert.base64Decode('CgxHZW5lcmFsUmVwbHkSEgoEY29kZRgBIAEoDVIEY29kZRIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEhIKBGRhdGEYAyABKAlSBGRhdGE=');
@$core.Deprecated('Use logoutRequestDescriptor instead')
const LogoutRequest$json = const {
  '1': 'LogoutRequest',
  '2': const [
    const {'1': 'membersID', '3': 1, '4': 1, '5': 13, '10': 'membersID'},
  ],
};

/// Descriptor for `LogoutRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logoutRequestDescriptor = $convert.base64Decode('Cg1Mb2dvdXRSZXF1ZXN0EhwKCW1lbWJlcnNJRBgBIAEoDVIJbWVtYmVyc0lE');
@$core.Deprecated('Use memberRequestDescriptor instead')
const MemberRequest$json = const {
  '1': 'MemberRequest',
  '2': const [
    const {'1': 'membersID', '3': 1, '4': 1, '5': 13, '10': 'membersID'},
  ],
};

/// Descriptor for `MemberRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List memberRequestDescriptor = $convert.base64Decode('Cg1NZW1iZXJSZXF1ZXN0EhwKCW1lbWJlcnNJRBgBIAEoDVIJbWVtYmVyc0lE');
@$core.Deprecated('Use memberTransactionRequestDescriptor instead')
const MemberTransactionRequest$json = const {
  '1': 'MemberTransactionRequest',
  '2': const [
    const {'1': 'fromMembersID', '3': 1, '4': 1, '5': 13, '10': 'fromMembersID'},
    const {'1': 'toMembersID', '3': 2, '4': 1, '5': 13, '10': 'toMembersID'},
    const {'1': 'paymentType', '3': 3, '4': 1, '5': 13, '10': 'paymentType'},
    const {'1': 'amount', '3': 4, '4': 1, '5': 13, '10': 'amount'},
  ],
};

/// Descriptor for `MemberTransactionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List memberTransactionRequestDescriptor = $convert.base64Decode('ChhNZW1iZXJUcmFuc2FjdGlvblJlcXVlc3QSJAoNZnJvbU1lbWJlcnNJRBgBIAEoDVINZnJvbU1lbWJlcnNJRBIgCgt0b01lbWJlcnNJRBgCIAEoDVILdG9NZW1iZXJzSUQSIAoLcGF5bWVudFR5cGUYAyABKA1SC3BheW1lbnRUeXBlEhYKBmFtb3VudBgEIAEoDVIGYW1vdW50');
@$core.Deprecated('Use memberDescriptor instead')
const Member$json = const {
  '1': 'Member',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'account', '3': 2, '4': 1, '5': 9, '10': 'account'},
  ],
};

/// Descriptor for `Member`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List memberDescriptor = $convert.base64Decode('CgZNZW1iZXISDgoCaWQYASABKA1SAmlkEhgKB2FjY291bnQYAiABKAlSB2FjY291bnQ=');
@$core.Deprecated('Use memberReplyDescriptor instead')
const MemberReply$json = const {
  '1': 'MemberReply',
  '2': const [
    const {'1': 'members', '3': 1, '4': 3, '5': 11, '6': '.pb.Member', '10': 'members'},
  ],
};

/// Descriptor for `MemberReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List memberReplyDescriptor = $convert.base64Decode('CgtNZW1iZXJSZXBseRIkCgdtZW1iZXJzGAEgAygLMgoucGIuTWVtYmVyUgdtZW1iZXJz');
