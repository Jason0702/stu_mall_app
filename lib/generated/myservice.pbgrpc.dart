///
//  Generated code. Do not modify.
//  source: myservice.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'myservice.pb.dart' as $0;
export 'myservice.pb.dart';

class MyServiceClient extends $grpc.Client {
  static final _$publish = $grpc.ClientMethod<$0.EventValue, $0.EventValue>(
      '/pb.MyService/Publish',
      ($0.EventValue value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.EventValue.fromBuffer(value));
  static final _$subscribe = $grpc.ClientMethod<$0.EventValue, $0.EventValue>(
      '/pb.MyService/Subscribe',
      ($0.EventValue value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.EventValue.fromBuffer(value));
  static final _$memberLogin =
      $grpc.ClientMethod<$0.LoginRequest, $0.GeneralReply>(
          '/pb.MyService/MemberLogin',
          ($0.LoginRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GeneralReply.fromBuffer(value));
  static final _$memberLogout =
      $grpc.ClientMethod<$0.LogoutRequest, $0.GeneralReply>(
          '/pb.MyService/MemberLogout',
          ($0.LogoutRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GeneralReply.fromBuffer(value));
  static final _$getMember =
      $grpc.ClientMethod<$0.MemberRequest, $0.GeneralReply>(
          '/pb.MyService/GetMember',
          ($0.MemberRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GeneralReply.fromBuffer(value));
  static final _$memberTransaction =
      $grpc.ClientMethod<$0.MemberTransactionRequest, $0.GeneralReply>(
          '/pb.MyService/MemberTransaction',
          ($0.MemberTransactionRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GeneralReply.fromBuffer(value));
  static final _$sendEvent =
      $grpc.ClientMethod<$0.SendEventRequest, $0.GeneralReply>(
          '/pb.MyService/SendEvent',
          ($0.SendEventRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GeneralReply.fromBuffer(value));

  MyServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.EventValue> publish($0.EventValue request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$publish, request, options: options);
  }

  $grpc.ResponseStream<$0.EventValue> subscribe($0.EventValue request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$subscribe, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.GeneralReply> memberLogin($0.LoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$memberLogin, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralReply> memberLogout($0.LogoutRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$memberLogout, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralReply> getMember($0.MemberRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getMember, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralReply> memberTransaction(
      $0.MemberTransactionRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$memberTransaction, request, options: options);
  }

  $grpc.ResponseFuture<$0.GeneralReply> sendEvent($0.SendEventRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendEvent, request, options: options);
  }
}

abstract class MyServiceBase extends $grpc.Service {
  $core.String get $name => 'pb.MyService';

  MyServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.EventValue, $0.EventValue>(
        'Publish',
        publish_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.EventValue.fromBuffer(value),
        ($0.EventValue value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.EventValue, $0.EventValue>(
        'Subscribe',
        subscribe_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.EventValue.fromBuffer(value),
        ($0.EventValue value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LoginRequest, $0.GeneralReply>(
        'MemberLogin',
        memberLogin_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LoginRequest.fromBuffer(value),
        ($0.GeneralReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LogoutRequest, $0.GeneralReply>(
        'MemberLogout',
        memberLogout_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LogoutRequest.fromBuffer(value),
        ($0.GeneralReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MemberRequest, $0.GeneralReply>(
        'GetMember',
        getMember_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MemberRequest.fromBuffer(value),
        ($0.GeneralReply value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.MemberTransactionRequest, $0.GeneralReply>(
            'MemberTransaction',
            memberTransaction_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.MemberTransactionRequest.fromBuffer(value),
            ($0.GeneralReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SendEventRequest, $0.GeneralReply>(
        'SendEvent',
        sendEvent_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SendEventRequest.fromBuffer(value),
        ($0.GeneralReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.EventValue> publish_Pre(
      $grpc.ServiceCall call, $async.Future<$0.EventValue> request) async {
    return publish(call, await request);
  }

  $async.Stream<$0.EventValue> subscribe_Pre(
      $grpc.ServiceCall call, $async.Future<$0.EventValue> request) async* {
    yield* subscribe(call, await request);
  }

  $async.Future<$0.GeneralReply> memberLogin_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LoginRequest> request) async {
    return memberLogin(call, await request);
  }

  $async.Future<$0.GeneralReply> memberLogout_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LogoutRequest> request) async {
    return memberLogout(call, await request);
  }

  $async.Future<$0.GeneralReply> getMember_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MemberRequest> request) async {
    return getMember(call, await request);
  }

  $async.Future<$0.GeneralReply> memberTransaction_Pre($grpc.ServiceCall call,
      $async.Future<$0.MemberTransactionRequest> request) async {
    return memberTransaction(call, await request);
  }

  $async.Future<$0.GeneralReply> sendEvent_Pre($grpc.ServiceCall call,
      $async.Future<$0.SendEventRequest> request) async {
    return sendEvent(call, await request);
  }

  $async.Future<$0.EventValue> publish(
      $grpc.ServiceCall call, $0.EventValue request);
  $async.Stream<$0.EventValue> subscribe(
      $grpc.ServiceCall call, $0.EventValue request);
  $async.Future<$0.GeneralReply> memberLogin(
      $grpc.ServiceCall call, $0.LoginRequest request);
  $async.Future<$0.GeneralReply> memberLogout(
      $grpc.ServiceCall call, $0.LogoutRequest request);
  $async.Future<$0.GeneralReply> getMember(
      $grpc.ServiceCall call, $0.MemberRequest request);
  $async.Future<$0.GeneralReply> memberTransaction(
      $grpc.ServiceCall call, $0.MemberTransactionRequest request);
  $async.Future<$0.GeneralReply> sendEvent(
      $grpc.ServiceCall call, $0.SendEventRequest request);
}
