import 'package:grpc/grpc.dart';
import '../../generated/myservice.pbgrpc.dart';
import '../save_model/member.dart' as d;

class GRPCClientSingleton {
  late ClientChannel channel;
  late MyServiceClient stub;

  GRPCClientSingleton(String host, int port) {
    channel = ClientChannel(host,
        port: port,
        options:
            const ChannelOptions(credentials: ChannelCredentials.insecure()));
    stub = MyServiceClient(channel);
  }

  Future<void> shutDown() async {
    await channel.terminate();
  }

  ResponseFuture<GeneralReply> transaction(int fromID, int toID, int amount) {
    MemberTransactionRequest memberTransactionRequest =
        MemberTransactionRequest.create()
          ..paymentType = 6
          ..fromMembersID = fromID
          ..toMembersID = toID
          ..amount = amount;
    return stub.memberTransaction(memberTransactionRequest);
  }

  //用戶登入
  ResponseFuture<GeneralReply> memberLogin(String _account, String _password) {
    LoginRequest loginRequest = LoginRequest.create()
      ..account = _account
      ..password = _password;
    return stub.memberLogin(loginRequest);
  }

  ResponseFuture<GeneralReply> memberLogOut() {
    LogoutRequest logoutRequest = LogoutRequest.create()
      ..membersID = d.LocalMember.memberId;
    return stub.memberLogout(logoutRequest);
  }

  ResponseStream<EventValue> eventMemberTransactionSuccessIn() {
    EventValue eventValue = EventValue.create()
      ..value = '${d.LocalMember.memberId}-memberTransactionSuccessIn:';
    return stub.subscribe(eventValue);
  }

  ResponseStream<EventValue> memberLoginFromAnotherDevice() {
    EventValue eventValue = EventValue.create()
      ..value = '${d.LocalMember.memberId}-memberLoginFromAnotherDevice:';
    return stub.subscribe(eventValue);
  }
}
