import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/product.dart';
import '../models/products_model.dart';
import '../models/products_type_model.dart';
import '../models/turple_model.dart';
import '../save_model/product.dart';
import '../utils/error_message.dart';

class ProductProvider with ChangeNotifier{




  Future<void> getProductTypes() async {
    Tuple<bool, String>  _r = await ProductApi().getProductTypes();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<ProductsTypeModel> _data = _list.map((e) =>  ProductsTypeModel.fromJson(e)).toList();
      LocalProduct().saveProductTypes(_data);
      notifyListeners();
    } catch (e) {
      log("ProductProvider getProductTypes: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }


  Future<void> getProducts() async {
    Tuple<bool, String>  _r = await ProductApi().getProducts();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<ProductsModel> _data = _list.map((e) =>  ProductsModel.fromJson(e)).toList();
      LocalProduct().saveProducts(_data);
      notifyListeners();
    } catch (e) {
      log("ProductProvider getProducts: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }
}