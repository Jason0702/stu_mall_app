import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/record.dart';
import '../enum/app_enum.dart';
import '../enum/order.dart';
import '../models/order_model.dart';
import '../models/ranking_models.dart';
import '../models/record_models.dart';
import '../models/turple_model.dart';
import '../save_model/member.dart';
import '../save_model/record.dart';
import '../utils/error_message.dart';

class RecordProvider with ChangeNotifier {
  Future<void> getPointRankings() async {
    Tuple<bool, String> _r = await RecordApi().getPointRanking();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<RankingModel> _data =
          _list.map((e) => RankingModel.fromJson(e)).toList();
      LocalRecord().saveRanking(_data);
      notifyListeners();
    } catch (e) {
      log("RecordProvider getPointRankings: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  Future<void> getOrderRecords() async {
    Tuple<bool, String> _r =
        await RecordApi().getOrderRecords(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<OrderModel> _data =
          _list.map((e) => OrderModel.fromJson(e)).toList();
      _data.removeWhere(
          (element) => (element.status ?? 0) >= OrderStatusEnum.cancel.index);
      _data.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
      LocalRecord().saveOrders(_data);
      notifyListeners();
    } catch (e) {
      log("RecordProvider getOrderRecords: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  Future<void> getDmRecords() async {
    Tuple<bool, String> _r =
        await RecordApi().getDmRecord(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<RecordModel> _data =
          _list.map((e) => RecordModel.fromJson(e)).toList();
      _data.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
      LocalRecord().saveDmRecord(_data);
      notifyListeners();
    } catch (e) {
      log("RecordProvider getDmRecords: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  Future<void> getPointRecords() async {
    Tuple<bool, String> _r =
        await RecordApi().getPointsRecord(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<RecordModel> _data =
          _list.map((e) => RecordModel.fromJson(e)).toList();
      _data.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
      LocalRecord().savePointRecord(_data);
      notifyListeners();
    } catch (e) {
      log("RecordProvider getPointRecords: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  List<RecordModel> getDmRecordList(int set) {
    if (set == AppEnum.income) {
      return LocalRecord.dmRecordsIncome;
    }
    return LocalRecord.dmRecordsOutcome;
  }

  List<RecordModel> getPointRecordList(int set) {
    if (set == AppEnum.income) {
      return LocalRecord.pointRecordsIncome;
    }
    return LocalRecord.pointRecordsOutcome;
  }

  List<OrderModel> getOrderList(OrderEnum set) {
    switch (set) {
      case OrderEnum.payment:
        return LocalRecord.noPaymentOrders;
      case OrderEnum.shipping:
        return LocalRecord.shippingOrders;
      case OrderEnum.take:
        return LocalRecord.noTakeOrders;
    }
  }
}
