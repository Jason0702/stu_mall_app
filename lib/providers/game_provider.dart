import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/enum/app_enum.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/game.dart';
import '../enum/game.dart';
import '../models/game_question_model.dart';
import '../models/game_type_models.dart';
import '../models/turple_model.dart';
import '../save_model/game.dart';
import '../utils/error_message.dart';
import '../utils/util.dart';

class GameProvider with ChangeNotifier {
  bool jiugonggeTodayIsPlay = true;
  bool scratcherTodayIsPlay = true;
  bool quizIsPlay = true;

  //取得遊戲類型
  Future<void> getGameTypes() async {
    Tuple<bool, String> _r = await GameApi().getGameTypeData();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<GameTypeModel> _data =
          _list.map((e) => GameTypeModel.fromJson(e)).toList();
      LocalGame().saveGames(_data);
      getTodayIsPlay();
      notifyListeners();
    } catch (e) {
      log("GameProvider getGameTypes: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //取得今天是否遊玩（all）
  Future<void> getTodayIsPlay() async {
    DateTime now = new DateTime.now();
    String timestamp = Util.dateFormatISO.format(now);
    init();
    if (LocalGame.bingo.id != null &&
        LocalMember.memberId != AppEnum.noMember) {
      _jiugonggeTodayIsPlay(timestamp);
    }
    if (LocalGame.scratcher.id != null &&
        LocalMember.memberId != AppEnum.noMember) {
      _scratcherTodayIsPlay(timestamp);
    }
    if (LocalGame.quizQuestion?.id != null &&
        LocalMember.memberId != AppEnum.noMember) {
      getQuizQuestion();
    }
  }

  //九宮格是否有玩
  Future<void> _jiugonggeTodayIsPlay(String timestamp) async {
    Tuple<bool, String> _r = await GameApi().getGameTodayIsPlay(
        LocalGame.bingo.id!, LocalMember.memberId, timestamp);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      if (_list.isEmpty) {
        jiugonggeTodayIsPlay = false;
        notifyListeners();
      }
    } catch (e) {
      log("GameProvider getTodayIsPlay: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
    }
  }

  //刮刮樂可以玩嗎
  Future<void> _scratcherTodayIsPlay(String timestamp) async {
    Tuple<bool, String> _r = await GameApi().getGameTodayIsPlay(
        LocalGame.scratcher.id!, LocalMember.memberId, timestamp);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      if (_list.isEmpty) {
        scratcherTodayIsPlay = false;
        notifyListeners();
      }
    } catch (e) {
      log("GameProvider getTodayIsPlay: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
    }
  }

  //開放時間
  bool todayCanPlay(GameTypeModel data) {
    if (data.status == AppEnum.inactive) {
      return false;
    }
    DateTime? open = DateTime.tryParse(data.timeToOpen ?? "");
    DateTime? close = DateTime.tryParse(data.timeToClose ?? "");
    DateTime now = DateTime.now();
    if (open == null || close == null) {
      return false;
    }
    if (now.isBefore(open)) {
      return false;
    }
    if (now.isAfter(close)) {
      return false;
    }
    return true;
  }

  //玩遊戲
  Future<int?> playGame(int? gameId) async {
    if (gameId == null) {
      return null;
    }
    Tuple<bool, String> _r =
        await GameApi().playGame(gameId, LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast('${_r.i2}');
      return null;
    }
    if (gameId == LocalGame.bingo.id) {
      jiugonggeTodayIsPlay = true;
      notifyListeners();
    }
   /* if (gameId == LocalGame.scratcher.id) {
      scratcherTodayIsPlay = true;
    }*/
    int? point = int.tryParse(_r.i2!);
    return point;
  }

  //初始化
  init() {
    jiugonggeTodayIsPlay = true;
    scratcherTodayIsPlay = true;
    quizIsPlay = true;
  }

  //取遊戲名稱
  String getName(GameEnum set) {
    switch (set) {
      case GameEnum.bingo:
        return LocalGame.bingo.name ?? '';
      case GameEnum.quiz:
        return LocalGame.quiz.name ?? '';
      case GameEnum.scratcher:
        return LocalGame.scratcher.name ?? '';
    }
  }

  //取得問答題目
  Future<void> getQuizQuestion() async {
    Tuple<bool, String> _r = await GameApi().getActivity();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<GameModel> _data = _list.map((e) => GameModel.fromJson(e)).toList();
      LocalGame().saveQuizQuestion(_data.first);
      getQuizAnswerLog(_data.first.id ?? 0);
      notifyListeners();
    } catch (e) {
      log("GameProvider getQuizQuestion: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //取得回答題目紀錄
  Future<void> getQuizAnswerLog(int quizId) async {
    if (quizId == 0) {
      return;
    }
    Tuple<bool, String> _r =
        await GameApi().getActivityLog(quizId, LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      if (!_list.any((element) => element['question_id'] == LocalGame.quizQuestion?.questions?.first.id)) {
        quizIsPlay = false;
      }
      notifyListeners();
    } catch (e) {
      log("GameProvider getQuizQuestion: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //回答題目
  Future<void> answerQuiz(
      int questionId, String answer, int memberAnswer) async {
    EasyLoading.show();
    Map<String, dynamic> map = {
      'activity_id': LocalGame.quizQuestion?.id,
      'members_id': LocalMember.memberId,
      'question_id': questionId,
      'answer': answer,
      'member_answer': '[$memberAnswer]',
    };
    log("$map");

    Tuple<bool, String> _r = await GameApi().postAnswerLog(map);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    EasyLoading.dismiss();
    quizIsPlay = true;
    notifyListeners();
  }
}
