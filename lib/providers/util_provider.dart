import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/system-setting.dart';
import '../models/systen_setting_model.dart';
import '../api/api.dart';
import '../models/turple_model.dart';
import '../save_model/system_setting.dart';
import '../utils/error_message.dart';

class UtilProvider with ChangeNotifier {
  
  static const _addressFile = 'assets/data/address.json';
  Map<String, dynamic> jsonList = {};
  List city = [];
  Map<String, dynamic> weather = {};


  ///取得系統設定
  Future<void> getSystemSetting() async {
    Tuple<bool, String> _r = await SystemSettingApi().systemSettingByGuest();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try{
      SystemSettingModel systemSetting = SystemSettingModel.fromJson(json.decode(_r.i2!));
      log('解析：${systemSetting.toJson()}');
      LocalSetting().save(systemSetting);
      notifyListeners();
    } catch (e){
      log("SystemSettingModel getMember: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    notifyListeners();
  }

  ///解析json檔
  void decodeFile() {
    rootBundle.loadString(_addressFile).then((value) {
      jsonList = json.decode(value);
      city = jsonList['city'].map((element) => element['name']).toList();
    });
  }

  ///拿縣市取區域
  List<String> getArea(String _city) {
    List<String> area = [];
    area.clear();
    jsonList['city'].forEach((element) {
      if (element['name'] == _city) {
        List jsonList = element['dist'];
        for (var dist in jsonList) {
          area.add(dist['name']);
        }
      }
    });
    return area;
  }

  //取得台灣各區溫度
  Future<void> getWeather() async {
    try {
      var response = await Dio().get(Api.getWeatherApi);
      weather = jsonDecode(response.data);
    } catch (e) {
      log('取得台灣各區溫度 $e');
    }
  }

  //取得台灣各區域溫度
  String getWeatherArea(String city, String locality) {
    String temp = '';
    if(weather.isEmpty || city.isEmpty || locality.isEmpty){
      return '';
    }
    List tmp = weather['cwbopendata']['location'];
    tmp.forEach((element) {
      //log('${element['parameter'][0]['parameterValue']} ${element['parameter'][2]['parameterValue']} ${ element['weatherElement'][3]['elementValue']['value']}');
      if (element['parameter'][0]['parameterValue'] == city &&
          element['parameter'][2]['parameterValue'] == locality) {
        temp = element['weatherElement'][3]['elementValue']['value'].toString();
      }
    });

    return temp;
  }




}
