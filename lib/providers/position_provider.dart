import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PositionProvider with ChangeNotifier {
  //定位設定
  late LocationSettings locationSettings;

  //位置串流
  StreamSubscription<Position>? positionStream;

  //現在位置
  LatLng? _currentPosition;

  LatLng get currentPosition =>
      _currentPosition ?? const LatLng(22.658819256709965, 120.33318716157004);

  //初始化位置
  LatLng? _initialPosition;

  LatLng get initialPosition => _initialPosition!;

  //當前地址
  String? _address;


  Placemark? _placemark;

  String get city => (_placemark?.administrativeArea?.toString() ?? '').isNotEmpty
      ? _placemark?.administrativeArea.toString() ?? ''
      : _placemark?.subAdministrativeArea.toString() ?? '';

  String get locality => _placemark?.locality.toString() ?? '';

  String get address => _address ?? '';

  bool checkInitialPosition() {
    if (_initialPosition == null) {
      return false;
    }
    return true;
  }

  Future<bool> checkPosition() async {
    bool _result = false;
    await _determinePosition().then((value) {
      debugPrint('自身座標取得');
      _initialPosition = LatLng(value.latitude, value.longitude);
      _currentPosition = _initialPosition;
      _gpsSetting();
      _result = true;
    }, onError: (value) async {
      if (value != null) {
        if (value == 'GPS服務沒有權限') {
          LocationPermission permission = await Geolocator.requestPermission();
          debugPrint('GPS: $permission ');
          _result = false;
        }
        if (value == 'GPS權限缺少 (value: LocationPermission.denied).') {
          LocationPermission permission = await Geolocator.requestPermission();
          debugPrint('GPS: $permission ');
          _result = false;
        }
        if (value == 'GPS服務未開啟') {
          Geolocator.openLocationSettings();
          _result = true;
        }
      }
    });
    return _result;
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('GPS服務未開啟');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error('GPS服務沒有權限');
    }
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error('GPS權限缺少 (value: $permission).');
      }
    }
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: Platform.isIOS
            ? LocationAccuracy.bestForNavigation
            : LocationAccuracy.best);
  }

  void _gpsSetting() {
    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = const LocationSettings(
          accuracy: LocationAccuracy.best, distanceFilter: 5);
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      locationSettings = AppleSettings(
          accuracy: LocationAccuracy.bestForNavigation,
          distanceFilter: 500,
          pauseLocationUpdatesAutomatically: false,
          showBackgroundLocationIndicator: true);
    }
    _getPositionStream();
  }

  void _getPositionStream() {
    positionStream ??=
        Geolocator.getPositionStream(locationSettings: locationSettings)
            .listen((Position? position) {
      if (position != null) {
        _currentPosition = LatLng(position.latitude, position.longitude);
        _getAddress();
      }
    });
  }

  void cancelStream() {
    positionStream!.cancel();
  }

  void _getAddress() async {
    List<Placemark> _placemarks = [];
    try{
      _placemarks = await placemarkFromCoordinates(
          _currentPosition!.latitude, _currentPosition!.longitude);
    } catch (e) {
      log('${e.toString()}');
    }

    if (_placemarks.isNotEmpty) {
      _placemark = _placemarks.first;
      _address = "${_placemark?.administrativeArea}${_placemark?.locality}${_placemark?.thoroughfare}${_placemark?.subThoroughfare}";
      notifyListeners();
    }
  }
}
