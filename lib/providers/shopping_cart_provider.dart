import 'dart:convert';
import 'dart:developer';
import 'package:com.sj.stu_mall_app/save_model/product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/order.dart';
import '../api/shopping_cart.dart';
import '../enum/app_enum.dart';
import '../enum/order.dart';
import '../enum/shopping_cart.dart';
import '../models/products_model.dart';
import '../models/shop_carts_models.dart';
import '../models/turple_model.dart';
import '../save_model/member.dart';
import '../save_model/shopping_cart.dart';
import '../save_model/system_setting.dart';
import '../utils/error_message.dart';

class ShoppingCartProvider with ChangeNotifier {
  List<ShoppingCartsModel> get memberCart =>
      isImmediately ? _immediatelyCarts : LocalShoppingCart.memberCart;
  ShoppingCartApi _cartApi = ShoppingCartApi();
  ShoppingCartStepEnum step = ShoppingCartStepEnum.selectQty;

  int get totalMoney => memberCart.fold(
      0, (prev, element) => prev += (element.price! * element.qty!));

  List<ShoppingCartsModel> _immediatelyCarts = [];

  bool isImmediately = false;

  ///新增商品
  Future<void> addItem(ShoppingCartsModel data) async {
    if (EasyLoading.isShow) {
      return;
    }
    if (memberCart.any((element) => element.productsId == data.productsId)) {
      log("更改數量");
      ShoppingCartsModel tmp = memberCart
          .firstWhere((element) => element.productsId == data.productsId);
      data.id = tmp.id;
      //log("更改數量${tmp.id}  ${data.id}");
      //更改數量
      data.qty = tmp.qty! + data.qty!;
      _cartApi.updatedShoppingCart(data);
      tmp.qty = data.qty;
    } else {
      log("新增商品");
      //新增商品
      EasyLoading.show(status: "加入購物車中");
      Tuple<bool, String> _r =
          await _cartApi.addShoppingCart(LocalMember.memberId, data);
      if (!_r.i1!) {
        EasyLoading.showToast(EasyLoadingMessage.networkError);
        return;
      }
      await getShoppingCarts();
      EasyLoading.showToast("已加入購物車");
    }
    notifyListeners();
  }

  ///減少商品
  Future<void> subItem(ShoppingCartsModel data) async {
    if (EasyLoading.isShow) {
      return;
    }
    if (memberCart.any((element) => element.productsId == data.productsId)) {
      ShoppingCartsModel tmp = memberCart
          .firstWhere((element) => element.productsId == data.productsId);
      if (tmp.qty == 1) {
        EasyLoading.show(status: "移出購物車中");
        Tuple<bool, String> _r = await _cartApi.deleteShoppingCart(tmp.id!);
        if (!_r.i1!) {
          EasyLoading.showToast(EasyLoadingMessage.networkError);
          return;
        }
        memberCart.removeWhere((element) => element.id == tmp.id);
        EasyLoading.showToast("已移出購物車");
      } else {
        data.id = tmp.id;
        data.qty = tmp.qty! - data.qty!;
        _cartApi.updatedShoppingCart(data);
        tmp.qty = data.qty;
      }
    }
    notifyListeners();
  }

  ///刪除商品
  Future<void> deleteItem(ShoppingCartsModel data) async {
    if (EasyLoading.isShow) {
      return;
    }
    log('選擇不要 ${memberCart.any((element) => element.productsId == data.productsId)}');
    if (memberCart.any((element) => element.productsId == data.productsId)) {
      ShoppingCartsModel tmp = memberCart
          .firstWhere((element) => element.productsId == data.productsId);
      EasyLoading.show(status: "移出購物車中");
      Tuple<bool, String> _r = await _cartApi.deleteShoppingCart(tmp.id!);
      if (!_r.i1!) {
        EasyLoading.showToast(EasyLoadingMessage.networkError);
        return;
      }
      memberCart.removeWhere((element) => element.id == tmp.id);
      EasyLoading.showToast("已移出購物車");
    }
    notifyListeners();
  }

  Future<void> getShoppingCarts() async {
    Tuple<bool, String> _r =
        await _cartApi.getShoppingCart(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<ShoppingCartsModel> _data =
          _list.map((e) => ShoppingCartsModel.fromJson(e)).toList();
      LocalShoppingCart.save(_data);
      notifyListeners();
    } catch (e) {
      log("ShoppingCartProvider getShoppingCarts: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  void changeShoppingCartStepEnum(ShoppingCartStepEnum data) {
    step = data;
    notifyListeners();
  }

  bool getOpenStatus(OrderPaymentEnum set) {
    switch (set) {
      case OrderPaymentEnum.none:
        return false;
      case OrderPaymentEnum.cash:
        return false;
      case OrderPaymentEnum.credit:
        return LocalSetting.iPassPaymentStatus;
      case OrderPaymentEnum.linePay:
        return LocalSetting.linePayPaymentStatus;
      case OrderPaymentEnum.atm:
        return false;
      case OrderPaymentEnum.vAtm:
        return false;
      case OrderPaymentEnum.dmCoin:
        return LocalSetting.dmPaymentStatus;
    }
  }

  bool getDeliveryOpenStatus(DeliveryTypeEnum set) {
    switch (set) {
      case DeliveryTypeEnum.normal:
        return LocalSetting.toHome.status == AppEnum.active;
      case DeliveryTypeEnum.store:
        return LocalSetting.convenienceStore.status == AppEnum.active;
      case DeliveryTypeEnum.self:
        return LocalSetting.selfGet.status == AppEnum.active;
      case DeliveryTypeEnum.cabinet:
        return LocalSetting.cabinetGet.status == AppEnum.active;
    }
  }

  int getDeliverFee(DeliveryTypeEnum set) {
    switch (set) {
      case DeliveryTypeEnum.normal:
        return LocalSetting.toHome.fee ?? 120;
      case DeliveryTypeEnum.store:
        return LocalSetting.convenienceStore.fee ?? 0;
      case DeliveryTypeEnum.self:
        return LocalSetting.selfGet.fee ?? 0;
      case DeliveryTypeEnum.cabinet:
        return LocalSetting.cabinetGet.fee ?? 0;
    }
  }

  List<ProductsModel> checkProductIsEnough() {
    List<ProductsModel> _products = LocalProduct.products;
    List<ProductsModel> notEnoughItem = [];

    for (var cartItems in memberCart) {
      if (!_products.any((element) => element.id == cartItems.productsId)) {
        continue;
      }
      ProductsModel product =
          _products.firstWhere((element) => element.id == cartItems.productsId);
      if (cartItems.qty! > product.qty!) {
        notEnoughItem.add(product);
      }
    }

    return notEnoughItem;
  }

  void changeIsImmediately(bool set) {
    isImmediately = set;
    notifyListeners();
  }

  void addImmediatelyCart(ProductsModel set) {
    _immediatelyCarts.clear();
    ShoppingCartsModel single = ShoppingCartsModel(
        productsId: set.id, qty: 1, name: set.name, price: set.price);
    _immediatelyCarts.add(single);
    notifyListeners();
  }

  Future<Tuple<bool, String>> postOrder(int totalMoney, OrderPaymentEnum paymentType,
      DeliveryTypeEnum deliveryOption) async {
    Map<String, dynamic> map = {
      'payment_type': paymentType.index,
      'goods_amount': totalMoney,
      'pay_amount': totalMoney,
      "product_info": json.encode(memberCart.map((e) => e.toProductInfo()).toList()),
      'delivery_option': deliveryOption.index,
      'users_id': 1,
      "use_dm_coin": paymentType == OrderPaymentEnum.dmCoin ? totalMoney : 0,
      'members_id': LocalMember.memberId,
      'members_name': LocalMember.memberModel!.name,
      'members_cellphone': LocalMember.memberModel!.cellphone,
      'members_gender': 1,
      'members_address': '824高雄市燕巢區橫山路59號',
      'recv_name': LocalMember.memberModel!.name,
      'recv_cellphone': LocalMember.memberModel!.cellphone,
      'recv_gender': 1,
      'recv_address': '824高雄市燕巢區橫山路59號',
      'members_discount': 1
    };
    Tuple<bool, String> _r = await OrderApi().postOrder(map);
    if(!_r.i1!){
      return  Tuple<bool, String>(false,'');
    }
    //如果不是立即購買
    if(!isImmediately){
      //清空線上購物車
      ShoppingCartApi().deleteAllShoppingCart(LocalMember.memberId);
      LocalShoppingCart.save([]);
    }
    _immediatelyCarts.clear();
    isImmediately = false;
    return  Tuple<bool, String>(true,_r.i2);
  }
}
