import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/api/auth.dart';
import 'package:com.sj.stu_mall_app/app.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:com.sj.stu_mall_app/save_model/shopping_cart.dart';
import 'package:com.sj.stu_mall_app/utils/error_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../api/member.dart';
import '../grpc/grpc.dart';
import '../models/member_model.dart';
import '../models/token_model.dart';
import '../models/turple_model.dart';
import '../api/api.dart';
import '../save_model/member.dart';
import '../utils/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();

  Future<void> memberLogin(String name, String password) async {
    EasyLoading.show(status: EasyLoadingMessage.waiting);
    //驗證
    Tuple<bool, String> _r = await AuthApi().memberLogin(name, password);
    if (!_r.i1!) {
      EasyLoading.showToast("${_r.i2}");
      return;
    }
    TokenModel _data;
    try {
      _data = TokenModel.fromJson(jsonDecode(_r.i2!));
      Api.accessToken = _data.token;
      Api.forceUpdatePassword = _data.forceUpdatePassword ?? false;
      SharedPreferenceUtil().saveUser(name, password);
    } catch (e) {
      log("AuthProvider memberLogin: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    //取會員
    _r = await MemberApi().getMember(_data.id!);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    MemberModel _member;
    //save
    try {
      _member = MemberModel.fromJson(jsonDecode(_r.i2!));
      LocalMember().saveMember(_member);
      Tuple<bool, String> r = await MemberApi().updateToken(_member.id!);
      if(r.i1!){
        log('Firebase: ${r.i2!}');
      }else{
        log('Firebase: ${r.i2}');
      }
      notifyListeners();
    } catch (e) {
      log("AuthProvider getMember: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    EasyLoading.showToast(EasyLoadingMessage.loginSuccess);
    GrpcUtil.connectGrpc(name, password);
    delegate.replace(name: RouteName.home);
  }

  Future<void> autoLogin() async {
    Map<String, dynamic> _map =
    await _sharedPreferenceUtil.getAccountAndPassword();
    String _name = _map['name'];
    String _password = _map['password'];
    if (_name.isEmpty || _password.isEmpty) {
      delegate.replace(name: RouteName.home);
      return;
    }
    memberLogin(_name,_password);
  }

  Future<void> logout() async {
    GrpcUtil.logOut();
    _sharedPreferenceUtil.delUser();
    LocalMember().saveMember(null);
    LocalMember().saveCoin(null);
    LocalShoppingCart.save([]);
    delegate.replace(name: RouteName.welcome);
  }
}
