import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:package_info_plus/package_info_plus.dart';

class PackagesInfoProvider with ChangeNotifier {
  //APP名稱
  String _appName = '';
  String get appName => _appName;
  //包名
  String _packageName = '';
  String get packageName => _packageName;
  //版本號
  String _version = '';
  String get version => _version;
  //BuildNumber
  String _buildNumber = '';
  String get buildNumber => _buildNumber;

  void getPackageInfo() async {
    PackageInfo _packageInfo = await PackageInfo.fromPlatform();
    log("取得包資料${_packageInfo.toString()}");
    _appName = _packageInfo.appName;
    _packageName = _packageInfo.packageName;
    _version = _packageInfo.version;
    _buildNumber = _packageInfo.buildNumber;
  }
}
