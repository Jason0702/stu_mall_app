
import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/save_model/news.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/new.dart';
import '../enum/app_enum.dart';
import '../models/carousels_model.dart';
import '../models/news_model.dart';
import '../models/turple_model.dart';
import '../utils/error_message.dart';

class NewsProvider with ChangeNotifier{

  Future<void> getNews() async {
    Tuple<bool, String>  _r = await NewApi().getNews();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<NewsModel> _data = _list.map((e) =>  NewsModel.fromJson(e)).toList();
      //保留 受眾是會員的
      _data.retainWhere((element) => element.types == 2);
      _data.retainWhere((element) => element.status == AppEnum.active);
      LocalNew.saveNews(_data);
      notifyListeners();
    } catch (e) {
      log("NewsProvider getNews: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  Future<void> getCarousels() async {
    Tuple<bool, String>  _r = await NewApi().getCarousels();
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      List<dynamic> _list = jsonDecode(_r.i2!);
      List<CarouselsModel> _data = _list.map((e) =>  CarouselsModel.fromJson(e)).toList();
      //保留 受眾是會員的
      _data.retainWhere((element) => element.audience == 1);
      _data.retainWhere((element) => element.status == AppEnum.active);
      LocalNew.saveCarouselsModel(_data);
      notifyListeners();
    } catch (e) {
      log("NewsProvider getCarousels: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

}