import 'dart:convert';
import 'dart:developer';

import 'package:com.sj.stu_mall_app/grpc/grpc.dart';
import 'package:com.sj.stu_mall_app/save_model/system_setting.dart';
import 'package:com.sj.stu_mall_app/utils/shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../api/member.dart';
import '../app.dart';
import '../enum/app_enum.dart';
import '../models/turple_model.dart';
import '../models/user_coin_models.dart';
import '../save_model/member.dart';
import '../utils/error_message.dart';
import '../utils/util.dart';

class MemberProvider with ChangeNotifier {
  //獲取點數
  Future<void> getMemberPoints() async {
    Tuple<bool, String> _r =
        await MemberApi().getMemberPoint(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      CoinModels coinModels = CoinModels.fromJson(jsonDecode(_r.i2!));
      LocalMember().saveCoin(coinModels);
      notifyListeners();
    } catch (e) {
      log("MemberProvider getMemberPoints: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //會員簽到
  Future<void> memberCheckIn() async {
    //一天簽到一次
    if (Util.dateFormatISO.format(DateTime.now()) ==
        LocalMember.coinModels?.checkInAt) {
      EasyLoading.showToast(EasyLoadingMessage.isCheck);
      return;
    }
    if (EasyLoading.isShow) {
      return;
    }
    EasyLoading.show();
    //簽到
    Tuple<bool, String> _r =
        await MemberApi().getMembersCheckIn(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    EasyLoading.showToast(EasyLoadingMessage.checkSuccess);
    LocalMember.coinModels?.sticker = LocalMember.coinModels!.sticker! + 1;
    notifyListeners();
    //累計10天
    if (LocalMember.sticker >= AppEnum.totalSticker) {
      await _exchangeStickerToPoints();
    }
    getMemberPoints();
  }

  //會員貼紙轉點數
  Future<void> _exchangeStickerToPoints() async {
    //轉換點數
    Tuple<bool, String> _r =
        await MemberApi().membersStickerConvertToPoint(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    EasyLoading.showSuccess(
        "${AppEnum.totalSticker}天簽到完成，恭喜獲得${AppEnum.stickerExChangePoints}點");
    //畫面扣除點數
    LocalMember.coinModels?.sticker =
        LocalMember.coinModels!.sticker! - AppEnum.totalSticker;
    notifyListeners();
  }

  //會員點數換DM幣
  Future<void> exchangePointsToDmCoin(String qty) async {
    //輸入為空
    if (qty.isEmpty) {
      EasyLoading.showToast(EasyLoadingMessage.setInput);
      return;
    }
    //轉int
    int? points = int.tryParse(qty);
    if (points == null) {
      EasyLoading.showToast(EasyLoadingMessage.intOnly);
      return;
    }
    if (points == 0) {
      EasyLoading.showToast(EasyLoadingMessage.pointsCantBeZero);
      return;
    }
    if (points < LocalSetting.exchangeRatePointToDm) {
      EasyLoading.showError('請輸入超過${LocalSetting.exchangeRatePointToDm}點');
      return;
    }
    //點數小於最小值
    if (LocalMember.points < points) {
      EasyLoading.showError('您的點數小於您輸入的點數');
      return;
    }
    if (EasyLoading.isShow) {
      return;
    }
    EasyLoading.show();
    //轉換點數
    Tuple<bool, String> _r = await MemberApi()
        .getMembersPointConvertToDmCoin(LocalMember.memberId, points);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    EasyLoading.showSuccess(EasyLoadingMessage.exchangeSuccess);
    getMemberPoints();
  }

  //獲取qrcode
  Future<void> getMemberQrcode() async {
    Tuple<bool, String> _r =
        await MemberApi().getMemberQrcode(LocalMember.memberId);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      LocalMember().saveQrcode(_r.i2 ?? '');
      notifyListeners();
    } catch (e) {
      log("MemberProvider getMemberQrcode: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //會員更換密碼
  Future<void> changeMemberPassword(String newPassword, String password) async {
    Tuple<bool, String> _r = await MemberApi()
        .changeMemberPassword(LocalMember.memberId, newPassword, password);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      SharedPreferenceUtil().savePassword(newPassword);
      EasyLoading.showToast(EasyLoadingMessage.settingSuccess);
      notifyListeners();
      delegate.popRoute();
    } catch (e) {
      log("MemberProvider getMemberPoints: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  //會員資料更改
  Future<void> changeMemberData(
      String name, String cellphone, String email) async {
    Map<String, dynamic> map = {
      'id': LocalMember.memberId,
      'name': name,
      'cellphone': cellphone,
      'email': email,
    };
    Tuple<bool, String> _r =
        await MemberApi().changeMemberData(LocalMember.memberId, map);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
    try {
      LocalMember.memberModel?.name = name;
      LocalMember.memberModel?.cellphone = cellphone;
      LocalMember.memberModel?.email = email;
      EasyLoading.showToast(EasyLoadingMessage.settingSuccess);
      notifyListeners();
    } catch (e) {
      log("MemberProvider getMemberPoints: $e}");
      EasyLoading.showToast(EasyLoadingMessage.networkError);
      return;
    }
  }

  Future<void> postTransferData(String account, int money) async {
    if (EasyLoading.isShow) {
      return;
    }
    EasyLoading.show();
    Tuple<bool, String> _r = await MemberApi().checkAccountExits(account);
    if (!_r.i1!) {
      EasyLoading.showToast(EasyLoadingMessage.noMember);
      return;
    }
    int? otherId = int.tryParse(_r.i2!);
    if (otherId == null) {
      return;
    }
    GrpcUtil.transaction(LocalMember.memberId, otherId, money);
  }
}
