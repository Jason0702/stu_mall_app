
import 'package:flutter/material.dart';

import '../utils/app_size.dart';
import '../widgets/app_widget.dart';

/*
* 確認取消視窗
*
* */
class ConfirmDialog extends StatefulWidget {
  const ConfirmDialog({Key? key, required this.text}) : super(key: key);
  final String text;
  @override
  _ConfirmDialogState createState() => _ConfirmDialogState();
}

class _ConfirmDialogState extends State<ConfirmDialog> {
  String get text => widget.text;
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: SafeArea(
          child: Stack(
            children: [
              Center(
                child: Container(
                  width: 330,
                  height: 250,
                  padding:const  EdgeInsets.symmetric(
                      horizontal: AppSize.defaultPadding,vertical: 20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      const Spacer(),
                      Text(
                        text,
                        style: const TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: AppSize.dialogMainTextSize),
                      ),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                        Expanded(child: _cancelWidget()),
                          SizedBox(width: 20,),
                          Expanded(child: _confirmWidget())
                      ],)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  ///確認按鈕
  Widget _confirmWidget() {
    return GestureDetector(
      onTap: () => _confirmOnTap(),
      child: SizedBox(
        width: AppSize.dialogButtonSize,
        child: buttonWidget("確認"),
      ),
    );
  }

  void _confirmOnTap(){
    Navigator.of(context).pop(true);
  }

  ///取消按鈕
  Widget _cancelWidget() {
    return GestureDetector(
      onTap: () => _cancelOnTap(),
      child: SizedBox(
        width: AppSize.dialogButtonSize,
        child: buttonWidgetDisable("取消"),
      ),
    );
  }
  void _cancelOnTap(){
    Navigator.of(context).pop(false);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}
