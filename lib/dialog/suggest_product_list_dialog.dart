

import 'package:flutter/material.dart';

import '../models/products_model.dart';
import '../utils/app_size.dart';
import '../widgets/app_widget.dart';

class SuggestDialog extends StatefulWidget {
  const SuggestDialog({Key? key, required this.data}) : super(key: key);
  final List<ProductsModel> data;

  @override
  State<SuggestDialog> createState() => _SuggestDialogState();
}

class _SuggestDialogState extends State<SuggestDialog> {
  List<ProductsModel> get _list => widget.data;


  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: SafeArea(
          child: Stack(
            children: [
              Center(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppSize.defaultPadding),
                  width: 350,
                  height: 400,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const Text(
                        "目前庫存不足，建議修改數量為：",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      _itemTitle(),
                      ..._list.map((e) => _itemInfo(e)),
                      const Spacer(
                        flex: 3,
                      ),
                      _cancelOrderWidget(),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Widget _itemTitle() {
    return Row(
      children: const [
        Expanded(
            flex: 2,
            child: Center(
              child: Text(
                "名稱",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            )),
        Expanded(
            flex: 1,
            child: Center(
              child: Text(
                "數量",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            )),
      ],
    );
  }

  Widget _itemInfo(ProductsModel data) {
    return Row(
      children: [
        Expanded(
            flex: 2,
            child: Center(
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  "${data.name}",
                  style: const TextStyle(fontSize: 17, color: Colors.black),
                ),
              ),
            )),
        Expanded(
            flex: 1,
            child: Center(
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  "${data.qty}",
                  style: const TextStyle(fontSize: 17, color: Colors.black),
                ),
              ),
            )),
      ],
    );
  }

  ///取消按鈕
  Widget _cancelOrderWidget() {
    return GestureDetector(
      onTap: () => _cancelOrderOnTap(),
      child: SizedBox(
        width: 280,
        child: buttonWidget("關閉視窗"),
      ),
    );
  }

  ///取消事件
  void _cancelOrderOnTap() {

    Navigator.pop(context);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }
}
