import 'package:flutter/material.dart';
import '../widgets/app_widget.dart';

class SelectDeliverInfoDialog extends StatefulWidget {
  const SelectDeliverInfoDialog({Key? key}) : super(key: key);
  @override
  State<SelectDeliverInfoDialog> createState() => _SelectDeliverInfoDialogState();
}

class _SelectDeliverInfoDialogState extends State<SelectDeliverInfoDialog> {

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: SafeArea(
          bottom: false,
          child: Column(
            children: [
              titleBarIntegrate('宅配資訊', [_backButton()]),
            ],
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: backButtonWidget(),
    );
  }


  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}
