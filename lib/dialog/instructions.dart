import 'package:com.sj.stu_mall_app/widgets/app_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import '../utils/no_shadow_scroll_behavior.dart';

class InstructionsDialog extends StatefulWidget {
  const InstructionsDialog({Key? key, required this.data, required this.title})
      : super(key: key);
  final String data;
  final String title;

  @override
  _InstructionsDialogState createState() => _InstructionsDialogState();
}

class _InstructionsDialogState extends State<InstructionsDialog> {
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
      onBackButtonPressed: () => pop(),
      child: Scaffold(
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              titleBarIntegrate('${widget.title}', [_backButton()]),
              htmlBody(),
            ],
          ),
        ),
      ),
    );
  }

  Widget htmlBody() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Html(
          data: widget.data,
          style: {
            "p": Style(
                //margin: const EdgeInsets.only(left: 5, right: 5),
                fontSize: FontSize.larger,
                color: Colors.black)
          },
        ),
      ),
    );
  }

  Widget _backButton() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, false);
      },
      child: backButtonWidget(),
    );
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}
