import 'package:flutter/material.dart';


import '../app.dart';
import '../routers/route_name.dart';
import '../utils/app_size.dart';
import '../widgets/app_widget.dart';


class NotLoggingDialog extends StatefulWidget {
  const NotLoggingDialog({Key? key}) : super(key: key);

  @override
  State<NotLoggingDialog> createState() => _NotLoggingDialogState();
}

class _NotLoggingDialogState extends State<NotLoggingDialog> {
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
      onBackButtonPressed: () => pop(),
      child: Center(
        child: Container(
          width: 350,
          height: 300,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Column(
            children: [
              const Spacer(),
              const Text(
                "請先登入帳號！",
                style: TextStyle(
                    fontSize: AppSize.dialogMainTextSize,
                    fontWeight: FontWeight.bold),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => confirmOnTap(),
                    child: buttonWidget("確認"),
                  ),
                ],
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  void confirmOnTap() {
    delegate.replace(name: RouteName.welcome);
  }

  Future<bool> pop() async {
    return true;
  }
}
