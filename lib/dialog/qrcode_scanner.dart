import 'package:com.sj.stu_mall_app/utils/app_size.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';


import '../utils/app_image.dart';

class QrcodeScanner extends StatefulWidget {
  const QrcodeScanner({Key? key}) : super(key: key);

  @override
  _QrcodeScannerState createState() => _QrcodeScannerState();
}

class _QrcodeScannerState extends State<QrcodeScanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late Barcode result;

  ///.code.toString
  QRViewController? controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: AppSize.appWidth,
            height: AppSize.appHeight,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context,'');
                },
                child: Container(
                    margin: EdgeInsets.only(top: 30, right: 10),
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                      size: 60,
                    )),
              )
            ],
          ),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 50, right: 50, bottom: 100),
              child: Image.asset(AppImage.imgScanQr)),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 300),
              child: Text(
                '請掃描QR CODE',
                textScaleFactor: 1,
                style: TextStyle(color: Colors.white, fontSize: 25),
              )),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      controller.stopCamera();
      result = scanData;
      final String qrCode = result.code ?? '';
      Navigator.pop(context, qrCode);
      controller.resumeCamera();
      setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller?.dispose();
  }
}
