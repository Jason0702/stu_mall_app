import 'package:com.sj.stu_mall_app/providers/member_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../save_model/member.dart';
import '../utils/app_image.dart';

class PointsCard extends StatelessWidget {
  const PointsCard({Key? key,this.mainAxis}) : super(key: key);
 final MainAxisAlignment? mainAxis;


  @override
  Widget build(BuildContext context) {
    return Consumer<MemberProvider>(builder: (context, member, _) {
      return Row(
        mainAxisAlignment: mainAxis ?? MainAxisAlignment.center,
        children: [
          Image.asset(
            AppImage.iconDmDollar,
            width: 40,
          ),
          Container(
            child:LocalMember.memberId != 0 ? RichText(
              textScaleFactor: 1,
              text: TextSpan(
                  text: ' 會員DM幣 ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                        text: "${LocalMember.dmCoin}枚",
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    TextSpan(
                        text: '/${LocalMember.points}' + "點",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                  ]),
            ) : Text('請登入會員',  style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold)),
          ),
        ],
      );
    });
  }
}
