import 'package:flutter/material.dart';

import '../utils/app_color.dart';

class TitleHasSign extends StatelessWidget {
  const TitleHasSign(this.data, {Key? key}) : super(key: key);
  final String data;


  @override
  Widget build(BuildContext context) {
    return _cardTitle(data);
  }


  Widget _cardTitle(String data) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 3),
      alignment: Alignment.centerLeft,
      child: RichText(
          textScaleFactor: 1,
          text: TextSpan(
              text: data,
              style: TextStyle(
                  fontSize: 19,
                  color: AppColor.darkBlue,
                  fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontSize: 19,
                        color: Colors.red,
                        fontWeight: FontWeight.bold))
              ])),
    );
  }
}
