import 'package:com.sj.stu_mall_app/app.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../api/api.dart';
import '../models/products_model.dart';
import '../routers/route_name.dart';
import '../utils/util.dart';

class ItemCard extends StatefulWidget {
  const ItemCard(this.data, {Key? key, this.showMoney}) : super(key: key);
  final ProductsModel data;
  final bool? showMoney;

  @override
  State<ItemCard> createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  ProductsModel get data => widget.data;

  bool get showMoney => widget.showMoney ?? true;

  @override
  Widget build(BuildContext context) {
    return _commodityInformationCardView();
  }

  //商品資料
  Widget _commodityInformationCardView() {
    return GestureDetector(
      onTap: () => itemCardOnTap(),
      child: Container(
        height: 100,
        width: 330,
        child: Card(
          elevation: 3,
          margin: EdgeInsets.only(left: 8, right: 8, top: 8),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: CachedNetworkImage(
                imageUrl: Util().getPhotoUrl(data.photo.toString()),
                errorWidget: (context, _, __) => Center(
                    child: Text(
                  '無法顯示商品圖片',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                )),
              )),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                height: 50,
                child: Text(
                  data.name ?? ' ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
              ),
              if (showMoney)
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(left: 10),
                  child: Text(
                    data.price.toString() + '枚 ',
                    textScaleFactor: 1,
                    style: TextStyle(color: Colors.red, fontSize: 17),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  void itemCardOnTap() {
    delegate.push(name: RouteName.productInfo,arguments: data);
  }
}
