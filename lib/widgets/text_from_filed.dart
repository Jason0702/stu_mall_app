import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';

///設定
class TextFieldSetting {
  ///輸入框圓角
  static const double textFieldCircular = 10;

  ///輸入框邊框顏色
  static const Color textFieldBorderColor = Color(0xFF1E2C39);

  ///輸入框邊框顏色
  static const Color textFieldErrorBorderColor = Color(0xFFFF0000);

  ///輸入框文字顏色
  static const Color textFieldTextColor = Color(0xFF9A9A9A);

  ///填充顏色hasFocus
  static const Color fillColorFocus = Color(0xFFFFFFFF);

  ///填充顏色NoFocus
  static const Color fillColorNoFocus = Color(0xFFFFFFFF);

  ///輸入框文字顏色
  static const Color textFieldGreyColor = Color(0xFFF3F3F3);

  static const EdgeInsets padding =
      EdgeInsets.symmetric(horizontal: 15, vertical: 17);

  //手機號碼驗證
  bool phoneVerification(String _phone) {
    if (_phone.isEmpty) return false;
    bool _regExp = RegExp(r'^09\d{8}$').hasMatch(_phone);
    return _regExp;
  }

  //密碼驗證
  bool passwordVerification(String password) {
    if (password.isEmpty) return false;
    bool _regExp = RegExp(r'^\w{6,12}$').hasMatch(password);
    return _regExp;
  }

  //電子信箱驗證
  bool emailVerification(String email) {
    if (email.isEmpty) return false;
    bool _regExp = RegExp(r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]').hasMatch(email);
    return _regExp;
  }

  //active
  static OutlineInputBorder active = OutlineInputBorder(
    borderSide: const BorderSide(color: textFieldBorderColor, width: 1),
    borderRadius: BorderRadius.circular(textFieldCircular),
  );

  //noActive
  static OutlineInputBorder noActive = OutlineInputBorder(
    borderSide: const BorderSide(color: textFieldBorderColor, width: 1),
    borderRadius: BorderRadius.circular(textFieldCircular),
  );

  //error
  static OutlineInputBorder error = OutlineInputBorder(
    borderSide: const BorderSide(color: textFieldErrorBorderColor, width: 1),
    borderRadius: BorderRadius.circular(textFieldCircular),
  );
}

///帳號
class StudentIdTextField extends StatefulWidget {
  const StudentIdTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<StudentIdTextField> createState() => _StudentIdTextFieldState();
}

class _StudentIdTextFieldState extends State<StudentIdTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return studentIdTextField(_controller, _textFieldFocus);
  }

  Widget studentIdTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入帳號",
          prefixIcon: Container(
            padding: EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.person,
              color: AppColor.darkBlue,
              size: 25,
            ),
          ),
          /*      prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginUser,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入帳號";
          }
          /*else if (!TextFieldSetting().phoneVerification(value)) {
          return "手機號碼格式錯誤";
        }*/
          return null;
        },
      ),
    );
  }
}

///手機(帳號)
class AccountTextField extends StatefulWidget {
  const AccountTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<AccountTextField> createState() => _AccountTextFieldState();
}

class _AccountTextFieldState extends State<AccountTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return accountTextField(_controller, _textFieldFocus);
  }

  Widget accountTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          prefixIcon: Container(
            padding: EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.phone,
              color: AppColor.darkBlue,
              size: 25,
            ),
          ),
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入手機號碼",
          /*      prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginUser,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入手機號碼";
          }
          /*else if (!TextFieldSetting().phoneVerification(value)) {
          return "手機號碼格式錯誤";
        }*/
          return null;
        },
      ),
    );
  }
}

///密碼
class PasswordTextField extends StatefulWidget {
  const PasswordTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<PasswordTextField> createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  bool _isShowPassword = false;

  void _showPassword() {
    _isShowPassword = !_isShowPassword;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return passwordTextField(
        _controller, _isShowPassword, _showPassword, _textFieldFocus);
  }

  ///密碼
  Widget passwordTextField(TextEditingController _controller,
      bool isShowPassword, VoidCallback callback, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入密碼",
          prefixIcon: Container(
            padding: EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.lock,
              color: AppColor.darkBlue,
              size: 25,
            ),
          ),
          /*  prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginPassword,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
          suffixIcon: IconButton(
            icon: Icon(
              isShowPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.grey[400],
              size: 25,
            ),
            onPressed: () => callback(),
          ),
        ),
        obscureText: !isShowPassword,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入密碼";
          }
          else if (value.trim().length < 6 || value.trim().length > 12) {
            return "請輸入6~12位英數字";
          } else if (!TextFieldSetting().passwordVerification(value)) {
            return "密碼只能包含英數文字加底線";
          }

          return null;
        },
      ),
    );
  }
}

///驗證碼
class ConfirmCodeTextField extends StatefulWidget {
  const ConfirmCodeTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<ConfirmCodeTextField> createState() => _ConfirmCodeTextFieldState();
}

class _ConfirmCodeTextFieldState extends State<ConfirmCodeTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return confirmCodeTextField(_controller, _textFieldFocus);
  }

  ///驗證碼
  Widget confirmCodeTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入驗證碼",
          /*prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginShield,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入驗證碼";
          }
          return null;
        },
      ),
    );
  }
}

///信箱
class EmailTextField extends StatefulWidget {
  const EmailTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  State<EmailTextField> createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  TextEditingController get _controller => widget.controller;
  FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return emailTextField(_controller, _textFieldFocus);
  }

  ///信箱
  Widget emailTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入電子信箱",
          /*  prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginMail,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _controller,

        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入電子信箱";
          } else if (!TextFieldSetting().emailVerification(value)) {
            return "電子信箱格式錯誤";
          }
          return null;
        },
      ),
    );
  }
}

///邀請碼
class InvitedCodeTextField extends StatefulWidget {
  const InvitedCodeTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<InvitedCodeTextField> createState() => _InvitedCodeTextFieldState();
}

class _InvitedCodeTextFieldState extends State<InvitedCodeTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return invitedCodeTextField(_controller, _textFieldFocus);
  }

  ///邀請碼
  Widget invitedCodeTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入邀請碼",
          /*prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginShield,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (!Util().invitedCodeVerification(value ?? "")) {
            return "邀請碼格式錯誤";
          }
          return null;
        },
      ),
    );
  }
}

///地址
class AddressTextField extends StatefulWidget {
  const AddressTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<AddressTextField> createState() => _AddressTextFieldState();
}

class _AddressTextFieldState extends State<AddressTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return addressTextField(_controller, _textFieldFocus);
  }

  ///地址
  Widget addressTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        focusNode: focusNode,
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入地址",
          /*prefixIcon: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            AppImage.iconLoginShield,
            width: AppSize.prefixIconWidth,
          ),
        ),*/
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "請輸入地址";
          }
          return null;
        },
      ),
    );
  }
}

//內容
class ContentTextField extends StatefulWidget {
  const ContentTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<ContentTextField> createState() => _ContentTextFieldState();
}

class _ContentTextFieldState extends State<ContentTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return contentTextField(_controller, _textFieldFocus);
  }

  ///內容
  Widget contentTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          //文字方向
          hintTextDirection: TextDirection.ltr,
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入內容",
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLines: 8,
        keyboardType: TextInputType.multiline,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.trim().isEmpty) {
            return "請輸入內容";
          }
          return null;
        },
      ),
    );
  }
}

///暱稱 / 姓名
class NickNameTextField extends StatefulWidget {
  const NickNameTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<NickNameTextField> createState() => _NickNameTextFieldState();
}

class _NickNameTextFieldState extends State<NickNameTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return nickNameTextField(_controller, _textFieldFocus);
  }

  ///暱稱 / 姓名
  Widget nickNameTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入暱稱",
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.name,
        textInputAction: TextInputAction.next,
        focusNode: focusNode,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "暱稱不得為空";
          } else if (value.length > Util.nicknameLimit) {
            return "暱稱不能大於${Util.nicknameLimit}字";
          }
          return null;
        },
      ),
    );
  }
}

///身高
class HeightTextField extends StatefulWidget {
  const HeightTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  State<HeightTextField> createState() => _HeightTextFieldState();
}

class _HeightTextFieldState extends State<HeightTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return heightTextField(_controller, _textFieldFocus);
  }

  ///身高
  Widget heightTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        focusNode: focusNode,
        decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入您的身高",
          suffix: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 7),
            child: Text("cm"),
          ),
          prefixIcon: Container(
            width: 50,
            height: 50,
            alignment: Alignment.centerRight,
            child: const Text(
              "身高",
              style: TextStyle(
                  color: TextFieldSetting.textFieldTextColor, fontSize: 20),
            ),
          ),
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
        ],
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "身高不得為空";
          } else if (double.tryParse(value) == null) {
            return "不可輸入非法字元";
          } else if (value.contains(".")) {
            if (value.split(".")[1].length > 1 ||
                value.split(".")[0].length > 4) {
              return "超出數值範圍";
            }
          }
          return null;
        },
      ),
    );
  }
}

///星座
class ConstellationTextField extends StatefulWidget {
  const ConstellationTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<ConstellationTextField> createState() => _ConstellationTextFieldState();
}

class _ConstellationTextFieldState extends State<ConstellationTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return constellationTextField(_controller, _textFieldFocus);
  }

  ///星座
  Widget constellationTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        readOnly: true,
        focusNode: focusNode,
        textAlign: TextAlign.start,
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          prefixIcon: Container(
            width: 50,
            height: 50,
            alignment: Alignment.centerRight,
            child: const Text(
              "星座",
              style: TextStyle(
                  color: TextFieldSetting.textFieldTextColor, fontSize: 20),
            ),
          ),
        ),
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
      ),
    );
  }
}

//體重
class WeightTextField extends StatefulWidget {
  const WeightTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  State<WeightTextField> createState() => _WeightTextFieldState();
}

class _WeightTextFieldState extends State<WeightTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return weightTextField(_controller, _textFieldFocus);
  }

  ///體重
  Widget weightTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        focusNode: focusNode,
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入您的體重",
          suffix: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 7),
            child: Text("kg"),
          ),
          prefixIcon: Container(
            width: 50,
            height: 50,
            alignment: Alignment.centerRight,
            child: const Text(
              "體重",
              style: TextStyle(
                  color: TextFieldSetting.textFieldTextColor, fontSize: 20),
            ),
          ),
        ),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9.]')),
        ],
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "體重不得為空";
          } else if (double.tryParse(value) == null) {
            return "不可輸入非法字元";
          } else if (value.contains(".")) {
            if (value.split(".")[1].length > 1 ||
                value.split(".")[0].length > 4) {
              return "超出數值範圍";
            }
          }

          return null;
        },
      ),
    );
  }
}

///折扣代碼
class DiscountCodeTextField extends StatefulWidget {
  const DiscountCodeTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<DiscountCodeTextField> createState() => _DiscountCodeTextFieldState();
}

class _DiscountCodeTextFieldState extends State<DiscountCodeTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return discountCodeTextField(_controller, _textFieldFocus);
  }

  ///折扣代碼
  Widget discountCodeTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      focusNode: focusNode,
      decoration: InputDecoration(
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: focusNode.hasFocus
            ? TextFieldSetting.fillColorFocus
            : TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入折扣代碼",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.name,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        return null;
      },
    );
  }
}

///統一編號
class UnifiedCompilationTextField extends StatefulWidget {
  const UnifiedCompilationTextField(
      {Key? key, required this.controller, required this.isValidate})
      : super(key: key);
  final TextEditingController controller;
  final bool isValidate;

  @override
  State<UnifiedCompilationTextField> createState() =>
      _UnifiedCompilationTextFieldState();
}

class _UnifiedCompilationTextFieldState
    extends State<UnifiedCompilationTextField> {
  TextEditingController get _controller => widget.controller;

  bool get _isValidate => widget.isValidate;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return unifiedCompilationTextField(
        _controller, _isValidate, _textFieldFocus);
  }

  ///統一編號
  Widget unifiedCompilationTextField(TextEditingController _controller,
      bool _isValidate, FocusNode focusNode) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      focusNode: focusNode,
      decoration: InputDecoration(
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: focusNode.hasFocus
            ? TextFieldSetting.fillColorFocus
            : TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入統一編號",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.name,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (_isValidate) {
          if (value == null || value.isEmpty) {
            return "統一編號不得為空";
          }
          return null;
        }
        return null;
      },
    );
  }
}

///收件人
class RecipientNameTextField extends StatefulWidget {
  const RecipientNameTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  State<RecipientNameTextField> createState() => _RecipientNameTextFieldState();
}

class _RecipientNameTextFieldState extends State<RecipientNameTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return recipientNameTextField(_controller, _textFieldFocus);
  }

  ///收件人
  Widget recipientNameTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      focusNode: focusNode,
      decoration: InputDecoration(
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: focusNode.hasFocus
            ? TextFieldSetting.fillColorFocus
            : TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入全名",
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.name,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "姓名不得為空";
        } else if (value.length > Util.nicknameLimit) {
          return "姓名不能大於${Util.nicknameLimit}字";
        }
        return null;
      },
    );
  }
}

///備註
class RemarkTextField extends StatefulWidget {
  const RemarkTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  State<RemarkTextField> createState() => _RemarkTextFieldState();
}

class _RemarkTextFieldState extends State<RemarkTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return remarkTextField(_controller, _textFieldFocus);
  }

  ///備註
  Widget remarkTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return TextFormField(
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        hintTextDirection: TextDirection.ltr,
        contentPadding: TextFieldSetting.padding,
        focusedBorder: TextFieldSetting.active,
        enabledBorder: TextFieldSetting.noActive,
        errorBorder: TextFieldSetting.error,
        focusedErrorBorder: TextFieldSetting.error,
        fillColor: focusNode.hasFocus
            ? TextFieldSetting.fillColorFocus
            : TextFieldSetting.fillColorNoFocus,
        filled: true,
        hintStyle: TextStyle(color: Colors.grey.shade400),
        hintText: "請輸入留言",
      ),
      focusNode: focusNode,
      maxLines: 6,
      keyboardType: TextInputType.multiline,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        return null;
      },
    );
  }
}

///生日
Widget birthdayTextField(
    TextEditingController _controller, VoidCallback _selectBirthDay) {
  return Container(
    //height: AppSize.textFieldHeight,
    //margin: const EdgeInsets.symmetric(vertical: 0),
    child: TextFormField(
      onTap: () => _selectBirthDay(),
      readOnly: true,
      textAlign: TextAlign.start,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          border: OutlineInputBorder(
            borderSide: const BorderSide(
                color: TextFieldSetting.textFieldBorderColor, width: 1),
            borderRadius:
                BorderRadius.circular(TextFieldSetting.textFieldCircular),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "YYYY / MM / DD",
          suffixIcon: const Icon(
            Icons.keyboard_arrow_down,
            size: 20,
            color: Colors.black,
          )),
      //autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "請選擇生日";
        }
        return null;
      },
    ),
  );
}

class MoneyTextField extends StatefulWidget {
  const MoneyTextField({Key? key,required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  State<MoneyTextField> createState() => _MoneyTextFieldState();
}

class _MoneyTextFieldState extends State<MoneyTextField> {
  TextEditingController get _controller => widget.controller;
  final FocusNode _textFieldFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textFieldFocus.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return moneyTextField(_controller, _textFieldFocus);
  }

  ///體重
  Widget moneyTextField(
      TextEditingController _controller, FocusNode focusNode) {
    return Container(
      //height: AppSize.textFieldHeight,
      //margin: const EdgeInsets.symmetric(vertical: 0),
      child: TextFormField(
        textAlign: TextAlign.start,
        focusNode: focusNode,
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          contentPadding: TextFieldSetting.padding,
          focusedBorder: TextFieldSetting.active,
          enabledBorder: TextFieldSetting.noActive,
          errorBorder: TextFieldSetting.error,
          focusedErrorBorder: TextFieldSetting.error,
          fillColor: focusNode.hasFocus
              ? TextFieldSetting.fillColorFocus
              : TextFieldSetting.fillColorNoFocus,
          filled: true,
          hintStyle: TextStyle(color: Colors.grey.shade400),
          hintText: "請輸入金額",
        ),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9.]')),
        ],
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "金額不得為空";
          } if(value.contains(".")){
            return "金額不得包含小數點";
          }
          return null;
        },
      ),
    );
  }
}
