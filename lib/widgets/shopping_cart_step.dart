import 'package:com.sj.stu_mall_app/providers/shopping_cart_provider.dart';
import 'package:com.sj.stu_mall_app/routers/route_name.dart';
import 'package:com.sj.stu_mall_app/utils/app_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../enum/shopping_cart.dart';
import '../utils/app_color.dart';

class ShoppingCartStep extends StatefulWidget {
  const ShoppingCartStep({Key? key}) : super(key: key);

  @override
  State<ShoppingCartStep> createState() => _ShoppingCartStepState();
}

class _ShoppingCartStepState extends State<ShoppingCartStep> {
  @override
  Widget build(BuildContext context) {
    return statusList();
  }

  Widget statusList() {
    return Container(
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
              child: GestureDetector(
                  onTap: () => backToSelectQty(),
                  child: _stepCard(ShoppingCartStepEnum.selectQty))),
          _arrowCard(),
          Expanded(child: _stepCard(ShoppingCartStepEnum.orderInfo)),
          _arrowCard(),
          Expanded(child: _stepCard(ShoppingCartStepEnum.finish)),
        ],
      ),
    );
  }

  void backToSelectQty() {
    ShoppingCartProvider _shoppingCartProvider =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    if(_shoppingCartProvider.isImmediately){
      return;
    }
    if(_shoppingCartProvider.step == ShoppingCartStepEnum.finish){
      return;
    }
    _shoppingCartProvider
        .changeShoppingCartStepEnum(ShoppingCartStepEnum.selectQty);
    delegate.popUntilPage(name: RouteName.shoppingCart);
  }

  Widget _arrowCard() {
    return Icon(
      Icons.arrow_forward,
      color: AppColor.shoppingCartNone,
    );
  }

  Widget _stepCard(ShoppingCartStepEnum set) {
    return Consumer<ShoppingCartProvider>(builder: (context, s, _) {
      return Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: s.step.index >= set.index
                  ? AppColor.shoppingCartActivity
                  : AppColor.shoppingCartNone,
            ),
            child: Image.asset(set.image),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              child: Text(
            set.label,
            style: TextStyle(
                color: s.step.index >= set.index
                    ? AppColor.shoppingCartActivity
                    : AppColor.shoppingCartNone,
                fontWeight: FontWeight.bold,
                fontSize: 16),
          )),
          SizedBox(
            height: 10,
          ),
        ],
      );
    });
  }
}
