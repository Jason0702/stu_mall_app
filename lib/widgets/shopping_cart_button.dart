
import 'package:com.sj.stu_mall_app/enum/shopping_cart.dart';
import 'package:com.sj.stu_mall_app/save_model/member.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../dialog/not_logging_dialog.dart';
import '../providers/member_provider.dart';
import '../providers/shopping_cart_provider.dart';
import '../routers/route_name.dart';


class ShoppingCartButton extends StatefulWidget {
  const ShoppingCartButton({Key? key}) : super(key: key);

  @override
  _ShoppingCartButtonState createState() {
    return _ShoppingCartButtonState();
  }
}

class _ShoppingCartButtonState extends State<ShoppingCartButton> {
  ShoppingCartProvider? _shoppingCartProvider;
  MemberProvider? _memberProvider;

  @override
  Widget build(BuildContext context) {
    _shoppingCartProvider = Provider.of<ShoppingCartProvider>(context);
    _memberProvider ??= Provider.of<MemberProvider>(context);
    return _shoppingCartWidget();
  }
  ///購物車
  Widget _shoppingCartWidget() {
    return GestureDetector(
      key: UniqueKey(),
      onTap: () => _shoppingCartOnTap(),
      child: Container(
        color: Colors.transparent,
        child: Stack(
          children: [
            Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children: [
                Container(
                  child: Icon(Icons.shopping_cart,size: 35,color: Colors.white,),
                ),
                Text('購物車',style: TextStyle(color:Colors.white,fontSize:15),)
              ],
            ),
            Positioned(right: 0, top: 0, child: _redDot())
          ],
        ),
      ),
    );
  }

  Widget _redDot() {
    return Offstage(
      offstage: _shoppingCartProvider!.memberCart.isEmpty,
      child: Container(
        width: 20,
        height: 20,
        padding: const EdgeInsets.all(2),
        decoration:
        const BoxDecoration(color: Colors.red, shape: BoxShape.circle),
        child: FittedBox(
          child: Text(
            "${_shoppingCartProvider!.memberCart.length}",
            style: const TextStyle(
                fontSize: 18,color: Colors.white),
          ),
        ),
      ),
    );
  }

  ///購物車事件
  void _shoppingCartOnTap() {
    if(LocalMember.memberId == 0){
      _showNotLoggingDialog();
      return;
    }
    _shoppingCartProvider!.changeIsImmediately(false);
    delegate.popUntilPage(name: RouteName.shoppingCart);
  }



  //未登入視窗
  void _showNotLoggingDialog() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            alignment: Alignment.bottomRight,
            scale: anim1.value,
            child: NotLoggingDialog(key: UniqueKey(),),
          );
        });
  }
}
