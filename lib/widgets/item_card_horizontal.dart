import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../dialog/confirm_dialog.dart';
import '../models/products_model.dart';
import '../models/shop_carts_models.dart';
import '../providers/member_provider.dart';
import '../providers/shopping_cart_provider.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';

class ItemCardHorizontal extends StatefulWidget {
  const ItemCardHorizontal({Key? key, required this.data}) : super(key: key);
  final ShoppingCartsModel data;

  @override
  _ItemCardHorizontalState createState() => _ItemCardHorizontalState();
}

class _ItemCardHorizontalState extends State<ItemCardHorizontal> {
  ShoppingCartProvider? _shoppingCartProvider;

  ShoppingCartsModel get data => widget.data;

  int _countValue() {
    return data.qty ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    _shoppingCartProvider = Provider.of<ShoppingCartProvider>(context);
    return _wineCard();
  }

  ///酒卡
  Widget _wineCard() {
    return Container(
      width: AppSize.appWidth,
      margin:
          EdgeInsets.symmetric(horizontal: AppSize.defaultPadding, vertical: 5),
      padding: const EdgeInsets.symmetric(
          horizontal: AppSize.defaultPadding, vertical: 15),
      height: 165,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Container(
            width: 120,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.contain,
                    image: CachedNetworkImageProvider(
                      Util().getPhotoUrl(data.photo!),
                    ),
                    onError: (error, stackTrace) {
                      debugPrint('Error: ${error.toString()}');
                    }),
                borderRadius: BorderRadius.circular(6)),
          ),
          const SizedBox(
            width: 10,
          ),
          //品名 內容
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(data.name ?? " ",
                    maxLines: 2,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    )),
                _priceMoney(),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: _addAndMinusWidget(),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () => _deleteItem(),
            child: Container(
                height: 150,
                margin: EdgeInsets.symmetric(horizontal: 5),
                padding: EdgeInsets.symmetric(vertical: 8.5),
                alignment: Alignment.bottomCenter,
                child: Icon(Icons.delete)),
          ),
        ],
      ),
    );
  }

  ///價格錢
  Widget _priceMoney() {
    return Text(
      "${data.price!.ceil()}枚",
      style: const TextStyle(
          fontSize: 25, color: Colors.red, fontWeight: FontWeight.bold),
    );
  }

  //+ -
  Widget _addAndMinusWidget() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, border: Border.all(color: Colors.black)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //-
          GestureDetector(
            onTap: () => _minusWidgetOnTap(),
            child: Container(
              padding: const EdgeInsets.all(5),
              width: AppSize.addWidgetHIconSize,
              decoration: BoxDecoration(
                border: Border(right: BorderSide(color: Colors.black)),
                color: Colors.white,
              ),
              child: const FittedBox(
                  child: Icon(
                Icons.remove,
                color: Colors.black,
              )),
            ),
          ),
          //數字
          SizedBox(
            width: 55,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "${_countValue()}",
                style: const TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          //+
          GestureDetector(
            onTap: () => _addWidgetOnTap(),
            child: Container(
              padding: const EdgeInsets.all(5),
              width: AppSize.addWidgetHIconSize,
              decoration: BoxDecoration(
                border: Border(left: BorderSide(color: Colors.black)),
                color: Colors.white,
              ),
              child: const FittedBox(
                  child: Icon(
                Icons.add,
                color: Colors.black,
              )),
            ),
          ),
        ],
      ),
    );
  }

  void _addWidgetOnTap() {
     ShoppingCartsModel value = ShoppingCartsModel(productsId: data.productsId, qty: 1);
    _shoppingCartProvider!.addItem(value);
    setState(() {});
  }

  Future<void> _minusWidgetOnTap() async {
    if (_countValue() == 1) {
       bool _ans = await _confirmDialog("您確定要移除嗎?");
      //選擇不要
      if (!_ans) {
        return;
      }
    }
    ShoppingCartsModel value = ShoppingCartsModel(productsId: data.productsId, qty: 1);
    _shoppingCartProvider!.subItem(value);
    setState(() {});
  }

  Future<void> _deleteItem() async {
    bool _ans = await _confirmDialog("您確定要移除嗎?");
    //選擇不要
    if (!_ans) {
      return;
    }
    ShoppingCartsModel value = ShoppingCartsModel(productsId: data.productsId, qty: 1);
    _shoppingCartProvider!.deleteItem(value);
  }

  ///確認視窗
  Future<bool> _confirmDialog(String data) async {
    dynamic _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: ConfirmDialog(
              key: UniqueKey(),
              text: data,
            ),
          );
        });
    if (_result != null && (_result as bool)) {
      return true;
    } else {
      return false;
    }
  }
}
