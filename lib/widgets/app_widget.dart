import 'package:flutter/material.dart';

import '../utils/app_color.dart';
import '../utils/app_size.dart';

class AppWidgetSetting{
  ///AppBarHeight
  static const double appBarHeight = 65;
  static const Color buttonTextColor = Color(0xFFFFFFFF);
}

///返回按鈕 <-
Widget backButtonWidget() {
  return const SizedBox(
    child: Icon(
      Icons.arrow_back,
      size: 35,
      color: Colors.white,
    ),
  );
}

///返回按鈕 X
Widget backButtonClearWidget() {
  return const SizedBox(
    child: Icon(
      Icons.clear,
      size: 35,
      color: Colors.white,
    ),
  );
}


///標題 跟 左邊右邊list 自定義icon
Widget titleBarIntegrate(String _title,
    [List<Widget>? _left, List<Widget>? _right]) {
  return Container(
    height: AppWidgetSetting.appBarHeight,
    width: AppSize.appWidth,
    decoration: const BoxDecoration(
      color: AppColor.appMainColor,
      boxShadow: [
        BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.5), //陰影y軸偏移量
            blurRadius: 0.1, //陰影模糊程度
            spreadRadius: 0.1 //陰影擴散程度
        )
      ],
    ),
    child: Stack(
      alignment: Alignment.center,
      children: [
        //左按鈕
        if (_left != null)
          Positioned(
              left: 15,
              child: Row(
                children: _left,
              )),
        //標題文字
        FittedBox(
          child: Text(
            _title,
            style: const TextStyle(color: Colors.white,fontSize: 25),
          ),
        ),
        //右按紐
        if (_right != null)
          Positioned(
              right: 15,
              child: Row(
                children: _right,
              )),
      ],
    ),
  );
}

///按鈕
Widget buttonWidget(String value) {
  return Container(
      width: 270,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: AppColor.buttonOrange,
          border: Border.all(width: 1,color: AppColor.buttonOrange),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [AppColor.shadow]),
      child: FittedBox(
        child: Text(
          value,
          style: const TextStyle(color: AppWidgetSetting.buttonTextColor, fontSize: 18,fontWeight: FontWeight.bold),
        ),
      ));
}

Widget buttonWidgetDisable(String value) {
  return Container(
      width: 270,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: AppColor.backgroundColor,
          border: Border.all(width: 1,color: AppColor.greyCo),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [AppColor.shadow]),
      child: Text(
        value,
        style: const TextStyle(color: AppColor.greyCo, fontSize: 18),
      ));
}

///按鈕
Widget buttonRedWidget(String value) {
  return Container(
      width: 270,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: AppColor.textRedColor,
          border: Border.all(width: 1,color: AppColor.textRedColor),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [AppColor.shadow]),
      child: FittedBox(
        child: Text(
          value,
          style: const TextStyle(color: AppWidgetSetting.buttonTextColor, fontSize: 18,fontWeight: FontWeight.bold),
        ),
      ));
}

///小按鈕
Widget smallButtonWidget(String value) {
  return Container(
      width: 70,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 7),
      decoration: BoxDecoration(
          color: AppColor.buttonOrange,
          border: Border.all(width: 1,color: AppColor.buttonOrange),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [AppColor.shadow]),
      child: FittedBox(
        child: Text(
          value,
          style: const TextStyle(color: AppWidgetSetting.buttonTextColor, fontSize: 18,fontWeight: FontWeight.bold),
        ),
      ));
}

Widget buttonWidgetDisableGrey(String value) {
  return Container(
      width: 270,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          color: AppColor.greyColorC1,
          border: Border.all(width: 1,color: AppColor.greyColorC1),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [AppColor.shadow]),
      child: Text(
        value,
        style: const TextStyle(color: Colors.white, fontSize: 18),
      ));
}

///title標題
Widget titleCard(String value) {
  return Container(
    width: AppSize.appWidth,
    padding: const EdgeInsets.symmetric(
        horizontal: AppSize.defaultPadding, vertical: 10),
    color: AppColor.greyColorDE,
    child: Text(
      value,
      style: const TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
    ),
  );
}
///title標題
Widget titleCardWhite(String value) {
  return Container(
    width: AppSize.appWidth,
    padding: const EdgeInsets.symmetric(
        horizontal: AppSize.defaultPadding, vertical: 10),
    color: Colors.white,
    child: Text(
      value,
      style: const TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
    ),
  );
}

///沒資料
Widget noDataWidget(String text,String image) {
  return SizedBox(
    width: AppSize.appWidth,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          image,
          width: 150,
        ),
        Text(
          text,
          style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: AppColor.greyColorC1),
        )
      ],
    ),
  );
}

//沒有大頭照UI統一
Widget noAvatarImage(){
  return Container(
    width: 60,
    height: 60,
    decoration: const BoxDecoration(
        shape: BoxShape.circle),
    child: const CircleAvatar(
      backgroundColor: AppColor.appMainColor,
      child: Icon(Icons.person,color: Colors.white,size: 40,),
    ),
  );
}

Widget redDot(){
  return Container(
    width: 13,
    height: 13,
    decoration: const BoxDecoration(
        color: Colors.red, shape: BoxShape.circle),
  );
}

Widget moneyText(String title, String content) {
  return Padding(
    padding: const EdgeInsets.symmetric(
        vertical: 5, horizontal: AppSize.defaultPadding),
    child: Row(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          width: 100,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(title,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                )),
          ),
        ),
        const Spacer(),
        Text(content,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 17,
            )),
      ],
    ),
  );
}

Widget moneyTextBold(String title, String content) {
  return Padding(
    padding: const EdgeInsets.symmetric(
        vertical: 5, horizontal: AppSize.defaultPadding),
    child: Row(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          width: 100,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(title,
                style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                )),
          ),
        ),
        const Spacer(),
        Text(content,
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            )),
      ],
    ),
  );
}

Widget moneyTextRed(String title, String content) {
  return Padding(
    padding: const EdgeInsets.symmetric(
        vertical: 5, horizontal: AppSize.defaultPadding),
    child: Row(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          width: 100,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(title,
                style: const TextStyle(
                  color: AppColor.textRedColor,
                  fontSize: 17,
                )),
          ),
        ),
        const Spacer(),
        Text(content,
            style: const TextStyle(
              color: AppColor.textRedColor,
              fontSize: 17,
            )),
      ],
    ),
  );
}