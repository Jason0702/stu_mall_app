import 'package:com.sj.stu_mall_app/save_model/system_setting.dart';
import 'package:flutter/material.dart';

import '../dialog/instructions.dart';
import '../utils/app_color.dart';

class DmInstructionCard extends StatelessWidget {
  const DmInstructionCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: () => _showDialog(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.help,
            size: 25,
            color: AppColor.appMainColor,
          ),
          Text(
            '使用說明',
            textScaleFactor: 1,
            style: TextStyle(color: AppColor.appMainColor),
          )
        ],
      ),
    );
  }

  Future<void> _showDialog(BuildContext context) async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: InstructionsDialog(
              key: UniqueKey(), data: '${LocalSetting.setting?.instructions}', title: '使用說明',
            ),
          );
        });
  }
}
