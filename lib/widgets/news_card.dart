import 'package:cached_network_image/cached_network_image.dart';
import 'package:com.sj.stu_mall_app/models/news_model.dart';
import 'package:flutter/material.dart';

import '../route/page/news_info.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';

class NewsCard extends StatefulWidget {
  const NewsCard(this.data, {Key? key}) : super(key: key);
  final NewsModel data;

  @override
  State<NewsCard> createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  NewsModel get data => widget.data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _cardOnTap(),
      child: _eventCard(),
    );
  }

  //商品資料
  Widget _eventCard() {
    return Container(
      height: 220,
      width: AppSize.appWidth / 2,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Card(
        elevation: 3,
        margin: EdgeInsets.symmetric(horizontal: 10),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
                child: Image(
              image: CachedNetworkImageProvider(
                Util().getPhotoUrl(data.pic ?? ' '),
              ),
              fit: BoxFit.cover,
              width: AppSize.appWidth / 2,
            )),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(vertical: 1, horizontal: 3),
              child: Text(
                data.title ?? ' ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.symmetric(horizontal: 3, vertical: 3),
              child: Text(
                Util().doubleTime(Util().utcTimeToLocal(data.createdAt ?? ' ')),
                textScaleFactor: 1,
                style: TextStyle(color: AppColor.appMainColor, fontSize: 17),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _cardOnTap() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: NewsInfo(
              data: data,
              key: UniqueKey(),
            ),
          );
        });
  }
}
