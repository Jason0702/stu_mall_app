class AppImage {
  static const String imgGame1 = 'assets/images/img_game1.png';
  static const String imgGame2 = 'assets/images/img_game2.png';

  static const String imgLinePayLogo = 'assets/images/img_line_pay_logo.png';
  static const String imgScanQr = 'assets/images/img_scan_qr.png';
  static const String imgIpassLogo = 'assets/images/img_ipass_logo.png';
  static const String iconSuccess = 'assets/images/icon_success.png';
  static const String iconFailed = 'assets/images/icon_failed.png';
  static const String imgTSticker = 'assets/images/img_t_sticker.png';
  static const String imgEarnPoints1 = 'assets/images/img_earn_points_1.png';
  static const String imgEarnPoints2 = 'assets/images/img_earn_points_2.png';
  static const String imgEarnPoints3 = 'assets/images/img_earn_points_3.png';
  static const String imgEarnPoints4 = 'assets/images/img_earn_points_4.png';
  static const String imgEarnPoints5 = 'assets/images/img_earn_points_5.png';
  static const String imgEarnPoints6 = 'assets/images/img_earn_points_6.png';
  static const String imgEarnPoints7 = 'assets/images/img_earn_points_7.png';
  static const String imgEarnPoints8 = 'assets/images/img_earn_points_8.png';
  static const String imgEarnPoints9 = 'assets/images/img_earn_points_9.png';
  static const String imgEarnPoints10 = 'assets/images/img_earn_points_10.png';
  static const String iconTransaction = 'assets/images/icon_transaction.png';
  static const String btnPay = 'assets/images/btn_pay.png';
  static const String btnTabBarNewsNormal =
      'assets/images/btn_tab_bar_news_normal.png';
  static const String btnTabBarNewsSelected =
      'assets/images/btn_tab_bar_news_selected.png';
  static const String iconDmDollar = 'assets/images/icon_dm_dollar.png';
  static const String iconRanking = 'assets/images/icon_ranking.png';
  static const String iconGame = 'assets/images/icon_game.png';
  static const String iconIcon = 'assets/images/icon_icon.png';
  static const String iconMemberEarnPoints =
      'assets/images/icon_member_earn_points.png';
  static const String iconMyDmDollars = 'assets/images/icon_my_dm_dollars.png';
  static const String iconPersonalInformation =
      'assets/images/icon_personal_information.png';
  static const String iconSearchRecord = 'assets/images/icon_search_record.png';
  static const String iconUpload = 'assets/images/icon_upload.png';
  static const String logoStart = 'assets/images/logo_start.png';
  static const String qrcode = 'assets/images/qrcode.png';
  static const String logoBlack = 'assets/images/logo_black.png';
  static const String imgPrize1 = 'assets/images/img_prize1.png';
  static const String imgPrize2 = 'assets/images/img_prize2.png';
  static const String btnLuckydraw = 'assets/images/btn_luckydraw.png';
  static const String imgCartStep1 = 'assets/images/img_cart_step1.png';
  static const String imgCartStep2 = 'assets/images/img_cart_step2.png';
  static const String imgCartStep3 = 'assets/images/img_cart_step3.png';
  static const String imgPrize1Selected = 'assets/images/img_prize1-selected.png';
  static const String iconGuest = 'assets/images/icon_guest.png';
  static const String iconRegister = 'assets/images/icon_register.png';
  static const String bgGameResult = 'assets/images/bg_game_result.png';
  static const String imgGame3 = 'assets/images/img_game3.png';
  static const String titleGame = 'assets/images/title_game.png';
  static const String titleQuestion = 'assets/images/title_question.png';
  static const String titleGameGif = 'assets/images/title_game.gif';
  static const String iconCartStep1 = 'assets/images/icon_cart_step1.png';
  static const String iconCartStep2 = 'assets/images/icon_cart_step2.png';
  static const String iconCartStep3 = 'assets/images/icon_cart_step3.png';
  static const String imgGame2Frmae = 'assets/images/img_game2_frmae.png';
  static const String robotVerify = 'assets/images/robot_verify.png';
  static const String logoWhite = 'assets/images/logo_white.png';



  ///郵遞區號
  static const String address = 'assets/data/address.json';
}
