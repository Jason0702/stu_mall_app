


class AppSize{
  //width
  static double appWidth = 0;
  //height
  static double appHeight = 0;
  //商店Icon Size
  static const double shopIconSize = 40;
  //底下高度
  static const double tabSizeH = 70;
  //預設內縮
  static const double defaultPadding = 10;
  //商品卡片H
  static const double addWidgetH = 280;
  //商品卡片W
  static const double addWidgetW = 185;
  //商品卡片 +的大小
  static const double addWidgetIconSize = 45;
  //酒卡片橫式 +的大小
  static const double addWidgetHIconSize = 40;
  //產品介紹內文
  static const double productInfoTextSize = 20;
  //Dialog主要字大小
  static const double dialogMainTextSize = 20;
  //Dialog按鈕寬度
  static const double dialogButtonSize = 250;
  //小計內文
  static const double calcTextSize = 17;
  ///長一點的按鈕 350
  static const double longButtonWidth = 350;
  ///大頭照寬度
  static const double mugshotSize = 95;
  ///Dialog按鈕字大小
  static const double dialogButtonTextSize = 18;
  //post card icon size
  static double postCardIconSize = 25;
  //AppBarPage
  static const double buttonCircular = 50;
  ///Dialog按鈕padding水平
  static const double dialogButtonPaddingH = 40;
  ///Dialog按鈕padding垂直
  static const double dialogButtonPaddingV = 15;

}
