



import 'package:shared_preferences/shared_preferences.dart';

import 'util.dart';

class SharedPreferenceUtil{

  static const String ACCOUNT = "account";
  static const String PASSWORD = "password";
  static const String REMEMBER_ACCOUNT = "remember_account";
  static const String SHOPPING_CAR = "shopping_car ";
  //refreshTime
  static const String refreshTime = 'refreshTime';
  static const String hasMsgId = 'hasMsgId';

  ///刪除
  void delUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(ACCOUNT);
    sharedPreferences.remove(PASSWORD);
  }
  ///保存
  void saveUser(String _account, String _password) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(ACCOUNT, _account);
    sharedPreferences.setString(PASSWORD, _password);
  }

  ///保存
  void savePassword(String _password) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(PASSWORD, _password);
  }

  //取得帳號
  Future<String> getAccount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(ACCOUNT) ?? '';
  }
  //取得密碼
  Future<String> getPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(PASSWORD) ?? '';
  }

  Future<Map<String, dynamic>> getAccountAndPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map<String, dynamic> _map = {
      'name': sharedPreferences.getString(ACCOUNT) ?? '',
      'password': sharedPreferences.getString(PASSWORD) ?? ''
    };
    return _map;
  }


  ///儲存是否有按記住帳號
  void saveChecked(bool checked)async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool(REMEMBER_ACCOUNT, checked);
  }
  ///取得是否有按記住帳號
  Future<bool> getChecked() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getBool(REMEMBER_ACCOUNT) ?? false;
  }

  //保存時間
  void saveRefreshTime() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(refreshTime, Util.dateFormat.format(DateTime.now()));
  }
  //讀取時間
  Future<DateTime> getRefreshTime() async {
    SharedPreferences sharedPreference = await SharedPreferences.getInstance();
    return DateTime.parse(sharedPreference.getString(refreshTime) ?? DateTime.now().toString());
  }
  //保存MsgID
  void saveHasMsgId(bool value) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool(hasMsgId, value);
  }
  //讀取MsgID
  Future<bool> getHasMsgId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getBool(hasMsgId) ?? false;
  }
  //刪除MsgID
  void delHasMsgId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(hasMsgId);
  }













}