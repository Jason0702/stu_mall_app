
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NoShadowScrollBehavior extends ScrollBehavior{

  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    switch(getPlatform(context)){
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
        return child;
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return GlowingOverscrollIndicator(
            child: child,
            //不顯示頭部水波紋
            showLeading: false,
            //不顯示尾部水波紋
            showTrailing: false,
            axisDirection: axisDirection,
            color: Theme.of(context).colorScheme.secondary);
    }
  }
}