import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../api/api.dart';
import '../api/auth.dart';
import '../models/token_model.dart';
import '../models/turple_model.dart';
import 'shared_preferences.dart';
import 'util.dart';

class TokenRefresher {
  static final SharedPreferenceUtil _sharedPreferenceUtil =
      SharedPreferenceUtil();

  //會員Token刷新時間判斷
  static Timer? _tokenExpiredTimer;

  //刷新Token判斷
  static void setTokenRefresher() {
    _sharedPreferenceUtil.saveRefreshTime();
    log("set刷新Token判斷");
    _tokenExpiredTimer ??= Timer.periodic(
        const Duration(seconds: 30), (Timer t) => _tokenRefresh());
  }

  //刷新方法
  static void _tokenRefresh() {
    _sharedPreferenceUtil.getRefreshTime().then((value) {
      //取得上一次儲存Token的時間
      log('相差時間: ${DateTime.now().difference(value)}');
      if (DateTime.now().difference(value) >=
          Duration(minutes: Util().tokenExpired)) {
        //是否需要刷新Token
        //刷新Token
        _refreshToken();
      }
    });
  }

  //
  static void _refreshToken() async {
    final account = await _sharedPreferenceUtil.getAccount();
    final password = await _sharedPreferenceUtil.getPassword();
    //驗證
    Tuple<bool, String> _r = await AuthApi().memberLogin(account, password);
    if (!_r.i1!) {
      EasyLoading.showToast("${_r.i2}");
      return;
    }
    TokenModel _data;
    try {
      _data = TokenModel.fromJson(jsonDecode(_r.i2!));
      Api.accessToken = _data.token;
    } catch (e) {
      log("TokenRefresher _refreshToken: $e}");
      return;
    }
  }

  static void tokenCancel() {
    log("cancel刷新Token判斷");
    _tokenExpiredTimer?.cancel();
    _tokenExpiredTimer = null;
  }
}

