import 'dart:developer';

import 'package:com.sj.stu_mall_app/enum/order.dart';
import 'package:com.sj.stu_mall_app/utils/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../app.dart';
import '../routers/route_name.dart';

@pragma('vm:entry-point')
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  await setupFlutterNotifications();
  //showBackgroundFlutterNotification(message);
  log('Handling a background message ${message.messageId}');
}

late AndroidNotificationChannel channel;

bool isFlutterLocalNotificationsInitialized = false;

late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

Future<void> setupFlutterNotifications() async {
  if (isFlutterLocalNotificationsInitialized) {
    return;
  }

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
  const DarwinInitializationSettings initializationSettingsIOS = DarwinInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
  );

  const InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS
  );

  flutterLocalNotificationsPlugin.initialize(initializationSettings);

  channel = const AndroidNotificationChannel(
      'stu_mall_app_channel', 'stu_mall_app_Notifications',
      description: '易購商店推播通知', importance: Importance.high);

  try{

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    if (defaultTargetPlatform == TargetPlatform.iOS ||
        defaultTargetPlatform == TargetPlatform.macOS) {
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
          IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
        sound: true,
        alert: true,
        badge: true,
      );
    } else {
      //android 13
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
          ?.requestPermission();
    }
  } catch (e){
    debugPrint('Firebase Catch Error: ${e.toString()}');
  }



  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, badge: true, sound: true);

  isFlutterLocalNotificationsInitialized = true;
}

void showFlutterNotification(RemoteMessage message) async {
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
            android: AndroidNotificationDetails(channel.id, channel.name,
                channelDescription: channel.description,
                icon: '@mipmap/ic_launcher'
            )));
  }
  if (notification!.body!.contains('訂單取貨通知')){
    await Future.delayed(Duration(milliseconds: 500));
    if(delegate.page.where((element) => element.name == RouteName.myOrderRoute).isNotEmpty){
      if(delegate.canPop()){
        delegate.popRoute();
        delegate.push(name: RouteName.myOrderRoute, arguments: OrderEnum.take);
      }
    }else{
      delegate.push(name: RouteName.myOrderRoute, arguments: OrderEnum.take);
    }
  }
}

void showBackgroundFlutterNotification(RemoteMessage message){
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;

  SharedPreferenceUtil _shared = SharedPreferenceUtil();
  _shared.saveHasMsgId(notification!.body!.contains('訂單取貨通知') ? true : false);

  if (notification != null && android != null && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
            android: AndroidNotificationDetails(channel.id, channel.name,
                channelDescription: channel.description,
                icon: '@mipmap/ic_launcher',
            )));
  }
}

class FirebaseMessage {
  static Future<void> firebaseInit() async {
    await Firebase.initializeApp();
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    if (!kIsWeb) {
      await setupFlutterNotifications();
    }

    FirebaseMessaging.instance.getInitialMessage();

    //前景
    FirebaseMessaging.onMessage.listen(showFlutterNotification);
    //點擊開啟
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      log('A new onMessageOpenedApp event was published! /n ${message.toMap()}');
      if(message.notification!.body!.contains('訂單取貨通知')){
        await Future.delayed(Duration(milliseconds: 500));
        if(delegate.page.where((element) => element.name == RouteName.myOrderRoute).isNotEmpty){
          if(delegate.canPop()){
            delegate.popRoute();
            delegate.push(name: RouteName.myOrderRoute, arguments: OrderEnum.take);
          }
        }else{
          delegate.push(name: RouteName.myOrderRoute, arguments: OrderEnum.take);
        }
      }
    });
  }

  static Future<String?> getToken() async {
    if (defaultTargetPlatform == TargetPlatform.iOS ||
        defaultTargetPlatform == TargetPlatform.macOS) {
      String? token = await FirebaseMessaging.instance.getToken();
      log('FlutterFire Messaging Example: Got APNs token: $token');
      return token;
    } else {
      String? token = await FirebaseMessaging.instance.getToken();
      log('FlutterFire Messaging Example: Got token: $token');
      return token;
    }
  }
}
