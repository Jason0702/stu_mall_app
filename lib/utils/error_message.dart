class EasyLoadingMessage{

  static const String waiting = '請稍後';
  static const String networkError = '網路錯誤，請稍後再試';
  static const String confirmError = '驗證碼錯誤';
  static const String loginSuccess = '登入成功';
  static const String noInvitedCode = '邀請碼無效';
  static const String setGId = '請輸入G id';
  static const String setCellphone = '請輸入手機號碼';
  static const String settingComplete = '設定完成';
  static const String formError = '資料未填完全';
  static const String noMember = '查無會員';
  static const String settingSuccess = '設定成功';
  static const String settingFail = '設定失敗';
  static const String setInput = '內容不可為空';
  static const String addSuccess = '新增成功';
  static const String deleteSuccess = '刪除成功';
  static const String busy = '對方正在通話';
  static const String formatError = '請輸入6-12位英數字';
  static const String cellphoneUsed = '此手機號碼已使用在其他帳戶上';
  static const String creditCardAtLeast100 = '刷卡金額最低為100';
  static const String gameIsClose = '未到開放時間';
  static const String coinNotEnough = 'DM幣不足';
  static const String todayIsPlay = '今日已玩過';
  static const String noOpen = '未開放';
  static const String setName = '請輸入名字';
  static const String oldPasswordError = '舊密碼輸入錯誤';
  static const String newPasswordError = '兩次密碼輸入不同';
  static const String loginFormOtherDevice = '帳號已從其他裝置登入';

  static const String checkSuccess = '簽到成功';
  static const String selectItems = '請選擇至少一項商品';
  static const String selectAnswer = '請選擇一項答案';
  static const String isCheck = '今天已簽到';
  static const String intOnly = '請輸入數字';
  static const String pointsCantBeZero = '輸入點數不能為0';
  static const String exchangeSuccess = '兌換成功';
}
