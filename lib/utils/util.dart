import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import '../api/api.dart';
import '../enum/app_enum.dart';
import '../models/order_model.dart';

class Util {
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  static DateFormat dateFormatISO = DateFormat("yyyy-MM-dd");
  static DateFormat dateFormatBirthDay = DateFormat("yyyy/MM/dd");
  static DateFormat dateFormatHHMM = DateFormat("yyyy-MM-dd HH:mm");
  static NumberFormat formatter = NumberFormat("00");
  static DateFormat dateDay = DateFormat("MM/dd");
  final DateFormat _sqlDateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

  static List<int> deliveryValue = [120, 70, 0, 0];

  ///貼文圖片限制 (一定要有一張 + 四張額外的)
  static const int postLimit = 5;

  ///暱稱限制
  static const int nicknameLimit = 10;

  ///貼文留言限制30字
  static const int postMessageLimit = 30;

  ///Token過期時間
  final int _tokenExpired = 25;

  int get tokenExpired => _tokenExpired;

  ///檔案最大MB上限
  static const int fileMaxSizeMb = 50;

  ///系統設定defaultID
  static const int systemSettingDefaultID = 1;

  int getAppVersion({required String version}) {
    return int.parse(version.replaceAll('.', ''));
  }

  //取得現在UTC時間
  String getMillisecondsSinceEpochTime() {
    return DateTime.now().millisecondsSinceEpoch.toString();
  }

  //生日時間轉換
  Map<String, String> getBirthDay(DateTime _date) {
    Map<String, String> _map = {};
    _map['showText'] = dateFormatBirthDay.format(_date);
    _map['apiText'] = dateFormatISO.format(_date);
    return _map;
  }

  //取得生日時轉換
  String getBirthDayFormat(String _date) {
    DateTime _birthDay = dateFormatISO.parse(_date);
    return dateFormatBirthDay.format(_birthDay);
  }

  //要上傳生日時轉換
  String upLoadBirthDayFormat(String _date) {
    DateTime _birthDay = dateFormatBirthDay.parse(_date);
    return dateFormatISO.format(_birthDay);
  }

  //聊天系統氣泡時間
  String titleTime(DateTime time) {
    String month = formatter.format(time.month);
    String day = formatter.format(time.day);
    return "$month/$day(${chineseWeekDay(time.weekday)})";
  }

  //聊天頁 最後一筆訊息時間
  String lastMessageTime(String data) {
    DateTime time = utcTimeToLocal(data);
    return dateDay.format(time);
  }

  //訊息時間
  String messageTime(String data) {
    DateTime time = utcTimeToLocal(data);
    String hour = formatter.format(time.hour);
    String minute = formatter.format(time.minute);
    if (time.hour > 12) {
      return " $hour:$minute ";
    } else {
      return " $hour:$minute ";
    }
  }

  //時區處理
  DateTime utcTimeToLocal(String time) {
    DateTime localDate = _sqlDateFormat.parse(time, false).toLocal();
    return localDate;
  }

  //時區處理
  DateTime utcTimeToLocalByInt(int _time) {
    DateTime localDate =
        DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: true)
            .toLocal();
    return localDate;
  }

  //取得生日DateTime
  DateTime getDateTimeBirthDay(String _date) {
    return dateFormatISO.parse(_date);
  }

  //時區處理String
  String utcTimeToLocalString(String time) {
    String localDate = dateFormat.parse(time, true).toLocal().toString();
    return localDate;
  }

  String getTime(int _time) {
    DateTime _createdAt =
        DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
    return Util().doubleTime(_createdAt);
  }

  String doubleTime(DateTime _time) {
    return dateFormatISO.format(_time);
  }

  String getTimeDateFormat(int _time) {
    DateTime _createdAt =
        DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
    return dateFormatHHMM.format(_createdAt);
  }

  //星期轉成中文
  String chineseWeekDay(int weekday) {
    switch (weekday) {
      case 1:
        return "一";
      case 2:
        return "二";
      case 3:
        return "三";
      case 4:
        return "四";
      case 5:
        return "五";
      case 6:
        return "六";
      case 7:
        return "日";
      default:
        return "零";
    }
  }

  //手機號碼驗證
  bool phoneVerification(String _phone) {
    if (_phone.isEmpty) return false;
    bool _regExp = RegExp(r'^09\d{8}$').hasMatch(_phone);
    return _regExp;
  }

  //邀請碼驗證
  bool invitedCodeVerification(String value) {
    if (value.isEmpty) return true;
    bool _regExp = RegExp(r'^\w{0,50}$').hasMatch(value);
    return _regExp;
  }

  //assets轉image
  Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load(path);
    final file = File(
        '${(await getTemporaryDirectory()).path}/${path.substring(path.lastIndexOf("/") + 1, path.length)}');
    debugPrint("assets轉image ${file.path}");
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return file;
  }

  String getPhotoUrl(String _url) {
    return 'http://120.119.99.12:5030/public/$_url';
  }

  //商品有分資料夾 公共的
  String getProductUrl(String _url, String? _dir) {
    return '${Api.baseUrl}${_dir ?? '/upload'}/$_url';
  }

  //年齡計算
  String getAge(String _birthday) {
    int age = 0;
    DateTime _dteBirthday = Util().getDateTimeBirthDay(_birthday);
    DateTime dateTime = DateTime.now();
    if (dateTime.isBefore(_dteBirthday)) {
      return '';
    }
    int yearNow = dateTime.year; //當前年分
    int monthNow = dateTime.month; //當前月份
    int dayOfMonthNow = dateTime.day; //當前日期

    int yearBirth = _dteBirthday.year;
    int monthBirth = _dteBirthday.month;
    int dayOfMonthBirth = _dteBirthday.day;

    age = yearNow - yearBirth; //計算歲數
    if (monthNow <= monthBirth) {
      if (monthNow == monthBirth) {
        if (dayOfMonthNow < dayOfMonthBirth) age--; //當前日期在生日之前 年齡減一
      } else {
        age--; //當前月份在生日之前 年齡減一
      }
    }
    return '$age';
  }

  Uint8List getFile(String _path) {
    ///return _path;
    return Uint8List.fromList(File(_path).readAsBytesSync());
  }

/*  //距離計算
  String getDistanceBetween(LatLng _latLngA, LatLng _latLngB) {
    String _value = '';
    double _distance = Geolocator.distanceBetween(_latLngA.latitude,
        _latLngA.longitude, _latLngB.latitude, _latLngB.longitude);
    if (_distance > 1000) {
      _value = '${(_distance / 1000).toStringAsFixed(2)}km';
    } else {
      _value = '${_distance.toStringAsFixed(2)}m';
    }
    return _value;
  }*/
  //base64解密
  String decodeBase64(String data) {
    return String.fromCharCodes(base64Decode(data));
  }

  //base64加密
  String encodeBase64(String data) {
    List<int> content = utf8.encode(data);
    return base64Encode(content);
  }

  String formatMM(int seconds) {
    seconds = (seconds % 3600).truncate();
    final minutes = (seconds / 60).truncate();
    final minutesStr = (minutes).toString().padLeft(2, '0');
    return minutesStr;
  }

  String formatSS(int seconds) {
    seconds = (seconds % 3600).truncate();
    final secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return secondsStr;
  }

  String getStatus(OrderModel data) {
    int isTaked = data.isTaked!;
    int isPayment = data.isPayment!;
    int isShipping = data.isShipping!;
    if (isTaked == AppEnum.active) {
      return ("已取貨");
    }
    if (isPayment == AppEnum.inactive) {
      return ("未付款");
    }
    if (isShipping == AppEnum.inactive) {
      return ("發貨中");
    }
    return ("未取貨");
  }
}
