import 'package:flutter/material.dart';

class AppColor {
  static const Color appMainColor = Color(0xFF00BE9E);
  static const Color backgroundColor = Color(0xFFF5F5F5);
  static const Color darkBlue = Color(0xFF1E2C39);
  static const Color buttonOrange = Color(0xFFF8B551);
  static const Color textRedColor = Color(0xFFae2727);
  ///灰色C1
  static const Color greyColorC1 = Color(0xFFC1C1C1);
  ///灰色DE
  static const Color greyColorDE = Color(0xFFDEDEDE);
  static const Color hintColor = Color(0xFF005AB5);
  static const Color greyCo = Color(0xFFC0C0C0);
  static const Color quizGameBlue = Color(0xFF2E01A2);
  static const Color quizGameBlack = Color(0xFF11003D);
  static const Color quizGamePurple = Color(0xFF210076);
  static const Color selectQuizColor = Color(0xFF32BEA6);
  static const Color quizSendColor = Color(0xFFFFB951);
  static const Color shoppingCartNone = Color(0xFFBEBEBE);
  static const Color shoppingCartActivity = Color(0xFFF8B551);



  static BoxShadow shadow = BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    blurRadius: 6,
    offset: const Offset(0, 3), // changes position of shadow
  );
  static BoxShadow shadowReverse = BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    blurRadius: 6,
    offset: const Offset(0, -3), // changes position of shadow
  );
}
